import os
import sys
from glob import glob
from ruamel.yaml import YAML

if len(sys.argv) < 2:
        print("Sorry, you must specify the path to a CWL workflow directory")
        sys.exit(1)

dir_listing = glob(os.path.join(sys.argv[-1], "*.cwl"))

yaml = YAML()
yaml.preserve_quotes = True

for files in dir_listing:
    try:
        with open(files) as wf:
            parsed_cwl = yaml.load(wf)
    except Exception as e:
        continue

    try:
        if 'WIP' in parsed_cwl['label']:
            print(f"Skipping: {files}")
            continue
    except Exception as e:
        pass

    name = os.path.basename(files)

    os.system(f'cwltool --pack {files} > ./Workflows_Release/{name}')