# Status
Only active & future projects which are blocking switching to the new pipeline.

## Primary
Item | Assignee | Status
---|---|---
[Germline mutation calling (for merging into germline-muts db and inputting to funcotator)](https://gitlab.com/multiple-myeloma/daphni-app/-/issues/79) | waleed | gVCFs now computed at master<br>TODO: look at a cohort of gVCFs<br>TODO: update our own genomics DB with every new run

## Secondary
Each item includes writing a tool and a visualization. See more in the [full issue](https://gitlab.com/multiple-myeloma/daphni-app/issues/78) and descriptions of endpoint in this [teams notebook](https://mtsinai.sharepoint.com/sites/MM1000/_layouts/15/Doc.aspx?sourcedoc={43a1f041-37eb-4c99-a43f-0304305f1c02}&action=edit&wd=target%28Secondary%20Analysis.one%7Ce7dab835-483c-4cae-ab14-d8220e372404%2F2%5C%2F13%5C%2F20%20-%20Risk%20Scoring%7C7571e941-b078-4253-bcb7-980c406ed838%2F%29&wdorigin=703).

Item | Assignee | Status
---|---|---
Functional annotation (VICC) | waleed | MR out, waiting on jacob and evan
Clonality analysis | waleed | Running in DAPHNI-Master<br>Can handle NA purity & "no variants," dumping empty CNVs<br>TODO: CWL-wrap old clonality plot R script<br>Maybe TODO: write a new plotter<br>TODO: Annotate clones w/ "mutations" and "mutations" w/ clones
Risk classification | alessandro & somebody else | To add: GEP70 from old Daphni, somewhere on crusher<br>New: Alessandro to provide script along w/ Sherry
Pathway analysis (DNA) | | Alessandro almost done with list of pathways, will post on GitLab
Pathway analysis (RNA) | waleed & david | TODO: Run [Singscore analysis](https://gitlab.com/multiple-myeloma/daphni-app/-/issues/24#note_326862121), Waleed has results and interpretting with david<br>Alessandro posted a few pathways on Teams for testing Singscore<br>Alessandro to post pathways to GitLab<br>Waleed to evaluate both MixEnrich and Singscore, waiting on pathway list
RNA-based drug repurposing ||
Patient profile based drug repurposing ||
Clinical Trial Eligibility || waleed looking into clinicaltrials.gov (sp?) api
Prediction engine | evan | Evaluating & demoing ElasticSearch as a datastore for prediction engine
Reconsider report UX (how to add new plots/pages to report?) || punting until more tools are added
