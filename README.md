# Sections

### [BioCWL](#biocwl)
* [Works In Progress](#WIP)
* [Naming Tools](#naming)
* [Optional Inputs](#optionalinputs)
* [Record Inputs](#recordinputs)
* [ENUM Inputs](#enuminputs)
* [Secondary Files](#secondaryfiles)

<a name="biowcwl"></a>
# BioCWL

A majority of the principles in this guide can be applied to both WDL and CWL however CWL will be the primary filetype for pipelines.

## Released Pipelines
Our CI/CD system generates release versions of workflows in the Workflow_Release folder. If you are aiming to use these workflows in your analysis it is highly recommended you use these. These have been validated and checked and will contain the urls to the linked tool files.

<a name="WIP"></a>

## Work In Progress 🧱

If you are developing a CWL workflow or tool please make sure to prefix the label tag content with WIP- so our CI systems knows to skip this workflow. It will by default not generate a release but will also produce a failing tag with not much info unless the WIP is present.

From:
```yaml
label: MyBioPipeline
```
To
```yaml
label: WIP-MyBioPipeline
```
<a name="naming"></a>

## Naming Tools 📛

Tools should follow the convention of being prefixed by the parent tool name and Camelcase like so i.e.
`
BWA-Mem.cwl`
or `
BWA-Index.cwl
`

## Tool Feature Requirements 🆕

In order to keep consistent with the biotools spec, the first 3 lines of tool wrappers should be as follows. Our CI/CD system checks for these so make sure to include them so they can be merged into the repo.

```yaml
#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool
```
The first line allows the tool to be run as a single command.
The second specifies the cwl version.
<br/>

**IMPORTANT!** 
The current spec is at v1.1, the IIDSGT group commits tools at **v1.0** due to a limit in our job management system. It is recommended to use the current spec whenever possible.


## Making Files Executable ✴️

Files should be marked as executable before being added 

`
chmod +x tool.cwl
`

## Requirements Section 🧾

There is a requirements section which handles settings for docker and runner config. Docker containers should be from biocontainers.pro if possible.

```yaml
requirements:
  DockerRequirement:
    dockerPull: "biocontainers/fastqc:v0.11.5_cv3"
```

## Validation ✅

Tools need to be free of warning when running with

`
cwltool --validate
`

## Adding Tools To The Repository ➕

Please add tools via pull requests to the repository. Our CI/CD runs validation against the tools and will soon support doing unit tests on the individual tools.

## Descriptions 📃

Tool descriptions should be motivated by a real world use of this tool in a workflow.
The description should focus on a single way of using the tool.
Signs that a tool description is including too much: lots of javascript; complicated data structures; every single flag is listed.

## Schema Description

If you use schema.org annotations, specify the schema using the RDF version: `$schemas: [ http://schema.org/version/latest/schema.rdf ]`

# CWL Tips & Tricks

<a name="optionalinputs"></a>
## Optional Inputs 💯

In CWL unlike WDL optional input settings are defined in the type section of the yaml and not on the declaration. Notice the question mark is on the type.

```yaml
inputs:
  InputRead1:
    type: File
    inputBinding:
      position: 100

  #Optional Inputs
  isCasava:
    type: boolean?
    inputBinding:
      position: 1
      prefix: "--casava"
```
<a name="enuminputs"></a>
## Enum Inputs ⚜️

For commandline flags that require a specific input as the argument an enum type can be declared in CWL. **Specifying null here is known as long form style. It does the same thing as the question mark on the other inputs.**

```yaml
Format:
  type:
    - "null"
    - type: enum
      symbols:
        - bam
        - sam
        - bam_mapped
        - sam_mapped
        - fastq
  inputBinding:
    position: 2
    prefix: "--format"
```
<a name="recordinputs"></a>
## Record Inputs 📀

For commandline flags that are either **mutually exclusive** or **dependent** a special record type can be defined. You can also specify null here to create optional inputs.

```yaml
#Using record inputs to create mutually exclusive inputs

  Strand:
    type:
      - "null"
      - type: record
        name: forward
        fields:
          forward:
              type: boolean
              inputBinding:
                prefix: "--fr-stranded"

      - type: record
        name: reverse
        fields:
          reverse:
            type: boolean
            inputBinding:
              prefix: "--rf-stranded"

  PseudoBam:
    type: boolean?
    inputBinding:
      prefix: "--pseudobam"

#Using record inputs to create dependent inputs
  
  GenomeBam:
    type:
      - "null"
      - type: record
        name: genome_bam
        fields:
          genomebam:
            type: boolean
            inputBinding:
              prefix: "--genomebam"

          gtf:
            type: File
            inputBinding:
              prefix: "--gtf"

          chromosomes:
            type: File
            inputBinding:
              prefix: "--chromosomes"
```

<a name="secondaryfiles"></a>
## Secondary Files 2️⃣

Some tools require additional files as inputs that are meant to be found in the same directory and do not have an explicit link. This is where the secondaryFiles section is utilized for. Here you specify the extension or regex for certain files that need to be mounted. These must be explicitly set in the inputs file as well.

**Important**
There is a quirk with cwl that secondary files specified in inputs.yaml must be at the same root directory as primary inputs. If primary file is at /scratch/genome.fa then all files must be located at /scratch

```yaml
 Index:
    type: File
    inputBinding:
      position: 200
    secondaryFiles:
      - .fai
      - .amb
      - .ann
      - .bwt
      - .pac
      - .sa
```

## Workflow 💣

### Setting Mutually Exclusive Parameters or Dependent w/ Files

If you want workflow level inputs to be passed to an input field and have the accessible from the self object you need 1. to pass them in using the **source** field and 2. include the MultipleInputFeature requirement in the workflow.

```yaml
GenomeBam:
  source: [STARGtf, LenFile]
  valueFrom: |
    ${return {"genomebam": true, "gtf": self.STARGtf, "chromosomes": self.LenFile}}
```

### Setting Mutually Exclusive Parameters

In order to properly set fields in a record input type, you need to pass a dictionary to the input to properly set the parameters. This is done by using inline javascript and returning the dictionary with the key of the field you want to set. The source field is set to indicate the input from the workflow to be used as the value. 

```yaml
steps:

  build_hisat2_index:
    run: ../Tools/Hisat2-Index.cwl
    in:
      InputFiles:
        source: FastaFiles
        valueFrom : | 
          ${return {"fasta": self};}

      IndexName: IndexName
    
    out: [indexes]
```

### Setting Booleans

These can be set by using the default field
```yaml
input:
  default:true
```
### Concating Strings in Inputs

The valueFrom field must be used instead of default.

```yaml
input:
  valueFrom: $("My String:" + input.stringvalue)
```

# BioWDL

# All tool wrappers should be in a single file and not include any other files. Workflows are allowed to include WDL components

Here are some practices that should be followed in regards to building WDL modules in order to keep things consistent.

## Versioning 📜

Task versioning will be autogenerated and inserted into by the CI system for each major release. This will be inserted into the task_version metadata field.

Tool versioning must be handled manually, this will be concordant with each release of tools. (Hint: Use bioconda to get proper versioning)


## Styling 🎨

* The first block within the task should be a meta block

* Variables should use camelcase structure in definition and be specified as either outputFile(n) where n represents the filename/number or  outputDirectory if the output is a directory.

* Task names should be lowercase.

* Commandline arguments should be specified in their long form wherever possible. Makes it easier on other users to see what is actually being executed on the commandline.


```r
task (taskName) {
    meta {
        key1:"value"
        key2:"value"
    }
}
```

## Metadata ⚛️

The metadata section in each task should include at minimal the following metadata fields. It should also follow camelcasing styling.

* Tool Version
* Task Version 
* Matching Container

```r
meta {
    ToolVersion:""
    TaskVersion:""
    Container:""
}

```

## Inputs 🔎

Inputs should be listed in the order of required first then all other optional inputs should be listed.

```r
File InputRead1            
String OutputDir        
File? InputRead2 
```
Command line flags should be created using camelcase Boleans that determine if the flag is set, they should be followed immediately by another variable that is set if their input is required

```r
Boolean? isCasava
Boolean? isFormat
String?  Format
```

## Command ☣️

The command section of a WDL should take a standard bash style script. Arguments however should be split by using bash multi-line commands by utilizing \ 

Since WDL DOES support inline expressions you should use them to properly fill command line flags if optional variables are set.

**At the end of each flag a space must be included before the closing double quote to ensure proper interpretation of the command.**

```r
   command {
       fastqc --extract \
       -o ${OutputDir} \
       ${if isCasava == false then "--casava " else ""} \
       ${if Format != "" then "--format " + format + " " else ""} \
       ${if Kmers != 0 then "-k " + Kmers + " " else ""} \
       ${if Adapters != "" then "-a " + Adapters + " " else ""} \
       ${if Contaminants != "" then "-c " + Contaminants + " " else ""} \
       ${InputRead1}
   }
```
## Runtime 🏃🏾‍

The runtime section should only require two parameters at the current time. 

This should include the container and the continueOnReturnCode flag set to false, unless the tool never exits with a 0 exit-code.

```r
   runtime {
      continueOnReturnCode: false
      docker: "biocontainers/fastqc:v0.11.5_cv3"
   }
```