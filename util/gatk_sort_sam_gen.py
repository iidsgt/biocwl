
from cwl_builder import *

builder = CwlBuilder(template="""cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "broadinstitute/gatk:4.1.1.0"
  InlineJavascriptRequirement: {{}}

inputs:
  # REQUIRED ARGS
{required_section}

  # OPTIONAL ARGS
{optional_section}

arguments: 
  - valueFrom: "SortSam"
    position: -1

baseCommand: ["/gatk/gatk"]

outputs:
  index:
    type: File
    outputBinding:
      glob: $(inputs.Output)
""")

with CwlType.WithDefaults(repeated=False, optional=False):
  builder.AddArgs(SamOrBamFile, 'INPUT')
  builder.AddArgs(string, 'OUTPUT',
      default="""$("SortSam" + inputs.Input.nameext)""")
  builder.AddArg(Enum('SORT_ORDER', ['queryname', 'coordinate', 'duplicate']))

with CwlType.WithDefaults(repeated=True, optional=True):
  builder.AddArgs(File, 'arguments_file')
  builder.AddArgs(Directory, 'TMP_DIR')

with CwlType.WithDefaults(repeated=False, optional=True):
  builder.AddArgs(boolean, """
      help
      version
      CREATE_INDEX
      CREATE_MD5_FILE
      QUIET
      USE_JDK_DEFLATER
      USE_JDK_INFLATER
      showHidden""")
  builder.AddArgs(int, """
      COMPRESSION_LEVEL
      MAX_RECORDS_IN_RAM""")
  builder.AddArgs(string, "java-options", position=-2)
  builder.AddArgs(File, "GA4GH_CLIENT_SECRETS")
  builder.AddArgs(FastaFile, "REFERENCE_SEQUENCE")
  builder.AddArg(Enum("VALIDATION_STRINGENCY",
                      ['STRICT',
                       'LENIENT',
                       'SILENT']))
  builder.AddArg(Enum("VERBOSITY",
                      ['ERROR',
                       'WARNING',
                       'INFO',
                       'DEBUG']))

print(builder.Build())
