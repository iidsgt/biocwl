#! /bin/bash

python3 gatk_merge_bam_alignment_gen.py > ../Tools/GATK-MergeBamAlignment.cwl
python3 gatk_sort_sam_gen.py > ../Tools/GATK-SortSam.cwl
python3 gatk_set_nm_md_and_uq_tags_gen.py > ../Tools/GATK-SetNmMdAndUqTags.cwl
python3 star_gen.py > ../Tools/STAR.cwl
