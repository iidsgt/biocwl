# Utility classes for building CWL files.

class CwlType(object):
  """
  Call WithDefaults to override the default CTOR values in a local context. Eg:
    with CwlType.WithDefaults(optional=True):
      # Construct a bunch of optional arguments.
      ...

  Properties:
    name: arg name on command line
  """

  TYPE_PROPERTIES = ['repeated',
                     'optional',
                     'exclude_from_command_line',
                     'default',
                     'position',
                     'camel_name',
                     'separate']
  def __init__(self, name, **kwargs):
    self.name = name
    self.ctor_args = kwargs

  def RawName(self):
    return self.name

  def GetCamelName(self):
    if self.Get('camel_name') is not None:
      return self.Get('camel_name')
    return ''.join([word.capitalize() for word in self.name.lower().split('_')])

  def GetBasicType(self):
    raise UnimplementedError()

  def GetFullType(self):
    assert self.Get("repeated") is not None
    assert self.Get("optional") is not None
    return (self.GetBasicType() +
            ('[]' if self.Get('repeated') else '') +
            ('?' if self.Get('optional')else ''))

  def GetFormatStr(self):
    return ''


  # Utilities for maintaining the Type CTOR defaults.
  class GlobalPropertySet(object):
    """Maintains the global defaults values for parameter properties."""

    def __init__(self, properties):
      self.properties = properties

    def _GetInternal(self, prop):
      return None if prop not in self.properties else self.properties[prop]

    @classmethod
    def GetStack(classname, stack=[]):
      return stack
   
    @classmethod
    def Get(classname, prop):
      assert prop in CwlType.TYPE_PROPERTIES, "Invalid property name %s" % prop
      for prop_set in reversed(CwlType.GlobalPropertySet.GetStack()):
        if prop in prop_set.properties:
          return prop_set.properties[prop]
      return None

  class GlobalPropertyContext(object):
    def __init__(self, properties):
      self.props = properties

    def __enter__(self):
      CwlType.GlobalPropertySet.GetStack().append(CwlType.GlobalPropertySet(self.props))

    def __exit__(self, type, value, traceback):
      CwlType.GlobalPropertySet.GetStack().pop()

  @classmethod
  def WithDefaults(classname, **kwargs):
    for prop in kwargs:
      assert prop in CwlType.TYPE_PROPERTIES, "Invalid property name %s" % prop
    return CwlType.GlobalPropertyContext(kwargs)

  def Get(self, property_name):
    if property_name in self.ctor_args:
      return self.ctor_args[property_name]
    return CwlType.GlobalPropertySet.Get(property_name)


# TYPES

# Basic types.
class double(CwlType):
  def GetBasicType(self): return 'double'

class int(CwlType):
  def GetBasicType(self): return 'int'

class boolean(CwlType):
  def GetBasicType(self): return 'boolean'

class string(CwlType):
  def GetBasicType(self): return 'string'


# File types.
class File(CwlType):
  def GetBasicType(self): return 'File'

class FastaFile(CwlType):
  def GetBasicType(self): return 'File'
  def GetFormatStr(self):
    return """
  format: http://edamontology.org/format_1929  # FASTA """

class SamOrBamFile(CwlType):
  def GetBasicType(self): return 'File'
  def GetFormatStr(self):
    return """
  format:  # SAM or BAM
   - "http://edamontology.org/format_2573"
   - "http://edamontology.org/format_2572" """

class Directory(CwlType):
  def GetBasicType(self): return 'Directory'

# Complex types.
class Enum(CwlType):
  def __init__(self, name, enum_values, *args, **kwargs):
    super().__init__(name, *args, **kwargs)
    self.enum_values = enum_values

  def GetFullType(self):
    assert not self.Get('repeated'), "Can not yet support repeated enums."
    symbols = ('\n' + ' ' * 6 + '- ').join([''] + self.enum_values)
    return """
   - "null"
   - type: enum
     symbols:{symbols}""".format(symbols=symbols)


class CwlBuilder(object):
  """Builds a single CWL file, collecting the arguments and other params."""
  def __init__(self, template):
    self.template = template
    self.required_args = dict()
    self.optional_args = dict()


  def _ArgToString(self, arg, prefix, include_format=True):
    camel = arg.GetCamelName()
    type_str = arg.GetFullType()
    format_str = arg.GetFormatStr() if include_format else ''

    input_binding_str = ''
    if not arg.Get('exclude_from_command_line'):
      input_binding_str = """
  inputBinding:
    prefix: "--{raw}" """.format(raw=arg.RawName())
      if arg.Get('repeated'):
        if arg.Get('separate') is not None and arg.Get('separate'):
          input_binding_str += """
    separate: true"""
        else:
          input_binding_str += """
    separate: false
    itemSeparator: "," """

      if arg.Get('position') is not None:
        input_binding_str += """
    position: %d""" % arg.Get('position')

    default_str = ''
    if arg.Get('default') is not None:
      default_str = """
  default: %s""" % arg.Get('default')

    arg_str = """{camel}: 
  type: {type_str}{format_str}{default_str}{input_binding_str}""".format(
      type_str=type_str,
      format_str=format_str,
      default_str=default_str,
      input_binding_str=input_binding_str,
      camel=camel)
    # Add the prefix.
    return ('\n' + prefix).join([''] + arg_str.split('\n'))


  def AddArg(self, arg):
    camel = arg.GetCamelName()
    arg_str = self._ArgToString(arg, prefix="  ")
    assert arg.Get('optional') is not None
    if arg.Get('optional'):
      self.optional_args[camel] = arg_str
    else:
      self.required_args[camel] = arg_str


  def AddMutuallyExclusiveArgs(self, arg_category_name, arg_group_1, arg_group_2):
    # For now, as a hack, we just use the first argument as
    # the name of the whole argument group.
    arg_group_1_name = arg_group_1[0].GetCamelName()
    arg_group_2_name = arg_group_2[0].GetCamelName()

    # One major difference between CommandInputParameter and CommandInputRecordField
    # is that record fields don't support "format" fields.
    arg_group_1_str = ''.join([self._ArgToString(arg, prefix=' ' * 9, include_format=False)
                               for arg in arg_group_1])
    arg_group_2_str = ''.join([self._ArgToString(arg, prefix=' ' * 9, include_format=False)
                               for arg in arg_group_2])
    self.optional_args[arg_category_name] = """
  {arg_category_name}:
    type:
     - "null"
     - type: record
       name: {arg_group_1_name}
       fields:{arg_group_1_str}
     - type: record
       name: {arg_group_2_name}
       fields:{arg_group_2_str}""".format(
         arg_category_name=arg_category_name,
         arg_group_1_name=arg_group_1_name,
         arg_group_1_str=arg_group_1_str,
         arg_group_2_name=arg_group_2_name,
         arg_group_2_str=arg_group_2_str)

  def AddArgs(self, arg_type, args, **kwargs):
    if type(args) == str:
      args = args.split()
    assert type(args) == list, "Failure on " + str(args)
    for arg_name in args:
      self.AddArg(arg_type(arg_name, **kwargs))

  def Build(self):
    required_section = '\n'.join(
        [v for k,v in sorted(self.required_args.items(), key=lambda val : val[0])])
    optional_section = '\n'.join(
        [v for k,v in sorted(self.optional_args.items(), key=lambda val : val[0])])
    return self.template.format(required_section=required_section,
                                optional_section=optional_section)
