# Run like python util/gatk_merge_bam_alignment_gen.py > GATK-MergeBamAlignment.cwl

from cwl_builder import *

builder = CwlBuilder(template="""cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "broadinstitute/gatk:4.1.1.0"
  InlineJavascriptRequirement: {{}}

inputs:
  # REQUIRED ARGS
{required_section}

  # OPTIONAL ARGS
{optional_section}

arguments: 
  - valueFrom: "MergeBamAlignment"
    position: -1

baseCommand: ["/gatk/gatk"]

outputs:
  index:
    type: File
    outputBinding:
      glob: $(inputs.Output)
""")

with CwlType.WithDefaults(repeated=False, optional=False):
  builder.AddArgs(string, "OUTPUT",
      default="""$("MergeBamAlignment" + inputs.Input.nameext)""")
  builder.AddArgs(FastaFile, "REFERENCE_SEQUENCE")
  builder.AddArgs(FastaFile, "REFERENCE_SEQUENCE_INDEX",
                  exclude_from_command_line=True)
  builder.AddArgs(SamOrBamFile, "UNMAPPED_BAM")

with CwlType.WithDefaults(repeated=False, optional=True):
  builder.AddArgs(boolean, """
      ADD_MATE_CIGAR
      ALIGNED_READS_ONLY
      ALIGNER_PROPER_PAIR_FLAGS
      CLIP_ADAPTERS
      CLIP_OVERLAPPING_READS
      help
      INCLUDE_SECONDARY_ALIGNMENTS
      IS_BISULFITE_SEQUENCE
      UNMAP_CONTAMINANT_READS
      version
      ADD_PG_TAG_TO_READS
      CREATE_INDEX
      CREATE_MD5_FILE
      QUIET
      USE_JDK_DEFLATER
      USE_JDK_INFLATER
      showHidden""")

  builder.AddArgs(string, """
      PROGRAM_GROUP_COMMAND_LINE
      PROGRAM_GROUP_NAME
      PROGRAM_GROUP_VERSION
      PROGRAM_RECORD_ID
      READ2_TRIM""")

  builder.AddArgs(File, "GA4GH_CLIENT_SECRETS")

  builder.AddArgs(Directory, """
      MAX_INSERTIONS_OR_DELETIONS
      MIN_UNCLIPPED_BASES
      READ1_TRIM
      READ2_TRIM
      COMPRESSION_LEVEL
      MAX_RECORDS_IN_RAM""")

  builder.AddArgs(string, "java-options", position=-2)

  # Enums.
  builder.AddArg(Enum("PRIMARY_ALIGNMENT_STRATEGY",
                      ['BestMapq',
                       'EarliestFragment',
                       'BestEndMapq',
                       'MostDistant']))
  builder.AddArg(Enum("SORT_ORDER",
                      ['unsorted',
                       'queryname',
                       'coordinate',
                       'duplicate',
                       'unknown']))
  builder.AddArg(Enum("UNMAPPED_READ_STRATEGY",
                      ['COPY_TO_TAG',
                       'DO_NOT_CHANGE',
                       'MOVE_TO_TAG']))
  builder.AddArg(Enum("VALIDATION_STRINGENCY",
                      ['STRICT',
                       'LENIENT',
                       'SILENT']))
  builder.AddArg(Enum("VERBOSITY",
                      ['ERROR',
                       'WARNING',
                       'INFO',
                       'DEBUG']))


with CwlType.WithDefaults(repeated=True, optional=True):
  builder.AddArgs(File, 'arguments_file')

  builder.AddArgs(string, """
      ATTRIBUTES_TO_REMOVE
      ATTRIBUTES_TO_RETAIN
      ATTRIBUTES_TO_REVERSE
      ATTRIBUTES_TO_REVERSE_COMPLEMENT
      EXPECTED_ORIENTATIONS
      MATCHING_DICTIONARY_TAGS""")

  builder.AddArgs(Directory, "TMP_DIR")

  builder.AddMutuallyExclusiveArgs(
      "Reads",
      [SamOrBamFile("ALIGNED_BAM")],
      [SamOrBamFile("READ1_ALIGNED_BAM"), SamOrBamFile("READ2_ALIGNED_BAM")])

print(builder.Build())
