from argparse import ArgumentParser
from os import access, cpu_count, X_OK
from pathlib import Path
from re import match
from subprocess import run

import sys

import crayons
green = lambda s: print(crayons.green(s))
yellow = lambda s: print(crayons.yellow(s))
red = lambda s: print(crayons.red(s))

from joblib import Parallel, delayed

failed_paths = []
def check_path(cwl_path):
    failures = []
    def err(msg):
        failures.append(msg)

    if not cwl_path.name.lower().endswith(".cwl"):
        return (cwl_path, failures)

    green(f'Checking {cwl_path}')

    result = run([ 'cwltool', '--validate', str(cwl_path) ], capture_output=True, check=False)
    returncode = result.returncode
    if returncode:
        err({
            'msg': 'Failed validation',
            'returncode': returncode,
            'stdout': result.stdout.decode(),
            'stderr': result.stderr.decode(),
        })

    with cwl_path.open('r') as f:
        lines = [ line.rstrip() for line in f.readlines() if line.rstrip() ]

        expected = [
            "#!/usr/bin/env cwl-runner",
            r"cwlVersion: ?v1\.[01]",
            r"class: (?:CommandLineTool|Workflow)",
        ]

        for idx, (line, regex) in enumerate(zip(lines, expected)):
            if not match(regex, line):
                err(f"Line {idx+1}: '{line}' didn't match '{regex}'")

    if not access(str(cwl_path), X_OK):
        err(f"File Failed Executable Requirement: {cwl_path}")

    return (cwl_path, failures)


def main(cwl_paths, n_jobs):
    n_jobs = n_jobs or cpu_count()
    green(f'Checking {len(cwl_paths)} (using {n_jobs} threads):')
    parallel = Parallel(n_jobs=n_jobs)
    failures = {
        cwl_path: failures
        for cwl_path, failures in
        parallel(
            delayed(check_path)(cwl_path)
            for cwl_path in cwl_paths
        )
        if failures
    }

    if failures:
        for path, errors in failures.items():
            red(f'{path}')
            for err in errors:
                if isinstance(err, str):
                    red(f'\t{err}')
                elif isinstance(err, dict):
                    red('\t%s (%d):' % (err['msg'], err['returncode']))
                    for line in err['stdout'].split('\n'):
                        yellow(f'\t\t{line[:-1]}')
                    for line in err['stderr'].split('\n'):
                        red(f'\t\t{line[:-1]}')
            print('')

        red(f'{len(failures)} CWLs failed:')
        for path in failures.keys():
            red(f'\t{path}')
        print('')

        sys.exit(1)

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('cwl_path', nargs='*', help='CWL paths to check; defaults to all CWLs in repo')
    parser.add_argument('-n', '--threads', required=False, default=None, type=int, help='Number of paths to check concurrently')
    args = parser.parse_args()
    cwl_paths = [ Path(cwl_path) for cwl_path in args.cwl_path ]
    if cwl_paths:
        cwl_paths = [
            subpath
            for cwl_path in cwl_paths
            for subpath in cwl_path.glob('**/*.cwl')
        ]
    else:
        cwd = Path.cwd()
        cwl_paths = [
            path.relative_to(cwd)
            for path in
            cwd.glob('**/*.cwl')
        ]

    main(cwl_paths, n_jobs=args.threads)
