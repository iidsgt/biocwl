secondary_yaml="/data1/daphni2/mm-yamls/secondary/SEMA-MM-060RNA/MM_0315_BM_01.SECONDARY.yml"
priority=850
new_yaml=${secondary_yaml##*/} 
report_id=`echo $new_yaml | sed 's/.SECONDARY.yml//g'`

echo "submitting SECONDARY for report ID" $report_id "with yaml" $secondary_yaml

docker run \
-v "/data1/daphni2/biocwl:/biocwl" \
-v "${secondary_yaml}:/yaml" \
-v /var/run/docker.sock:/var/run/docker.sock \
-e ARVADOS_API_TOKEN \
-e ARVADOS_API_HOST \
sinaiiidgst/arvclient \
arvados-cwl-runner \
--submit \
--no-wait \
--intermediate-output-ttl 2592000 \
--project-uuid myarv-j7d0g-4j0q8ta0l27blwm \
--priority $priority \
--name secondary_test \
--output-name secondary_test \
--output-tags SECONDARY,outputs \
/biocwl/Workflows/DAPHNI-Secondary.cwl \
/yaml