# Primary Analysis
*(Ported from [daphni-app#74](https://gitlab.com/multiple-myeloma/daphni-app/issues/74))*

![image](https://p199.p4.n0.cdn.getcloudapp.com/items/BluN9Rel/Screen+Shot+2020-02-04+at+2.05.53+PM.png?v=4d087dd0043fe36739434534d7e98a88)

## 2020-02-04
Direct primary-processing work:
- Testing MergeCountsVCF (Evan)
- Fixing Lancet `--reg` vs `--bed` flags (Jacob)

General / testing / housekeeping
- downsampling data in many ways (Evan/Ryan)

Other / Downstream tools:
- Debugging gene-fusions/STAR/Arriba (Waleed)
- Secondary analysis:
  - cohort merge
  - batch correction

## Small test data

### Currently
- sampled 10k reads uniformly across genome
- further restricting to specific intervals (chr17; 1200bp around tp53)

### Ideally
- chr17 (for tp53) and chr16 (to test parallelization / split+merge)
  - maybe even specific regions w/in them
- additionally down-sample by X% in those regions

## Nightly run
- [ ] Nightly run machine: failing on OOM
  - running steps in sequence to see if it helps
- [ ] JM nightly run: set up, debugging
  - no successful runs yet

## DNA
Currently iterating on downsampled FASTQs from earlier MM runs (TODO: which ones? @osmanwa), on S4 AWS PCluster

### DNA Pre-processing
![image](https://p199.p4.n0.cdn.getcloudapp.com/items/kpumgALo/Screen+Shot+2020-02-04+at+2.16.05+PM.png?v=572dbb7205fa3cc45d6985bc7a3d594e)
No known bugs.

### DNA Post-Processing
**TODO**:
- [x] Make DAPHNI-DNA-post-processing.cwl, put consensus variant calling inside - jacob
- [x] Add Facets-SNP.cwl to DNA-post-processing.cwl - jacob

#### CNV (aka SnpPileup aka Facets)
- [x] Docker image should be put on sinaiiidgst docker hub - jacob
- [ ] run on small data (master_small.cwl) [initial validation was done by David and Waleed way-back-when] - jacob
- [ ] run on large data (master.cwl) [initial validation was done by David and Waleed way-back-when]
- [ ] once it's running no problem on large data, do another round of validation (cause now preprocessing steps have changed)

#### Consensus Variant Calling
![image](https://p199.p4.n0.cdn.getcloudapp.com/items/WnuEOmxP/Screen+Shot+2020-02-04+at+2.19.18+PM.png?v=5021bdf3734fad393c22f988f579fe81)
- Lancet flags issue: `--reg` vs `--bed`
  - two axes of restricting the genome:
    - `--reg`: genomic regions (e.g. `chr17`)
    - `--bed`: intervals file
  - **TODO**: filter the WES bed file to just be e.g. chr17
    - useful in testing
    - also in prod: scattering lancet instances across chrs, calling each lancet w/ a chr-specific bed file
    - presumably a simple `bedtools` cmd; **TODO** what is it?

#### After consensus calling:
- [x] bamreadcounts (annotates VCF variants with read-depth etc. from BAMs) (@djevo1)
  - Tested (successfully) on chr17 10p FASTQs
- [x] funcotator (secondary)
  - Ran on chr17 10p bamreadcounts, but it dropped VAF...
- [ ] Merge funcotator into DAPHNI-Master @ master
- [ ] append variant allele frequence to funcotator output
  - then this **single file** (MAF format) has all mutation info (SNPs, indels), no VCFs necessary.

## RNA
### RNA Pre-processing
`DAPHNI-RNA-Pre.cwl`:
![image](https://p199.p4.n0.cdn.getcloudapp.com/items/4guxoX5Y/Screen+Shot+2020-02-04+at+2.33.24+PM.png?v=87709ff9405bea3e6237af7bed445755)

#### Gene Fusions
- Arriba only calling intra-chr deletions
  - seemingly bc STAR was not outputting chimeric reads
  - earlier fusion-calls (on Crusher / Daphni v1):
    - BAM came from gsnap
    - chimeric reads in separate SAM
  - David put 6 MMRF FASTQs on Crusher
    - 3 samples, should all have MMSET
    - all should call a WHSC1/MMSET fusion
    - Waleed ran them, saw some empty BAMs, implies something wrong with how we're running it. **TODO**
- Interesting research findings / TODOs
  - CCND1 fusions getting caught in filters, added to discarded fusions
    - David to look over CCND1 fusions caught by filter

#### TODOs
- [x] find test data that results in gene fusions being output (@osmanwa; #13)
  - figure out whether MMSET was supposed to be present in the test run https://gitlab.com/multiple-myeloma/daphni-app/issues/13#note_278386445
  - TBD whether to keep looking for MMSET on that sample, vs. run on ~20 samples from @Lagana offline
- [x] add MarkDuplicates to "new" RNA CWL (@osmanwa)
- [x] merge `DAPHNI-RNA{,_new}.cwl`, rename to `_pre`, call it from master (@osmanwa)
- [ ] Run all 10 MSSM samples, verify that they all have MMSET (btwn 4 and 14) translocations
  - Status: ~5/10 MSSM samples have been run thusfar
  - Note: CCND1 translocations would be cool to find, but not a marker for whether the tool is working.


### RNA Post-processing
![image](https://p199.p4.n0.cdn.getcloudapp.com/items/KouePENP/Screen+Shot+2020-02-04+at+2.42.38+PM.png?v=d879886020a0d11d1203bb3b2feeb532)

#### Detail: batch-correction
- **Blocked by having primary analysis working on large samples.**
- [ ] RNACohort-Merge.cwl
- [ ] feature-counts found and put in immutable-data S4 S3
  - batch names preserved, sample names replaced w/ arbitrary values
    - possible that arbitrary sample-name placeholder values will cause a problem
    - can redo upload if so