priority=300

# echo "creating merge input yaml"

# python /data1/daphni2/arv-utils/create_merge_yml_from_manifest.py \
# /data1/daphni2/mm-yamls/merge/ \
# /data1/daphni2/mssm-inventory/master-tables/arvados_primary_uuid_manifest_phi_latest.csv 

echo "submitting DNA+RNA Cohort Merge..." 

docker run \
-v "$PWD:/data" \
-v /var/run/docker.sock:/var/run/docker.sock \
-e ARVADOS_API_TOKEN \
-e ARVADOS_API_HOST \
sinaiiidgst/arvclient \
arvados-cwl-runner \
--submit \
--no-wait \
--intermediate-output-ttl 2592000 \
--project-uuid myarv-j7d0g-4j0q8ta0l27blwm \
--priority $priority \
--name DNA-RNA-Cohort-Merge \
--output-name DNA-RNA-Cohort-Merge \
--output-tags MERGE,outputs \
/data/Workflows/DNA-RNA-Cohort-Merge.cwl \
/data/cohort_merge.latest.yml
