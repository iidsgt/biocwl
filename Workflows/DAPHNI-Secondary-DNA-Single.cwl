#!/usr/bin/env cwl-runner
cwlVersion: v1.1

class: Workflow
label: DAPHNI DNA Secondary Analyses
requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  ScatterFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  SubworkflowFeatureRequirement: {}
  ResourceRequirement:
    ramMin: 1000      # [MB]
    coresMin: 1        # [cores]
    tmpdirMin: 45000   # [MB]

inputs:
  # Metadata about this run. This should be the column name of the
  # outputs from the RNA merge step (see RNACohort-Merge.cwl).
  report_id: string

  # Inputs that don't change.
  CytoBands: File
  CytobandsHG38Custom: File
  GeneConversionTable: File
  PathwaysDNA-RNA: File
  PathwaysMixEnrich: File
  ProgenyGeneConversionTable: File
  CNVFeatures: File
  ExpressionFeaturesRem: File
  IntervalFile: File

  # Outputs from Primary.
  facets_cellularity: File
  facets_cncf: File
  facets_summary: File
  counted_vcf: File
  annotated_variants: File[]
  tumor_dna_final_bam: File
  FacetsCNVAnnotateFile: File

  # Outputs from RNA Merge.
  #raw_counts: File
  #zscores: File
  #foldchange_table: File
  #merged_rna_rdata: File
  #batchTable: File
  #lincsAnnotRData: File
  
  
outputs:
  cluster_gene_list_output:
    type: File
    outputSource: dna_secondary/cluster_gene_list_output

  cnv_output:
    type: File
    outputSource: dna_secondary/cnv_output

  ssm_output:
    type: File
    outputSource: dna_secondary/ssm_output

  cnv_vicc_out:
    type: File
    outputSource: dna_secondary/cnv_vicc_out

  mutation_vicc_out:
    type: File
    outputSource: dna_secondary/mutation_vicc_out

  cnv_civic_out:
    type: File
    outputSource: dna_secondary/cnv_civic_out

  mutation_civic_out:
    type: File
    outputSource: dna_secondary/mutation_civic_out

  ViccMutationCloneData:
    type: File
    outputSource: dna_secondary/ViccMutationCloneData

  CivicMutationCloneData:
    type: File
    outputSource: dna_secondary/CivicMutationCloneData

  ViccCNVCloneData:
    type: File
    outputSource: dna_secondary/ViccCNVCloneData

  CivicCNVCloneData:
    type: File
    outputSource: dna_secondary/CivicCNVCloneData

  # clone_tree_plot:
  #   type: File[]
  #   outputSource: dna_secondary/clone_tree_plot

  tree_summary_output:
    type: File
    outputSource: dna_secondary/tree_summary_output

steps:
  dna_secondary:
    in:
      SampleID: report_id
      Cellularity: facets_cellularity
      CytoBand: CytoBands
      CytobandsHG38Custom: CytobandsHG38Custom
      FacetsFile: facets_cncf
      FacetsSummary: facets_summary
      FacetsCNVAnnotateFile: FacetsCNVAnnotateFile
      MAF:
        valueFrom: $(self[0])
        source: annotated_variants
      MergedVCF: counted_vcf
      OncoFile:
        valueFrom: $(self[0])
        source: annotated_variants
      TumorBam: tumor_dna_final_bam
      IntervalFile: IntervalFile
    run: ../Workflows/DAPHNI-DNA-Secondary.cwl
    out:
    - cnv_vicc_out
    - mutation_vicc_out
    - cnv_civic_out
    - mutation_civic_out
    - ViccMutationCloneData
    - CivicMutationCloneData
    - ViccCNVCloneData
    - CivicCNVCloneData
    - ssm_output
    - cnv_output
    - cluster_gene_list_output
#     - clone_tree_plot
    - parsed_cnvs
    - tree_summary_output