#!/usr/bin/env cwl-runner
cwlVersion: v1.0

class: Workflow
label: facets
requirements:
  InlineJavascriptRequirement: {}
  StepInputExpressionRequirement: {}

inputs:
  MergedVCF: File
  FacetsFile: File
  Cellularity: File
  CytoBand: File
  OncoFile: File

steps:

  PhyloWGS-Parse:
    run: ../Tools/PhyloWGS/PhyloWGS-Parse.cwl
    in:
      InputFile: FacetsFile
      CNVFormat:
        default: "facets"
      Cellularity: Cellularity
    out:
      [parsed_cnvs]

  PhyloWGS-CreateInputs:
    run: ../Tools/PhyloWGS/PhyloWGS-CreateInputs.cwl
    in:
      ParsedCNVs: PhyloWGS-Parse/parsed_cnvs
      MergedVCF: MergedVCF
    out:
      [prepared_cnvs, prepared_variants]

  PhyloWGS-Multievolve:
    run: ../Tools/PhyloWGS/PhyloWGS-Multievolve.cwl
    in:
      SSMs: PhyloWGS-CreateInputs/prepared_variants
      CNVs: PhyloWGS-CreateInputs/prepared_cnvs
    out:
      [trees]

  PhyloWGS-Writeresults:
    run: ../Tools/PhyloWGS/PhyloWGS-Writeresults.cwl
    in:
      Sample:
        default: "sample1"
      ZipFile: PhyloWGS-Multievolve/trees
    out:
      [tree_summary_output, muts_output, mutass_output]


  PhyloWGS-Post-MultiClusteranalysis:
    run: ../Tools/PhyloWGS/PhyloWGS-Post-MultiClusteranalysis.cwl
    in:
      SummJson: PhyloWGS-Writeresults/tree_summary_output #.gz
      PhyloSSM: PhyloWGS-CreateInputs/prepared_variants
      PhyloCNV: PhyloWGS-CreateInputs/prepared_cnvs
      MultiTimepointsJson: PhyloWGS-Writeresults/muts_output #.gz
      MultiTimepointsZip: PhyloWGS-Writeresults/mutass_output #zip file
      CytoBand: CytoBand
      OncoFile: OncoFile
    out:
      [ssm_output, cnv_output, cluster_gene_list_output]


outputs:
  ssm_output:
    type: File
    outputSource: PhyloWGS-Post-MultiClusteranalysis/ssm_output

  cnv_output:
    type: File
    outputSource: PhyloWGS-Post-MultiClusteranalysis/cnv_output

  cluster_gene_list_output:
    type: File
    outputSource: PhyloWGS-Post-MultiClusteranalysis/cluster_gene_list_output

  parsed_cnvs:
    type: File
    outputSource: PhyloWGS-Parse/parsed_cnvs

  tree_summary_output:
    type: File
    outputSource: PhyloWGS-Writeresults/tree_summary_output


