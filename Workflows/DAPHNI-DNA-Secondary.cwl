#!/usr/bin/env cwl-runner 
cwlVersion: v1.0

class: Workflow
label: DAPHNI DNA Secondary Analysis
requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  SubworkflowFeatureRequirement: {}


inputs:
  MergedVCF: File
  FacetsFile: File
  Cellularity: File
  CytoBand: File
  CytobandsHG38Custom: File
  OncoFile: File
  MAF: File
  SampleID: string
  FacetsCNVAnnotateFile: File
  IntervalFile: File
  FacetsSummary: File


steps:
  PhyloWGS:
    run: ../Workflows/PhyloWGS.cwl
    in:
      MergedVCF: MergedVCF
      FacetsFile: FacetsFile
      Cellularity: Cellularity
      CytoBand: CytoBand
      OncoFile: OncoFile
    out:
      [ssm_output, cnv_output, cluster_gene_list_output, parsed_cnvs, tree_summary_output]

  Vicc-CNV:
    run: ../Tools/Vicctor.cwl
    in:
      SampleID: SampleID
      Input: FacetsCNVAnnotateFile
      Type:
        default: "cnv"
      Url:
        default: "elastic:changeme@ec2-3-90-34-223.compute-1.amazonaws.com"
    out:
      [vicc_out, civic_out]

  Vicc-Mutations:
    run: ../Tools/Vicctor.cwl
    in:
      SampleID: SampleID
      Input: MAF
      Type:
        default: "mutation"
      Url:
        default: "elastic:changeme@ec2-3-90-34-223.compute-1.amazonaws.com"
    out:
      [vicc_out, civic_out]

  CloneAnnotate:
    run: ../Tools/cloneannotate.cwl
    in:
      MutationVicc: Vicc-Mutations/vicc_out
      MutationCivic: Vicc-Mutations/civic_out
      CNVVicc: Vicc-CNV/vicc_out
      CNVCivic: Vicc-CNV/civic_out
      ClusterGeneList: PhyloWGS/cluster_gene_list_output
      CNVCloneTable: PhyloWGS/cnv_output
    out:
      [ViccMutationCloneData, CivicMutationCloneData, ViccCNVCloneData, CivicCNVCloneData]

  # PlotClones:
  #   run: ../Tools/ClonePlot.cwl
  #   in:
  #     SummJson: PhyloWGS/tree_summary_output #.gz
  #     DNA_VICC: CloneAnnotate/ViccMutationCloneData
  #     DNA_Civic: CloneAnnotate/CivicMutationCloneData
  #     CNV_VICC: CloneAnnotate/ViccCNVCloneData
  #     CNV_civic: CloneAnnotate/CivicCNVCloneData
  #   out:
  #     [clone_tree_plot]

outputs:

  tree_summary_output:
    type: File
    outputSource: PhyloWGS/tree_summary_output

  cnv_vicc_out:
    type: File
    outputSource: Vicc-CNV/vicc_out

  mutation_vicc_out:
    type: File
    outputSource: Vicc-Mutations/vicc_out

  cnv_civic_out:
    type: File
    outputSource: Vicc-CNV/civic_out

  mutation_civic_out:
    type: File
    outputSource: Vicc-Mutations/civic_out

  ssm_output:
    type: File
    outputSource: PhyloWGS/ssm_output

  cnv_output:
    type: File
    outputSource: PhyloWGS/cnv_output

  cluster_gene_list_output:
    type: File
    outputSource: PhyloWGS/cluster_gene_list_output

  # clone_tree_plot:
  #   type: File[]
  #   outputSource: PlotClones/clone_tree_plot

  parsed_cnvs:
    type: File
    outputSource: PhyloWGS/parsed_cnvs

  ViccMutationCloneData:
    type: File
    outputSource: CloneAnnotate/ViccMutationCloneData

  CivicMutationCloneData:
    type: File
    outputSource: CloneAnnotate/CivicMutationCloneData

  ViccCNVCloneData:
    type: File
    outputSource: CloneAnnotate/ViccCNVCloneData

  CivicCNVCloneData:
    type: File
    outputSource: CloneAnnotate/CivicCNVCloneData
