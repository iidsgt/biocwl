#!/usr/bin/env cwl-runner
cwlVersion: v1.0

class: Workflow
label: Subsample BAM to paired FASTQ

requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  ScatterFeatureRequirement: {}

inputs:
  InputBam: File
  Threads: int
  Region: string
  OutputFname: string

steps:
  SubsetBam:
    run: ../../Tools/Samtools/Samtools-View.cwl
    in:
      InputFile: InputBam
      Format:
        valueFrom: |
          ${ return {"BAM": true}}
      Threads: Threads
      Region: Region
    out:
      [alignment]
  
  Bam2Fastq:
    run: ../../Tools/Samtools/Samtools-Fastq.cwl
    in:
      InputFile: SubsetBam/alignment
      Threads: Threads
      Compression:
        default: gz
    out:
      [Forward_Reads, Reverse_Reads]

  RepairReads:
    run: ../../Tools/BBTools/BBTools-Repair.cwl
    in:
      ForwardReads: Bam2Fastq/Forward_Reads
      ReverseReads: Bam2Fastq/Reverse_Reads
      Compression:
        default: gz
      Output: OutputFname
    out:
      [Forward_Reads, Reverse_Reads]

outputs:
  Forward_Reads:
    type: File
    outputSource: RepairReads/Forward_Reads

  Reverse_Reads:
    type: File
    outputSource: RepairReads/Reverse_Reads

