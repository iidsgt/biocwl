#!/usr/bin/env cwl-runner
cwlVersion: v1.0

class: Workflow
label: DAPHNI RNA Secondary Analysis
requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}


inputs:
  report_id: string
  featurecounts: File
  raw_counts: File
  zscores: File
  foldchange_table: File
  Fusions: File
  merged_rna_rdata: File
  GeneConvTable: File
  PathwaysDNA-RNA: File
  PathwaysMixEnrich: File
  ProgenyGeneConversionTable: File
  batchTable: File
  lincsAnnotRData: File
  mmrfReferenceMatrix: File
  ParsedCNVs: File
  ExpressionFeaturesRem: File
  CNVFeatures: File
  CytobandsHG38Custom: File

steps:

  Vicc-Fusions:
    run: ../Tools/Vicctor.cwl
    in:
      SampleID: report_id
      Input: Fusions
      Type:
        default: "fusions"
      Url:
        default: "elastic:changeme@ec2-3-90-34-223.compute-1.amazonaws.com"
    out:
      [vicc_out, civic_out]

  Vicc-Expression:
    run: ../Tools/Vicctor.cwl
    in:
      SampleID: report_id
      Input: zscores
      Type:
        default: "expression"
      Url:
        default: "elastic:changeme@ec2-3-90-34-223.compute-1.amazonaws.com"
    out:
      [vicc_out, civic_out]

  RNACohort-ComputeRisk:
    run: ../Tools/RNACohort/RNACohort-ComputeRisk.cwl
    in:
      RData: merged_rna_rdata
      GeneConvTable: GeneConvTable
    out:
      [latest_gep_scores, latest_gep_scores_tsv]

  RNACohort-CreateLincs:
    run: ../Tools/RNACohort/RNACohort-CreateLincs.cwl
    in:
      SampleName: report_id
      ZScoreTable: zscores
      GeneConvTable: GeneConvTable
    out:
      [genes_down, genes_up]

  RNACohort-Drugs:
    run: ../Tools/RNACohort/RNACohort-PostToLincs1000.cwl
    in:
      UpGenes: RNACohort-CreateLincs/genes_up
      DownGenes: RNACohort-CreateLincs/genes_down
    out:
      [drug_targets]

  L1000CDS2PostProcessing:
    run: ../Tools/L1000CDS2PostProcessing.cwl
    in:
      L1000json: RNACohort-Drugs/drug_targets
      lincsAnnotRData: lincsAnnotRData
    out:
      [l1000_drug_list]

  psnInputGenerator:
    run: ../Tools/psnInputGenerator.cwl
    requirements:
      ResourceRequirement:
        ramMin: 6000      # [MB]
        coresMin: 1      # [cores]
        tmpdirMin: 6000  # [MB]
    in:
      rnaRDataFile: merged_rna_rdata
      sampleName: report_id
      mmrfReferenceTable: mmrfReferenceMatrix
      parsedCNVs: ParsedCNVs
      cnvFeatures: CNVFeatures
      expressionFeatures: ExpressionFeaturesRem
      cytoBand: CytobandsHG38Custom
    out:
      - fsqn_normalized_counts
      - per_band_cnvs
      - psn_cnv_inputs
      - psn_exprs_inputs

  PredictTranslocation:
    run: ../Tools/predict_translocations.cwl
    in:
      SampleExpression: psnInputGenerator/fsqn_normalized_counts
    out:
      [transloc_out]

  MixEnrich:
    run: ../Tools/MixEnrich.cwl
    in:
      SampleName: report_id
      RData: merged_rna_rdata
      Pathways: PathwaysMixEnrich
      GeneConvTable: GeneConvTable
    out:
      [MixEnrichTable]

  Progeny:
    run: ../Tools/Progeny.cwl
    in:
      RData: merged_rna_rdata
      GeneConvTable: ProgenyGeneConversionTable
    out:
      [progenyPathwaylatest]

outputs:

  fusions_vicc_out:
    type: File
    outputSource: Vicc-Fusions/vicc_out

  fusions_civic_out:
    type: File
    outputSource: Vicc-Fusions/civic_out

  expression_vicc_out:
    type: File
    outputSource: Vicc-Expression/vicc_out

  expression_civic_out:
    type: File
    outputSource: Vicc-Expression/civic_out

  Risk_Scores_RData:
    type: File
    outputSource: RNACohort-ComputeRisk/latest_gep_scores

  Risk_Scores_Tsv:
    type: File
    outputSource: RNACohort-ComputeRisk/latest_gep_scores_tsv

  Drug_Targets:
    type: File
    outputSource: RNACohort-Drugs/drug_targets

  MixEnrichTable:
    type: File
    outputSource: MixEnrich/MixEnrichTable

  progenyPathwaylatest:
    type: File[]
    outputSource: Progeny/progenyPathwaylatest

  transloc_out:
    type: File
    outputSource: PredictTranslocation/transloc_out

  l1000_drug_list:
    type: File
    outputSource: L1000CDS2PostProcessing/l1000_drug_list