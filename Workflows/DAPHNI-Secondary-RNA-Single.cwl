#!/usr/bin/env cwl-runner
cwlVersion: v1.1

class: Workflow
label: DAPHNI RNA+DNA Secondary Analyses
requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  ScatterFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  SubworkflowFeatureRequirement: {}
  ResourceRequirement:
    ramMin: 1000      # [MB]
    coresMin: 1        # [cores]
    tmpdirMin: 45000   # [MB]

inputs:
  # Metadata about this run. This should be the column name of the
  # outputs from the RNA merge step (see RNACohort-Merge.cwl).
  report_id: string

  # Inputs that don't change.
  CytoBands: File
  CytobandsHG38Custom: File
  GeneConversionTable: File
  PathwaysDNA-RNA: File
  PathwaysMixEnrich: File
  ProgenyGeneConversionTable: File
  CNVFeatures: File
  ExpressionFeaturesRem: File
  mmrfReferenceMatrix: File
  ParsedCNVs: File

  # Outputs from Primary.
  facets_cellularity: File
  facets_cncf: File
  counted_vcf: File
  annotated_variants: File[]
  tumor_dna_final_bam: File
  featurecounts: File
  FacetsCNVAnnotateFile: File
  Fusions: File
  RSEMGeneResults: File

  # Outputs from RNA Merge.
  raw_counts: File
  zscores: File
  foldchange_table: File
  merged_rna_rdata: File
  batchTable: File
  lincsAnnotRData: File
  
  
outputs:
  expression_vicc_out:
    type: File
    outputSource: rna_secondary/expression_vicc_out

  expression_civic_out:
    type: File
    outputSource: rna_secondary/expression_civic_out

  fusions_vicc_out:
    type: File
    outputSource: rna_secondary/fusions_vicc_out

  fusions_civic_out:
    type: File
    outputSource: rna_secondary/fusions_civic_out

  MixEnrichTable:
    type: File
    outputSource: rna_secondary/MixEnrichTable

  progenyPathwaylatest:
    type: File[]
    outputSource: rna_secondary/progenyPathwaylatest

  Risk_Scores_RData:
    type: File
    outputSource: rna_secondary/Risk_Scores_RData

  Risk_Scores_Tsv:
    type: File
    outputSource: rna_secondary/Risk_Scores_Tsv

  l1000_drug_list:
    type: File
    outputSource: rna_secondary/l1000_drug_list

steps:
  rna_secondary:
    in:
      report_id: report_id
      featurecounts: featurecounts
      raw_counts: raw_counts
      zscores: zscores
      foldchange_table: foldchange_table
      Fusions: Fusions
      merged_rna_rdata: merged_rna_rdata
      GeneConvTable: GeneConversionTable
      ProgenyGeneConversionTable: ProgenyGeneConversionTable
      PathwaysMixEnrich: PathwaysMixEnrich
      PathwaysDNA-RNA: PathwaysDNA-RNA
      batchTable: batchTable
      lincsAnnotRData: lincsAnnotRData
      mmrfReferenceMatrix: mmrfReferenceMatrix
      CNVFeatures: CNVFeatures
      CytobandsHG38Custom: CytobandsHG38Custom
      ParsedCNVs: ParsedCNVs
      ExpressionFeaturesRem: ExpressionFeaturesRem
    run: ../Workflows/DAPHNI-RNA-Secondary-FRONTIERS.cwl
    out:
    - fusions_civic_out
    - fusions_vicc_out
    - expression_civic_out
    - expression_vicc_out
    - Risk_Scores_RData
    - Risk_Scores_Tsv
    - MixEnrichTable
    - progenyPathwaylatest
    - transloc_out
    - l1000_drug_list