#!/usr/bin/env cwl-runner
cwlVersion: v1.0

class: Workflow
label: DAPHNI DNA Primary Post-Processing

requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  SubworkflowFeatureRequirement: {}

inputs:
  SampleId: string
  NormalBam: File
  TumorBam: File
  Reference: File
  KnownSiteVCF: File
  IntervalFile: File
  GenomicRegion: string[]
  DbSnp: File
  AnnotationDB: Directory
  Microsatellites: File
  BedFile: File
  DbSnp146: Any
  HapMap: Any
  Omni: Any
  1000G: Any
  FuncoDatasourceGermline: Directory
  Mills: Any
  axiomPoly: Any
  noChrBed: File
  GeneConversionTable: File
  TranscriptIDs: File

steps:
  GermlineShortVariantDiscoveryCohort:
    run: ../Workflows/GATK-GermlineShortVariantDiscovery-Cohort.cwl
    in:
      SampleID: SampleId
      InputBam: TumorBam
      Reference: Reference
      BedFile: BedFile
      Mills: Mills
      axiomPoly: axiomPoly
      DbSnp146: DbSnp146
      HapMap: HapMap
      Omni: Omni
      1000G: 1000G
      FuncoDatasourceGermline: FuncoDatasourceGermline
    out:
      [filteredVCF, genomics_db_workspace, genotyped_vcf, annotated_germline_variants,
       indel_recalibrated_vfc, snp_recalibrated_vfc, SNP_tranches, INDEL_tranches,
       SitesOnlyVcf, snp_recal_file, indel_recal_file]

  ConsensusVariant:
    run: DAPHNI-Consensus-Variant.cwl
    in:
      NormalBam: NormalBam
      TumorBam: TumorBam
      Reference: Reference
      IntervalFile: IntervalFile
      GenomicRegion: GenomicRegion
      AnnotationDB: AnnotationDB
    out:
      [consensus_vcf, read_counts, counted_vcf, annotated_variants]

  FacetsSNP:
    run: Facets-SNP.cwl
    in:
      noChrBed: noChrBed
      GeneConversionTable: GeneConversionTable
      TranscriptIDs: TranscriptIDs
      PairName: SampleId
      NormalBam: NormalBam
      TumorBam: TumorBam
      DbSnp: DbSnp
    out:
      [snp_pileup, genome_segments, diagnostics_plot, cncf, summary, flags, cellularity, facets_annotated, parsed_cnvs]

  MSI:
    run: ../Tools/MSIsensor.cwl
    requirements:
      ResourceRequirement:
        ramMin: 2000
        coresMin: 2
    in:
      Microsatellites: Microsatellites
      NormalBam: NormalBam
      TumorBam: TumorBam
    out:
      [msi_output, msi_output_dis_tab, msi_output_germline, msi_output_somatic]

  MutationalBurden:
    run: ../Tools/MutationalBurden.cwl
    in:
      ConsensusVariants:
        valueFrom: $(self[0])
        source: ConsensusVariant/annotated_variants
      IntervalsBed: IntervalFile
    out:
      [mutational_burden, mutation_type_table, tmb_details]

  ScarScore:
    run: ../Tools/scarScore.cwl
    in:
      FacetsCNCF: FacetsSNP/cncf
      FacetsSummary: FacetsSNP/summary
      sampleID:
        valueFrom: tumor
    out:
      [scar_results]


outputs:
  tmb_details:
    type: File
    outputSource: MutationalBurden/tmb_details

  facets_annotated:
    type: File
    outputSource: FacetsSNP/facets_annotated

  snp_pileup:
    type: File
    outputSource: FacetsSNP/snp_pileup

  consensus_vcf:
    type: File
    outputSource: ConsensusVariant/consensus_vcf

  annotated_variants:
    type: File[]
    outputSource: ConsensusVariant/annotated_variants

  read_counts:
    type: File
    outputSource: ConsensusVariant/read_counts

  counted_vcf:
    type: File
    outputSource: ConsensusVariant/counted_vcf

  facets_parsed_cnvs:
    type: File
    outputSource: FacetsSNP/parsed_cnvs

  facets_genome_segments:
    type: File
    outputSource: FacetsSNP/genome_segments

  facets_diagnostics_plot:
    type: File
    outputSource: FacetsSNP/diagnostics_plot

  facets_cncf:
    type: File
    outputSource: FacetsSNP/cncf

  facets_summary:
    type: File
    outputSource: FacetsSNP/summary

  facets_flags:
    type: File
    outputSource: FacetsSNP/flags

  facets_cellularity:
    type: File
    outputSource: FacetsSNP/cellularity

  msi_output:
    type: File
    outputSource: MSI/msi_output

  msi_output_dis_tab:
    type: File
    outputSource: MSI/msi_output_dis_tab

  msi_output_germline:
    type: File
    outputSource: MSI/msi_output_germline

  msi_output_somatic:
    type: File
    outputSource: MSI/msi_output_somatic

  scar_results:
    type: File
    outputSource: ScarScore/scar_results

  genomics_db_workspace:
    type: Directory
    outputSource: GermlineShortVariantDiscoveryCohort/genomics_db_workspace

  genotyped_vcf:
    type: File
    outputSource: GermlineShortVariantDiscoveryCohort/genotyped_vcf

  annotated_germline_variants:
    type: File[]
    outputSource: GermlineShortVariantDiscoveryCohort/annotated_germline_variants

  SNP_tranches:
    type: File
    outputSource: GermlineShortVariantDiscoveryCohort/SNP_tranches

  INDEL_tranches:
    type: File
    outputSource: GermlineShortVariantDiscoveryCohort/INDEL_tranches

  indel_recalibrated_vfc:
    type: File
    outputSource: GermlineShortVariantDiscoveryCohort/indel_recalibrated_vfc

  snp_recalibrated_vfc:
    type: File
    outputSource: GermlineShortVariantDiscoveryCohort/snp_recalibrated_vfc

  indel_recal_file:
    type: File
    outputSource: GermlineShortVariantDiscoveryCohort/indel_recal_file

  snp_recal_file:
    type: File
    outputSource: GermlineShortVariantDiscoveryCohort/snp_recal_file


  filteredVCF:
    type: File
    outputSource: GermlineShortVariantDiscoveryCohort/filteredVCF

  SitesOnlyVcf:
    type: File
    outputSource: GermlineShortVariantDiscoveryCohort/SitesOnlyVcf