#!/usr/bin/env cwl-runner
cwlVersion: v1.1

class: Workflow
label: DAPHNI RNA+DNA Secondary Analyses
requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  ScatterFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  SubworkflowFeatureRequirement: {}
  ResourceRequirement:
    ramMin: 2000      # [MB]
    coresMin: 1        # [cores]
    tmpdirMin: 45000   # [MB]


inputs:
  # Metadata about this run. This should be the column name of the
  # outputs from the RNA merge step (see RNACohort-Merge.cwl).
  report_id: string

  # clinical metadata for the report
  patient_name: string
  patient_mrn: string
  patient_dob: string
  patient_sex: string
  date_sample_collected: string

  # Inputs that don't change.
  CytoBands: File
  CytobandsHG38Custom: File
  GeneConversionTable: File
  PathwaysDNA-RNA: File
  PathwaysMixEnrich: File
  ProgenyGeneConversionTable: File
  CNVFeatures: File
  ExpressionFeaturesRem: File
  IntervalFile: File
  bostonDist: File
  mmrfReferenceMatrix: File

  # PredEngine Refs
  prognosticMarkersTable: File
  daphniDrugTableSourcesAnnotated: File
  daphniMutationCensus: File
  daphniTier1Biomarkers: File
  hg38CytobandCoordinatesPerArm: File

  # SpinReport Refs
  hg38CytobandCoordinates: File
  hg38GeneCoordinatesTxDB: File
  mmrfMmpsnSubgroupSurvival: File

  # Outputs from Primary.
  facets_cellularity: File
  facets_cncf: File
  facets_summary: File
  counted_vcf: File
  annotated_variants: File[]
  tumor_dna_final_bam: File
  featurecounts: File
  FacetsCNVAnnotateFile: File
  Fusions: File
  MSIOut: File
  multiqc_zip: File

  # Outputs from DNA Merge
  msiTableLatest: File
  scarTableLatest: File
  tmbTableLatest: File

  # Outputs from RNA Merge.
  raw_counts: File
  zscores: File
  merged_rna_rdata: File
  lincsAnnotRData: File
  
outputs:
  cluster_gene_list_output:
    type: File
    outputSource: dna_secondary/cluster_gene_list_output

  cnv_output:
    type: File
    outputSource: dna_secondary/cnv_output

  ssm_output:
    type: File
    outputSource: dna_secondary/ssm_output

  cnv_vicc_out:
    type: File
    outputSource: dna_secondary/cnv_vicc_out

  mutation_vicc_out:
    type: File
    outputSource: dna_secondary/mutation_vicc_out

  cnv_civic_out:
    type: File
    outputSource: dna_secondary/cnv_civic_out

  mutation_civic_out:
    type: File
    outputSource: dna_secondary/mutation_civic_out

  ViccMutationCloneData:
    type: File
    outputSource: dna_secondary/ViccMutationCloneData

  CivicMutationCloneData:
    type: File
    outputSource: dna_secondary/CivicMutationCloneData

  ViccCNVCloneData:
    type: File
    outputSource: dna_secondary/ViccCNVCloneData

  CivicCNVCloneData:
    type: File
    outputSource: dna_secondary/CivicCNVCloneData

  # clone_tree_plot:
  #   type: File[]
  #   outputSource: dna_secondary/clone_tree_plot

  tree_summary_output:
    type: File
    outputSource: dna_secondary/tree_summary_output

  expression_vicc_out:
    type: File
    outputSource: rna_secondary/expression_vicc_out

  expression_civic_out:
    type: File
    outputSource: rna_secondary/expression_civic_out

  fusions_vicc_out:
    type: File
    outputSource: rna_secondary/fusions_vicc_out

  fusions_civic_out:
    type: File
    outputSource: rna_secondary/fusions_civic_out

  # MixEnrichTable:
  #   type: File
  #   outputSource: rna_secondary/MixEnrichTable

  progenyPathwaylatest:
    type: File[]
    outputSource: rna_secondary/progenyPathwaylatest

  selineScoreResults:
    type: File
    outputSource: rna_secondary/selineScoreResults
  
  Risk_Scores_RData:
    type: File
    outputSource: rna_secondary/Risk_Scores_RData

  Risk_Scores_Tsv:
    type: File
    outputSource: rna_secondary/Risk_Scores_Tsv

#  l1000_drug_list:
#    type: File
#    outputSource: rna_secondary/l1000_drug_list


  per_band_cnvs:
    type: File
    outputSource: rna_secondary/per_band_cnvs

  Predicted_Classes:
    type: File
    outputSource: rna_secondary/Predicted_Classes

  all_drug_details_ranked:
    type: File
    outputSource: PredictionEngine/all_drug_details_ranked

  variant_details:
    type: File
    outputSource: PredictionEngine/variant_details

  variant_summary_ranked:
    type: File
    outputSource: PredictionEngine/variant_summary_ranked

  somatic_mutation:
    type: File
    outputSource: PredictionEngine/somatic_mutation

  report:
    type: File
    outputSource: spin_report/report

  figures:
    type: File[]
    outputSource: spin_report/figures

  kables:
    type: File[]
    outputSource: spin_report/kables
    
  translocations:
    type: File
    outputSource: rna_secondary/transloc_out

  mmHallMarks:
    type: File
    outputSource: rna_secondary/mmHallMarks

steps:
  dna_secondary:
    in:
      SampleID: report_id
      Cellularity: facets_cellularity
      CytoBand: CytoBands
      CytobandsHG38Custom: CytobandsHG38Custom
      FacetsFile: facets_cncf
      FacetsSummary: facets_summary
      FacetsCNVAnnotateFile: FacetsCNVAnnotateFile
      MAF:
        valueFrom: $(self[0])
        source: annotated_variants
      MergedVCF: counted_vcf
      OncoFile:
        valueFrom: $(self[0])
        source: annotated_variants
      IntervalFile: IntervalFile
    run: ../Workflows/DAPHNI-DNA-Secondary.cwl
    out:
    - cnv_vicc_out
    - mutation_vicc_out
    - cnv_civic_out
    - mutation_civic_out
    - ViccMutationCloneData
    - CivicMutationCloneData
    - ViccCNVCloneData
    - CivicCNVCloneData
    - ssm_output
    - cnv_output
    - cluster_gene_list_output
    - parsed_cnvs
    - tree_summary_output
  rna_secondary:
    in:
      report_id: report_id
      featurecounts: featurecounts
      raw_counts: raw_counts
      zscores: zscores
      Fusions: Fusions
      merged_rna_rdata: merged_rna_rdata
      GeneConvTable: GeneConversionTable
      ProgenyGeneConversionTable: ProgenyGeneConversionTable
      PathwaysMixEnrich: PathwaysMixEnrich
      PathwaysDNA-RNA: PathwaysDNA-RNA
      lincsAnnotRData: lincsAnnotRData
      ParsedCNVs: dna_secondary/parsed_cnvs
      CNVFeatures: CNVFeatures
      CytobandsHG38Custom: CytobandsHG38Custom
      FacetsCNVAnnotateFile: FacetsCNVAnnotateFile
      ExpressionFeaturesRem: ExpressionFeaturesRem
      annotated_variants:
        valueFrom: $(self[0])
        source: annotated_variants
      bostonDist: bostonDist
      mmrfReferenceMatrix: mmrfReferenceMatrix
      scarTableLatest: scarTableLatest
      prognosticMarkersTable: prognosticMarkersTable
    run: ../Workflows/DAPHNI-RNA-Secondary.cwl
    out:
      - fusions_civic_out
      - fusions_vicc_out
      - expression_civic_out
      - expression_vicc_out
      - Risk_Scores_RData
      - Risk_Scores_Tsv
      # - MixEnrichTable
      - progenyPathwaylatest
      - transloc_out
      - Predicted_Classes
      - per_band_cnvs
#      - DNAPathwaysOut
#      - DNAPathwaysTableOut
#      - predicted_out_ccle
#      - IC50s
      - selineScoreResults
      - mmHallMarks
  PredictionEngine:
    requirements:
      ResourceRequirement:
        ramMin: 10000
        coresMin: 2
        tmpdirMin: 10000
    in:
      sampleID: report_id
      somMutFile:
        valueFrom: $(self[0])
        source: annotated_variants
      somMutFileVICC: dna_secondary/ViccMutationCloneData
      somMutCIVIC: dna_secondary/CivicMutationCloneData
      cnaFile: FacetsCNVAnnotateFile
      cnaFileVICC: dna_secondary/ViccCNVCloneData
      cnaFileCIVIC: dna_secondary/CivicCNVCloneData
      geneExprFileVICC: rna_secondary/expression_vicc_out
      geneExprFileCIVIC: rna_secondary/expression_civic_out
      scarFile: scarTableLatest
      treeFile: dna_secondary/tree_summary_output
      zScoreTable: zscores
      selineScoresFile: rna_secondary/selineScoreResults
      daphniBiomarkerTable: daphniTier1Biomarkers
      daphniDrugTable: daphniDrugTableSourcesAnnotated
      daphniMutationCensus: daphniMutationCensus
      mmPSNFile: rna_secondary/Predicted_Classes
      mmHallMarks: rna_secondary/mmHallMarks
    run: ../Tools/predEngine.cwl
    out:
      - all_drug_details_ranked
      - variant_details
      - variant_summary_ranked
      - somatic_mutation
      - cna
      - expression
  spin_report:
    requirements:
      ResourceRequirement:
        ramMin: 10000
        coresMin: 2
        tmpdirMin: 10000
    in:
      tiers:
        default: "1A,1B"
      sampleName: report_id
      patientName: patient_name
      MRN: patient_mrn
      DOB: patient_dob
      dateSampleCollected: date_sample_collected
      geneCoordinates: hg38GeneCoordinatesTxDB
      cytobandCoordinates: hg38CytobandCoordinates
      psnReferenceTable: mmrfMmpsnSubgroupSurvival
      sexChrs: patient_sex
      drugDetailsRanked: PredictionEngine/all_drug_details_ranked
      variantDetailsTable: PredictionEngine/variant_details
      variantSupportSummary: PredictionEngine/variant_summary_ranked
      prognosticMarkerSummary: rna_secondary/mmHallMarks
      zScoreTable: zscores
      seliFile: rna_secondary/selineScoreResults
      gep70File: rna_secondary/Risk_Scores_Tsv
      somFile: PredictionEngine/somatic_mutation
      cnaFile: PredictionEngine/cna
      exprFile: PredictionEngine/expression
      msiFile: msiTableLatest
      tmbFile: tmbTableLatest
      scarFile: scarTableLatest
      facetsCNCFResultsFile: facets_cncf
      tumorPurityPloidy: facets_summary
      treeFile: dna_secondary/tree_summary_output
      mmPSNFile: rna_secondary/Predicted_Classes
      multiQCFile: multiqc_zip
    run: ../Tools/spinStaticReport.cwl
    out:
      - report
      - figures
      - kables
