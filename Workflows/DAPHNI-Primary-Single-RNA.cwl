#!/usr/bin/env cwl-runner
cwlVersion: v1.1

class: Workflow
label: DAPHNI RNA Primary Analyses
requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  ScatterFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  SubworkflowFeatureRequirement: {}
  ResourceRequirement:
    ramMin: 1000      # [MB]
    coresMin: 1        # [cores]
    tmpdirMin: 45000   # [MB]
$namespaces:
  arv: http://arvados.org/cwl#
  cwltool: http://commonwl.org/cwltool#


inputs:
  AnnotationDB: Directory
  AssemblyFasta: File
  BWA-Index: File
  BlackList: File
  CytoBands: File
  DbSnp: File
  GeneConversionTable: File
  noChrBed: File
  TranscriptIDs: File
  GenomicRegion: string[]
  IntervalFile: File
  KnownSiteVCF: File
  LenFile: File
  MaxThreads: int
  Microsatellites: File
  ProteinDomains: File
  STARGtf: File
  STARIndex: Directory
  TumorRNAForwardReads: File[]
  TumorRNAReverseReads: File[]
  isTestSampleInput: boolean?
  Mills: Any
  axiomPoly: Any
  HapMap: Any
  Omni: Any
  1000G: Any
  DbSnp146: Any
  FuncoDatasourceGermline: Directory
  indices_folder: Directory

outputs:
  tumor_rna_Final_Bam:
    type: File
    outputSource: tumor_rna_pre/Final_Bam
  tumor_rna_Final_Index:
    type: File
    outputSource: tumor_rna_pre/Final_Index
  tumor_rna_Fusions:
    type: File
    outputSource: tumor_rna_pre/Fusions
  tumor_rna_FusionsPDF:
    type: File
    outputSource: tumor_rna_pre/FusionsPDF
  tumor_rna_MarkDuplicates_Metrics:
    type: File
    outputSource: tumor_rna_pre/MarkDuplicates_Metrics
  tumor_rna_Unmapped_Reads1:
    type: File
    outputSource: tumor_rna_pre/Unmapped_Reads1
  tumor_rna_Unmapped_Reads2:
    type: File
    outputSource: tumor_rna_pre/Unmapped_Reads2
  tumor_rna_featurecounts:
    type: File
    outputSource: tumor_rna_post/featurecounts
  rsem_gene_results_file:
    type: File
    outputSource: RSEM/gene_results_file
  multiqc_html:
    type: File
    outputSource: multiQCReport/multiqc_html

steps:
  tumor_rna_pre:
    in:
      AssemblyFasta: AssemblyFasta
      BlackList: BlackList
      CytoBands: CytoBands
      ForwardReads: TumorRNAForwardReads
      LenFile: LenFile
      ProteinDomains: ProteinDomains
      ReverseReads: TumorRNAReverseReads
      STARGtf: STARGtf
      STARIndex: STARIndex
      Threads: MaxThreads
    run: ../Workflows/DAPHNI-RNA-Pre.cwl
    out:
    - Final_Bam
    - Final_Index
    - MarkDuplicates_Metrics
    - Unmapped_Reads1
    - Unmapped_Reads2
    - Fusions
    - FusionsDiscarded
    - FusionsPDF
    - rna_fastp_json
    - starLog
  RSEM:
    run: ../Tools/rsem-calculate-expression.cwl
    in:
      upstream_read_file:
        valueFrom: $(self[0])
        source: TumorRNAForwardReads
#      downstream_read_file: TumorRNAReverseReads
      downstream_read_file:
        valueFrom: $(self[0])
        source: TumorRNAReverseReads
      indices_folder: indices_folder
#      output_filename:
#        valueFrom: tumor
    out:
      [ gene_results_file ]



  tumor_rna_post:
    in:
      Annotation: STARGtf
      InputBam: tumor_rna_pre/Final_Bam
      Threads: MaxThreads
    run: ../Workflows/DAPHNI-RNA-Post.cwl
    out:
    - featurecounts

  multiQCReport:
    in:
      qc_files_array: [tumor_rna_pre/rna_fastp_json, tumor_rna_pre/starLog]
    run: ../Tools/multiQC.cwl
    out:
    - multiqc_html