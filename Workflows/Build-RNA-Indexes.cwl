#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: Workflow
label: WIP

requirements:
  InlineJavascriptRequirement: {}

inputs:
  IndexName: string
  FastaFiles: File[]
  GtfFile: File

steps:

  build_hisat2_index:
    run: ../Tools/Hisat2-Index.cwl
    in:
      InputFiles:
        source: FastaFiles
        valueFrom: | 
          ${return {"fasta": self};}

      IndexName: IndexName
    
    out: [indexes]

  build_kallisto_index:
    run: ../Tools/Kallisto/Kallisto-Index.cwl
    in:
      InputFiles: FastaFiles
      IndexName: IndexName

    out: [index]

  build_star_index:
    run: ../Tools/STAR/STAR-Index.cwl
    requirements:
      ResourceRequirement:
        ramMin: 90000
        coresMin: 16
        tmpdirMin: 150000
    in:
      InputFiles: FastaFiles
      IndexName: IndexName
      Gtf: GtfFile
    out: [indexes]

outputs: 
  hisat2_index:
    type: File[]
    outputSource: build_hisat2_index/indexes

  kallisto_index:
    type: File
    outputSource: build_kallisto_index/index

  star_index:
    type: Directory
    outputSource: build_star_index/indexes