#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: Workflow
label: DAPHNI QC Pipeline

requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  ScatterFeatureRequirement: {}
  SubworkflowFeatureRequirement: {}

inputs:
  NormalDNAForwardReads: File[]
  NormalDNAReverseReads: File[]
  TumorDNAForwardReads: File[]
  TumorDNAReverseReads: File[]
  MaxThreads: int
  isTestSampleInput: boolean?

steps:
  cat_normal_forward_reads:
    run: ../Tools/CatFastqs.cwl
    in:
      Files: NormalDNAForwardReads
    out:
      [File]

  cat_normal_reverse_reads:
    run: ../Tools/CatFastqs.cwl
    in:
      Files: NormalDNAReverseReads
    out:
      [File]

  cat_tumor_forward_reads:
    run: ../Tools/CatFastqs.cwl
    in:
      Files: TumorDNAForwardReads
    out:
      [File]

  cat_tumor_reverse_reads:
    run: ../Tools/CatFastqs.cwl
    in:
      Files: TumorDNAReverseReads
    out:
      [File]
  
  fastp_normal:
    run: ../Tools/Fastp-preprocessFASTQ.cwl
    in:
      fastq1:
        source: [cat_normal_forward_reads/File]
      fastq2:
        source: [cat_normal_reverse_reads/File]
    out:
      [out_fastq1, out_fastq2, html_report, json_report]
  
  fastp_tumor:
    run: ../Tools/Fastp-preprocessFASTQ.cwl
    in:
      fastq1:
        source: [cat_tumor_forward_reads/File]
      fastq2:
        source: [cat_tumor_reverse_reads/File]
    out:
      [out_fastq1, out_fastq2, html_report, json_report]

  checkmate:
    run: ../Tools/NGSCheckmate/NGSCheckmate-Fastq-Pair.cwl
    in:
      NormalFastq:
        source: [fastp_normal/out_fastq1, fastp_normal/out_fastq2]
      TumorFastq:
        source: [cat_tumor_forward_reads/File, cat_tumor_reverse_reads/File]
      Threads: MaxThreads
    out:
      [data_dir]

  check_sample_pairs:
    run: ../Tools/Check-Sample-Matches.cwl
    in:
      InputFile: checkmate/data_dir
      isTestSampleInput: isTestSampleInput
    out:
      [isMatched]

outputs:
  normal_dna_forward_reads:
    type: File
    outputSource: cat_normal_forward_reads/File

  normal_dna_reverse_reads:
    type: File
    outputSource: cat_normal_reverse_reads/File
  
  tumor_dna_forward_reads:
    type: File
    outputSource: cat_tumor_forward_reads/File

  tumor_dna_reverse_reads:
    type: File
    outputSource: cat_tumor_reverse_reads/File

  # trimmed & corrected fastqs
  normal_dna_forward_reads_trimmed:
    type: File
    outputSource: fastp_normal/out_fastq1

  normal_dna_reverse_reads_trimmed:
    type: File
    outputSource: fastp_normal/out_fastq2
  
  tumor_dna_forward_reads_trimmed:
    type: File
    outputSource: cat_tumor_forward_reads/File

  tumor_dna_reverse_reads_trimmed:
    type: File
    outputSource: cat_tumor_reverse_reads/File

  isMatched:
    type: File
    outputSource: check_sample_pairs/isMatched
  
  # fastp metrics
  tumor_fastp_json:
    type: File
    outputSource: fastp_tumor/json_report
  
  normal_fastp_json:
    type: File
    outputSource: fastp_normal/json_report