#!/usr/bin/env cwl-runner
cwlVersion: v1.0

class: Workflow
label: DAPHNI DNA Primary Post-Processing

requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  SubworkflowFeatureRequirement: {}

inputs:
  SampleId: string
  NormalBam: File
  TumorBam: File
  Reference: File
  KnownSiteVCF: File
  IntervalFile: File
  GenomicRegion: string[]
  DbSnp: File
  AnnotationDB: Directory
  Microsatellites: File
  BedFile: File
  FuncoDatasourceGermline: Directory
  noChrBed: File
  GeneConversionTable: File
  TranscriptIDs: File
  Mills: File
  HapMap: File

steps:
  GermlineShortVariantDiscoverySingle-Tumor:
    run: ../Workflows/GATK-GermlineShortVariantDiscovery-Single-Sample.cwl
    in:
      SampleID: SampleId
      InputBam: TumorBam
      Reference: Reference
      Intervals: BedFile
      Mills: Mills
      HapMap: HapMap
      FuncoDatasourceGermline: FuncoDatasourceGermline
      OutputFilename:
        default: "annotated_variants.germline.Tumor.vcf"
    out:
      [annotateGermlineVCF]

  ConsensusVariant:
    run: DAPHNI-Consensus-Variant.cwl
    in:
      NormalBam: NormalBam
      TumorBam: TumorBam
      Reference: Reference
      IntervalFile: IntervalFile
      GenomicRegion: GenomicRegion
      AnnotationDB: AnnotationDB
    out:
      [counted_unfiltered_vcf, consensus_vcf, read_counts, counted_vcf, annotated_variants]

  GermlineShortVariantDiscoverySingle-Normal:
    run: ../Workflows/GATK-GermlineShortVariantDiscovery-Single-Sample.cwl
    in:
      SampleID: SampleId
      InputBam: NormalBam
      Reference: Reference
      Intervals: BedFile
      Mills: Mills
      HapMap: HapMap
      FuncoDatasourceGermline: FuncoDatasourceGermline
      OutputFilename:
        default: "annotated_variants.germline.Normal.vcf"
    out:
      [annotateGermlineVCF]

  FacetsSNP:
    run: Facets-SNP.cwl
    in:
      noChrBed: noChrBed
      GeneConversionTable: GeneConversionTable
      TranscriptIDs: TranscriptIDs
      PairName: SampleId
      NormalBam: NormalBam
      TumorBam: TumorBam
      DbSnp: DbSnp
    out:
      [snp_pileup, genome_segments, diagnostics_plot, cncf, summary, flags, cellularity, facets_annotated, parsed_cnvs]

  MSI:
    run: ../Tools/MSIsensor.cwl
    requirements:
      ResourceRequirement:
        ramMin: 2000
        coresMin: 2
    in:
      Microsatellites: Microsatellites
      NormalBam: NormalBam
      TumorBam: TumorBam
    out:
      [msi_output, msi_output_dis_tab, msi_output_germline, msi_output_somatic]
  MutationalBurden:
    run: ../Tools/MutationalBurden.cwl
    in:
      ConsensusVariants:
        valueFrom: $(self[0])
        source: ConsensusVariant/annotated_variants
      IntervalsBed: IntervalFile
    out:
      [mutational_burden, mutation_type_table, tmb_details]

  ScarScore:
    run: ../Tools/scarScore.cwl
    in:
      FacetsCNCF: FacetsSNP/cncf
      FacetsSummary: FacetsSNP/summary
      sampleID:
        valueFrom: tumor
    out:
      [scar_results]

outputs:
  facets_annotated:
    type: File
    outputSource: FacetsSNP/facets_annotated

  snp_pileup:
    type: File
    outputSource: FacetsSNP/snp_pileup

  consensus_vcf:
    type: File
    outputSource: ConsensusVariant/consensus_vcf

  annotated_variants:
    type: File[]
    outputSource: ConsensusVariant/annotated_variants

  read_counts:
    type: File
    outputSource: ConsensusVariant/read_counts

  counted_vcf:
    type: File
    outputSource: ConsensusVariant/counted_vcf
  
  counted_unfiltered_vcf:
    type: File
    outputSource: ConsensusVariant/counted_unfiltered_vcf

  facets_genome_segments:
    type: File
    outputSource: FacetsSNP/genome_segments

  facets_diagnostics_plot:
    type: File
    outputSource: FacetsSNP/diagnostics_plot

  facets_cncf:
    type: File
    outputSource: FacetsSNP/cncf

  facets_summary:
    type: File
    outputSource: FacetsSNP/summary

  facets_flags:
    type: File
    outputSource: FacetsSNP/flags

  facets_parsed_cnvs:
    type: File
    outputSource: FacetsSNP/parsed_cnvs

  facets_cellularity:
    type: File
    outputSource: FacetsSNP/cellularity

  mutational_burden:
    type: File
    outputSource: MutationalBurden/mutational_burden

  mutation_type_table:
    type: File
    outputSource: MutationalBurden/mutation_type_table

  tmb_details:
    type: File
    outputSource: MutationalBurden/tmb_details

  msi_output:
    type: File
    outputSource: MSI/msi_output

  msi_output_dis_tab:
    type: File
    outputSource: MSI/msi_output_dis_tab

  msi_output_germline:
    type: File
    outputSource: MSI/msi_output_germline

  msi_output_somatic:
    type: File
    outputSource: MSI/msi_output_somatic

  scar_results:
    type: File
    outputSource: ScarScore/scar_results

  annotateGermlineVCFTumor:
    type: File[]
    outputSource: GermlineShortVariantDiscoverySingle-Tumor/annotateGermlineVCF

  annotateGermlineVCFNormal:
    type: File[]
    outputSource: GermlineShortVariantDiscoverySingle-Normal/annotateGermlineVCF