#!/usr/bin/env cwl-runner
cwlVersion: v1.0

class: Workflow
label: Run Lancet on one chromosome

requirements:
  SubworkflowFeatureRequirement: {}
# $namespaces:
#   arv: "http://arvados.org/cwl#"
#   cwltool: "http://commonwl.org/cwltool#"
# hints:
#   arv:RunInSingleContainer: {}

inputs:
  TumorBam: File
  NormalBam: File
  Reference: File
  GenomicRegion: string
  IntervalFile: File

steps:
  SubsetBedFile:
    run: SubsetBedFile.cwl
    in:
      Bed: IntervalFile
      Chromosome: GenomicRegion
    out:
      [SubsettedBed]

  Lancet:
    run: ../Tools/Lancet.cwl
    in:
      NormalInput: NormalBam
      TumorInput: TumorBam
      Reference: Reference
      BedFile: SubsetBedFile/SubsettedBed
    out:
      [vcf]

outputs:
  vcf:
    type: File
    outputSource: Lancet/vcf
