#!/usr/bin/env cwl-runner
cwlVersion: v1.0

class: Workflow
label: DAPHNI Consensus Variant Calling

requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  ScatterFeatureRequirement: {}
  SubworkflowFeatureRequirement: {}
$namespaces:
  arv: http://arvados.org/cwl#
  cwltool: http://commonwl.org/cwltool#

inputs:
  NormalBam: File
  TumorBam: File
  Reference: File
  IntervalFile: File
  GenomicRegion: string[]
  AnnotationDB: Directory

steps:
  #Scattered
  Mutect2:
    run: ../Tools/GATK/GATK-Mutect2.cwl
    requirements:
      ResourceRequirement:
        coresMin: 8
        ramMin: 20000
        tmpdirMin: 10000
    in:
      NormalInput: NormalBam
      TumorInput: TumorBam
      Reference: Reference
      IntervalList: IntervalFile
    out:
      [vcf]

  FilterMutect2:
    run: ../Tools/GATK/GATK-FilterMutectCalls.cwl
    in:
      InputFile: Mutect2/vcf
      Reference: Reference
    out:
      [filteredVCF]

  UnzipMutect:
    run: ../Tools/Gunzip.cwl
    in:
      InputFile: FilterMutect2/filteredVCF
    out:
      [unzipped_file]

  Lancet:
    run: SingleChrLancet.cwl
    requirements:
      ResourceRequirement:
        coresMin: 8
        ramMin: 20000
        tmpdirMin: 10000
    scatter: GenomicRegion
    in:
      NormalBam: NormalBam
      TumorBam: TumorBam
      Reference: Reference
      GenomicRegion: GenomicRegion
      IntervalFile: IntervalFile
    out:
      [vcf]

  UpdateVCFDict:
    run: ../Tools/GATK/GATK-UpdateVCFSequenceDictionary.cwl
    scatter: VariantFile
    in:
      VariantFile: Lancet/vcf
      Reference: Reference
    out:
      [vcf]

  MergeLancet:
    run: ../Tools/GATK/GATK-GatherVCFs.cwl
    in:
      VariantFiles: UpdateVCFDict/vcf
      Output:
        default: 'lancet.merged.vcf'
    out:
      [merged_vcf]

  Manta:
    run: ../Tools/Manta.cwl
    requirements:
      ResourceRequirement:
        coresMin: 3
        ramMin: 2040
        tmpdirMin: 5000
    in:
      NormalInput: NormalBam
      TumorInput: TumorBam
      Reference: Reference
    out:
      [candidate_indels]

  Strelka:
    run: ../Tools/Strelka2.cwl
    requirements:
      ResourceRequirement:
        coresMin: 2
        ramMin: 5000
    in:
      NormalInput: NormalBam
      TumorInput: TumorBam
      Reference: Reference
      IndelCandidates: Manta/candidate_indels
      CallRegions: IntervalFile
    out:
      [snv_vcf, indel_vcf]

  UnzipStrelkaIndels:
    run: ../Tools/Gunzip.cwl
    in:
      InputFile: Strelka/indel_vcf
    out:
      [unzipped_file]

  UnzipStrelkaSNV:
    run: ../Tools/Gunzip.cwl
    in:
      InputFile: Strelka/snv_vcf
    out:
      [unzipped_file]

  Consensus:
    run: ../Tools/Farnsworth.cwl
    in:
      InputFiles: [UnzipMutect/unzipped_file, UnzipStrelkaSNV/unzipped_file, UnzipStrelkaIndels/unzipped_file, MergeLancet/merged_vcf]
    out:
      [consensus_vcf, regions]

  ReadCounts:
    run: ../Tools/Bamreadcount.cwl
    in:
      InputFile: TumorBam
      SiteList: Consensus/regions
      Reference: Reference
    out:
      [read_counts]

  MergeCountsVCF:
    run: ../Tools/VT/VT-VCFReadCountAnnotator.cwl
    in:
      ReadCounts: ReadCounts/read_counts
      InputVCF: Consensus/consensus_vcf
      NucleotideType:
        default: DNA
      SampleName:
        default: tumor
    out:
      [vcf]

  FilterConsensusVCF:
    run: ../Tools/filterConsensusVCF.cwl
    in:
      ConsensusVariants: MergeCountsVCF/vcf
    out:
      [filteredConsensusVCF]

  UnzipFilterConsensusVCF:
    run: ../Tools/Gunzip.cwl
    in:
      InputFile: FilterConsensusVCF/filteredConsensusVCF
    out:
      [unzipped_file]

  AnnotateVCF:
    run: ../Tools/GATK/GATK-Funcotator.cwl
    requirements:
      ResourceRequirement:
        coresMin: 2
        ramMin: 5000
    in:
      InputVCF: UnzipFilterConsensusVCF/unzipped_file
      DataSources: AnnotationDB
      Output:
        default: "annotated_variants.consensus.vcf"
      OutputFormat:
        default: MAF
      Reference: Reference
      RefVersion:
        default: hg38
    out:
      [annotated_variants]

outputs:
  counted_unfiltered_vcf:
    type: File
    outputSource: MergeCountsVCF/vcf
    
  consensus_vcf:
    type: File
    outputSource: Consensus/consensus_vcf

  read_counts:
    type: File
    outputSource: ReadCounts/read_counts

  counted_vcf:
    type: File
    outputSource: FilterConsensusVCF/filteredConsensusVCF

  annotated_variants:
    type: File[]
    outputSource: AnnotateVCF/annotated_variants
