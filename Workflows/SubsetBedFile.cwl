#!/usr/bin/env cwl-runner
cwlVersion: v1.0

class: Workflow
label: Subset BED file

requirements:
  SubworkflowFeatureRequirement: {}

inputs:
  Bed: File
  Chromosome: string

outputs:
  SubsettedBed:
    type: File
    outputSource: TabixIndex/File
    secondaryFiles:
    - .gz
    - .gz.tbi

steps:
  SortBed:
    run: ../Tools/Bed/SortBed.cwl
    in:
      Bed: Bed
    out:
      [File]

  BedExtract:
    run: ../Tools/Bed/BedExtract.cwl
    in:
      Bed: SortBed/File
      Chromosome: Chromosome
    out:
      [File]

  ZipBed:
    run: ../Tools/Bgzip.cwl
    in:
      In: BedExtract/File
    out:
      [Out]

  TabixIndex:
    run: ../Tools/Bed/Tabix.cwl
    in:
      Bed: ZipBed/Out
    out:
      [File]
