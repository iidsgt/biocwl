#!/usr/bin/env cwl-runner
cwlVersion: v1.0

class: Workflow
label: DAPHNI RNA Secondary Analysis
requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}


inputs:
  report_id: string
  featurecounts: File
  raw_counts: File
  zscores: File
  Fusions: File
  merged_rna_rdata: File
  GeneConvTable: File
  PathwaysDNA-RNA: File
  PathwaysMixEnrich: File
  ProgenyGeneConversionTable: File
  lincsAnnotRData: File
  ParsedCNVs: File
  ExpressionFeaturesRem: File
  CNVFeatures: File
  CytobandsHG38Custom: File
  annotated_variants: File
  FacetsCNVAnnotateFile: File
  scarTableLatest: File
  bostonDist: File
  mmrfReferenceMatrix: File
  prognosticMarkersTable: File



steps:

  Vicc-Fusions:
    run: ../Tools/Vicctor.cwl
    in:
      SampleID: report_id
      Input: Fusions
      Type:
        default: "fusions"
      Url:
        default: "elastic:changeme@ec2-3-90-34-223.compute-1.amazonaws.com"
    out:
      [vicc_out, civic_out]

  Vicc-Expression:
    run: ../Tools/Vicctor.cwl
    in:
      SampleID: report_id
      Input: zscores
      Type:
        default: "expression"
      Url:
        default: "elastic:changeme@ec2-3-90-34-223.compute-1.amazonaws.com"
    out:
      [vicc_out, civic_out]


  selineScore:
    run: ../Tools/selineScore.cwl
    requirements:
      ResourceRequirement:
        ramMin: 6000      # [MB]
        coresMin: 1      # [cores]
        tmpdirMin: 6000  # [MB]
    in:
      rDataFile: merged_rna_rdata
      bostonDist: bostonDist
    out:
      [selineScoreResults]
  
  RNACohort-ComputeRisk:
    run: ../Tools/RNACohort/RNACohort-ComputeRisk.cwl
    in:
      RData: merged_rna_rdata
      GeneConvTable: GeneConvTable
    out:
      [latest_gep_scores, latest_gep_scores_tsv]

  # RNACohort-CreateLincs:
  #   run: ../Tools/RNACohort/RNACohort-CreateLincs.cwl
  #   in:
  #     SampleName: report_id
  #     ZScoreTable: zscores
  #     GeneConvTable: GeneConvTable
  #   out:
  #     [genes_down, genes_up]

  psnInputGenerator:
    run: ../Tools/psnInputGenerator.cwl
    requirements:
      ResourceRequirement:
        ramMin: 6000      # [MB]
        coresMin: 1      # [cores]
        tmpdirMin: 6000  # [MB]
    in:
      rnaRDataFile: merged_rna_rdata
      sampleName: report_id
      mmrfReferenceTable: mmrfReferenceMatrix
      parsedCNVs: ParsedCNVs
      cnvFeatures: CNVFeatures
      expressionFeatures: ExpressionFeaturesRem
      cytoBand: CytobandsHG38Custom
    out:
      - fsqn_normalized_counts
      - per_band_cnvs
      - psn_cnv_inputs
      - psn_exprs_inputs

  PredictTranslocation:
    run: ../Tools/predict_translocations.cwl
    in:
      SampleExpression: psnInputGenerator/fsqn_normalized_counts
    out:
      [transloc_out]

  MMPSN:
    in:
      ExpressionFile: psnInputGenerator/psn_exprs_inputs
      CNVFile: psnInputGenerator/psn_cnv_inputs
    run: ../Tools/MMPSN.cwl
    out:
      [Predicted_Classes]

  # MixEnrich:
  #   run: ../Tools/MixEnrich.cwl
  #   requirements:
  #     ResourceRequirement:
  #       ramMin: 4080
  #       coresMin: 2
  #   in:
  #     SampleName: report_id
  #     RData: merged_rna_rdata
  #     Pathways: PathwaysMixEnrich
  #     GeneConvTable: GeneConvTable
  #   out:
  #     [MixEnrichTable]

  Progeny:
    run: ../Tools/Progeny.cwl
    requirements:
      ResourceRequirement:
        coresMin: 2
        ramMin: 10485
    in:
      RData: merged_rna_rdata
      GeneConvTable: ProgenyGeneConversionTable
    out:
      [progenyPathwaylatest]

  myelomahallmarks:
    in:
      ProgMarkTable: prognosticMarkersTable
      dnaSampleID: report_id
      TranslocTable: PredictTranslocation/transloc_out
      FacetncytTable: psnInputGenerator/per_band_cnvs
      ScarScorePath: scarTableLatest
      AnnotatedVariantsPath: annotated_variants
      FusionsTable: Fusions
    run: ../Tools/trialcheck.cwl
    out:
      [FinalOutput]


outputs:

  fusions_vicc_out:
    type: File
    outputSource: Vicc-Fusions/vicc_out

  fusions_civic_out:
    type: File
    outputSource: Vicc-Fusions/civic_out

  expression_vicc_out:
    type: File
    outputSource: Vicc-Expression/vicc_out

  expression_civic_out:
    type: File
    outputSource: Vicc-Expression/civic_out

  Risk_Scores_RData:
    type: File
    outputSource: RNACohort-ComputeRisk/latest_gep_scores

  Risk_Scores_Tsv:
    type: File
    outputSource: RNACohort-ComputeRisk/latest_gep_scores_tsv

  # Genes_Up:
  #   type: File
  #   outputSource: RNACohort-CreateLincs/genes_up

  # Genes_Down:
  #   type: File
  #   outputSource: RNACohort-CreateLincs/genes_down

#  Drug_Targets:
#    type: File
#    outputSource: RNACohort-Drugs/drug_targets

  # MixEnrichTable:
  #   type: File
  #   outputSource: MixEnrich/MixEnrichTable

  progenyPathwaylatest:
    type: File[]
    outputSource: Progeny/progenyPathwaylatest

  transloc_out:
    type: File
    outputSource: PredictTranslocation/transloc_out

  selineScoreResults:
    type: File
    outputSource: selineScore/selineScoreResults

  per_band_cnvs:
    type: File
    outputSource: psnInputGenerator/per_band_cnvs

  Predicted_Classes:
    type: File
    outputSource: MMPSN/Predicted_Classes

  mmHallMarks:
    type: File
    outputSource: myelomahallmarks/FinalOutput
