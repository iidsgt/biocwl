#!/usr/bin/env cwl-runner
cwlVersion: v1.0

class: Workflow
label: DAPHNI DNA Primary Pre-Processing

requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement : {}

inputs:
  ForwardReads: File
  ReverseReads: File
  BWA-Index: File
  MaxThreads: int
  SampleID: string #normal or tumor
  KnownSiteVCF: File

steps:
  BWA-Alignment:
    run: ../Tools/BWAX/BWAX-Mem.cwl
    requirements:
      ResourceRequirement:
        ramMin: 50000
        coresMin: 12
        tmpdirMin: 150000
    in:
      InputFile: [ForwardReads, ReverseReads]
      Index: BWA-Index
      isMarkShortSplit:
        default: true
      RgLine:
        source: SampleID
        valueFrom: ${return "@RG\\tID:" + self + "\\tSM:" + self + "\\tLB:" + self + "\\tPL:ILLUMINA\\tPU:0"}
      VerboseLevel:
        default: 1
      Threads: MaxThreads
      Format:
        default: "bam"
    out:
      [reads_out]

  FixMateInformation:
    run: ../Tools/GATK/GATK-FixMateInformation.cwl
    requirements:
      ResourceRequirement:
        ramMin: 30000
    in:
      InputFile: BWA-Alignment/reads_out
      MaxRecordsRam:
        default: 2000000
      SortOrder:
        default: coordinate
      ValidationStringency:
        default: STRICT
    out:
      [alignment]

  # Choose default parameters that match https://www.biostars.org/p/172474/.
  # In an attempt to prevent heap OOM.
  MarkDuplicates:
    run: ../Tools/GATK/GATK-MarkDuplicates.cwl
    requirements:
      ResourceRequirement:
        ramMin: 90000
        coresMin: 16
        tmpdirMin: 150000
    in:
      InputFile: FixMateInformation/alignment
      MaxRecordsRam:
        default: 350000
      JavaOptions:
        default: "-Xms30G -Xmx32G"
      isCreateIndex:
        default: true
    out:
      [alignment, metrics]

  BaseRecalibrator:
    run: ../Tools/GATK/GATK-BaseRecalibrator.cwl
    in:
      InputFile: MarkDuplicates/alignment
      Reference: BWA-Index
      KnownSites: KnownSiteVCF
      isCreateBamIndex:
        default: false
    out:
      [table]

  ApplyBQSR:
    run: ../Tools/GATK/GATK-ApplyBQSR.cwl
    requirements:
      ResourceRequirement:
        ramMin: 4080
        coresMin: 2
        tmpdirMin: 200000
    in:
      InputFile: MarkDuplicates/alignment
      BaseRecalFile: BaseRecalibrator/table
      isCreateBamIndex:
        default: true
      SampleId: SampleID
    out:
      [alignment, index]

  GenerateCoverage:
    run: ../Tools/Deeptools/Deeptools-Bamcoverage.cwl
    requirements:
      ResourceRequirement:
        ramMin: 10000
        coresMin: 2
    in:
      InputFile: ApplyBQSR/alignment
      OutFileFormat:
        default: bigwig
    out:
      [coverage]

  CollectMultipleMetrics:
    run: ../Tools/GATK/GATK-CollectMultipleMetrics.cwl
    requirements:
      ResourceRequirement:
        ramMin: 2040
        coresMin: 2
    in:
      INPUT: ApplyBQSR/alignment
      REFERENCE_SEQUENCE: BWA-Index
    out:
      [multiMetrics]

outputs:
  Final_Bam:
    type: File
    outputSource: ApplyBQSR/alignment

  Final_Bam_Index:
    type: File
    outputSource: ApplyBQSR/index

  MarkDuplicates_Metrics:
    type: File
    outputSource: MarkDuplicates/metrics

  Coverage:
    type: File?
    outputSource: GenerateCoverage/coverage
  
  # From GATK CollectMultipleMetrics
  multiMetrics:
    type: File[]
    outputSource: CollectMultipleMetrics/multiMetrics