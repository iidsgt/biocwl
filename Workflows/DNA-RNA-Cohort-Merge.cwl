#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: Workflow
label: DNA-RNA Cohort Merge

requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  ScatterFeatureRequirement: {}
  SubworkflowFeatureRequirement: {}

inputs:
  ReportIds: string[]
  Batches: string[]
  FeaturecountsFiles: File[]
  GeneConvTable: File
  msiFiles: File[]
  tmbFiles: File[]
  scarFiles: File[]

steps:
  RNACohortMerge:
    run: ../Workflows/RNACohort-Merge.cwl
    in:
      ReportIds: ReportIds
      Batches: Batches
      FeaturecountsFiles: FeaturecountsFiles
      GeneConvTable: GeneConvTable
    out:
      [raw_counts, zscores, normCorrCounts, merged_rna_rdata]

  DNACohortMerge:
    run: ../Workflows/DNACohort-Merge.cwl
    in:
      sampleIDList: ReportIds
      msiFiles: msiFiles
      tmbFiles: tmbFiles
      scarFiles: scarFiles
    out:
      [scarTableLatest, msiTableLatest, tmbTableLatest]

outputs:
  raw_counts:
    type: File
    outputSource: RNACohortMerge/raw_counts

  zscores:
    type: File
    outputSource: RNACohortMerge/zscores

  normCorrCounts:
    type: File
    outputSource: RNACohortMerge/normCorrCounts

  merged_rna_rdata:
    type: File
    outputSource: RNACohortMerge/merged_rna_rdata

  scarTableLatest:
    type: File
    outputSource: DNACohortMerge/scarTableLatest

  msiTableLatest:
    type: File
    outputSource: DNACohortMerge/msiTableLatest

  tmbTableLatest:
    type: File
    outputSource: DNACohortMerge/tmbTableLatest
