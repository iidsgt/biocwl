#!/usr/bin/env cwl-runner
cwlVersion: v1.0

class: Workflow
label: DAPHNI RNA Post-Processing
requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}

inputs:
  InputBam: File
  Threads: int
  Annotation: File


steps:
  FeatureCounts:
    requirements:
      ResourceRequirement:
        ramMin: 50000
        coresMin: 12
        tmpdirMin: 150000
    run: ../Tools/Subread/Subread-FeatureCounts.cwl
    in:
      InputFile: InputBam
      AnnotationFile: Annotation
      isPairedEnd:
        default: true
      NThreads: Threads

    out:
      [featurecounts]


outputs:
  featurecounts:
    type: File
    outputSource: FeatureCounts/featurecounts