#!/usr/bin/env cwl-runner
cwlVersion: v1.0

class: Workflow
label: DAPHNI RNA Pre-Processing

requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}

inputs:
  ForwardReads: File[]
  ReverseReads: File[]
  Threads: int
  STARIndex: Directory
  STARGtf: File
  AssemblyFasta: File
  BlackList: File
  ProteinDomains: File
  CytoBands: File



steps:
  CatForwardReads:
    run: ../Tools/CatFastqs.cwl
    requirements:
      ResourceRequirement:
        tmpdirMin: 32000
        coresMin: 4
    in:
      Files: ForwardReads
    out:
      [File]
  
  CatReverseReads:
    run: ../Tools/CatFastqs.cwl
    requirements:
      ResourceRequirement:
        tmpdirMin: 32000
        coresMin: 4
    in:
      Files: ReverseReads
    out:
      [File]

  fastp_rna:
    run: ../Tools/Fastp-preprocessFASTQ.cwl
    requirements:
      ResourceRequirement:
        coresMin: 4
        ramMin: 2040
    in:
      fastq1:
        source: [CatForwardReads/File]
      fastq2:
        source: [CatReverseReads/File]
    out:
      [out_fastq1, out_fastq2, html_report, json_report]

  STAR-Alignment:
    run: ../Tools/STAR/STAR-Align.cwl
    requirements:
      ResourceRequirement:
        ramMin: 80000
        coresMin: 16
        tmpdirMin: 80000
    in:
      Index: STARIndex
      InputFiles:
        source: [fastp_rna/out_fastq1, fastp_rna/out_fastq2]
      RunThreadN: Threads
      GenomeLoad:
        default: NoSharedMemory
      OutFilterType:
        default: BySJout
      OutFilterIntronMotifs:
        default: RemoveNoncanonical
      OutSAMtype:
        default: [BAM, SortedByCoordinate]
      ReadFilesCommand:
        default: "zcat"
      AlignIntronMin:
        default: 20
      AlignIntronMax:
        default: 1000000
      AlignMatesGapMax:
        default: 1000000
      AlignSJoverhangMin:
        default: 8
      AlignSJDBoverhangMin:
        default: 1
      SeedSearchStartLmax:
        default: 30
      ChimOutType:
        default: [WithinBAM, SoftClip]
      ChimSegmentMin:
        default: 15
      ChimJunctionOverhangMin:
        default: 15
      OutFilterMultimapNmax:
        default: 10
      OutFilterMismatchNmax:
        default: 10
      OutFilterMismatchNoverLmax:
        default: 0.1
      OutReadsUnmapped:
        default: Fastx
      OutSAMstrandField:
        default: intronMotif
      OutSAMunmapped:
        default: Within
      OutSAMmapqUnique:
        default: 255
      OutSamMode:
        default: Full
      LimitOutSAMoneReadBytes:
        default: 90000000
      LimitBAMsortRAM:
        default: 40000000000

    out: [alignment, unmapped_reads_1, unmapped_reads_2, starLog]

  Sam2Fastp:
    run: ../Tools/Samtools/Samtools-Fastp.cwl
    requirements:
      ResourceRequirement:
        coresMin: 8
        ramMin: 40000
    in:
      InputFile: STAR-Alignment/alignment
    out: [html, json]

  Arriba:
    run: ../Tools/Arriba/Arriba.cwl
    requirements:
      ResourceRequirement:
        coresMin: 8
        ramMin: 40000
    in:
      BlackList: BlackList
      ChimericOutBam: STAR-Alignment/alignment
      AnnotationGtf: STARGtf
      AssemblyFasta: AssemblyFasta
    out: [fusions, fusion_discarded]

  ArribaViz:
    run: ../Tools/Arriba/ArribaViz.cwl
    requirements:
      ResourceRequirement:
        coresMin: 8
        ramMin: 40000
    in:
      Fusions: Arriba/fusions
      ChimericOutBam: MarkDuplicates/alignment
      AnnotationGtf: STARGtf
      CytoBands: CytoBands
      ProteinDomains: ProteinDomains
    out: [fusionsPDF]

  MarkDuplicates:
    run: ../Tools/GATK/GATK-MarkDuplicates.cwl
    requirements:
      ResourceRequirement:
        ramMin: 90000
        coresMin: 16
        tmpdirMin: 150000
    in:
      InputFile: STAR-Alignment/alignment
      isRemoveDuplicates:
        default: false
      ValidationStringency:
        default: SILENT
      MaxRecordsRam:
        default: 350000
      JavaOptions:
        default: "-Xms30G -Xmx32G"
      isCreateIndex:
        default: true

    out: [alignment, metrics, index]

outputs:
  Final_Bam:
    type: File
    outputSource: MarkDuplicates/alignment

  Final_Index:
    type: File
    outputSource: MarkDuplicates/index

  MarkDuplicates_Metrics:
    type: File
    outputSource: MarkDuplicates/metrics

  Html:
    type: File
    outputSource: Sam2Fastp/html

  Json:
    type: File
    outputSource: Sam2Fastp/json

  Fusions:
    type: File
    outputSource: Arriba/fusions

  FusionsPDF:
    type: File
    outputSource: ArribaViz/fusionsPDF

  FusionsDiscarded:
    type: File
    outputSource: Arriba/fusion_discarded

  Unmapped_Reads1:
    type: ["null", File]
    outputSource: STAR-Alignment/unmapped_reads_1

  Unmapped_Reads2:
    type: ["null", File]
    outputSource: STAR-Alignment/unmapped_reads_2

  rna_fastp_json:
    type: File
    outputSource: fastp_rna/json_report

  starLog:
    type: File
    outputSource: STAR-Alignment/starLog