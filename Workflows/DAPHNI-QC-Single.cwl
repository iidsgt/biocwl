#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: Workflow
label: DAPHNI QC Pipeline

requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  ScatterFeatureRequirement: {}
  SubworkflowFeatureRequirement: {}

inputs:
  DNAForwardReads: File[]
  DNAReverseReads: File[]
  MaxThreads: int
  isTestSampleInput: boolean?

steps:
  cat_forward_reads:
    run: ../Tools/CatFastqs.cwl
    in:
      Files: DNAForwardReads
    out:
      [File]

  cat_reverse_reads:
    run: ../Tools/CatFastqs.cwl
    in:
      Files: DNAReverseReads
    out:
      [File]

  fastp:
    run: ../Tools/Fastp-preprocessFASTQ.cwl
    in:
      fastq1:
        source: [cat_forward_reads/File]
      fastq2:
        source: [cat_reverse_reads/File]
    out:
      [out_fastq1, out_fastq2, html_report, json_report]

outputs:
  dna_forward_reads:
    type: File
    outputSource: cat_forward_reads/File

  dna_reverse_reads:
    type: File
    outputSource: cat_reverse_reads/File

  # trimmed & corrected fastqs
  dna_forward_reads_trimmed:
    type: File
    outputSource: fastp/out_fastq1

  dna_reverse_reads_trimmed:
    type: File
    outputSource: fastp/out_fastq2

  
  # fastp metrics
  fastp_json:
    type: File
    outputSource: fastp/json_report
