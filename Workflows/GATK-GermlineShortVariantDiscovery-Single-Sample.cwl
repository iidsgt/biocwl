#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: Workflow
label: GATK Germline short variant discovery (SNPs + Indels) Best Practices Workflows

requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement : {}

inputs:
  InputBam: File
  Reference: File
  Mills: File
  HapMap: File
  Intervals: File
  FuncoDatasourceGermline: Directory
  OutputFilename: string

steps:
  HaplotypeCaller:
    run: ../Tools/GATK/GATK-HaplotypeCaller.cwl
    requirements:
      ResourceRequirement:
        ramMin: 50000
        coresMin: 4
    in:
      InputFile: InputBam
      Reference: Reference
      Intervals: Intervals
    out:
      [filteredVCF]

  CNNScoreVariants:
    run: ../Tools/GATK/GATK-CNNScoreVariants.cwl
    requirements:
      ResourceRequirement:
        ramMin: 8160
        coresMin: 4
    in:
      InputFile: InputBam
      Variant: HaplotypeCaller/filteredVCF
      Reference: Reference
      TensorType:
        default: "read_tensor"

    out:
      [filteredVCF]

  FilterVariantTranches:
    run: ../Tools/GATK/GATK-FilterVariantTranches.cwl
    hints:
      ResourceRequirement:
        coresMin: 6 # reader-threads + 1
        ramMin: 50000
        tmpdirMin: 150000
    in:
      Variant: CNNScoreVariants/filteredVCF
      ResourceMills: Mills
      ResourceHapMap: HapMap
      InfoKey:
        default: "CNN_1D"

    out:
      [filteredVCF]

  AnnotateGermlineVCF:
    run: ../Tools/GATK/GATK-Funcotator.cwl
    requirements:
      ResourceRequirement:
        coresMin: 2
        ramMin: 5000
    in:
      InputVCF: FilterVariantTranches/filteredVCF
      DataSources: FuncoDatasourceGermline
      Output: OutputFilename
      OutputFormat:
        default: MAF
      Reference: Reference
      RefVersion:
        default: hg38
    out:
      [annotated_variants]

outputs:

  annotateGermlineVCF:
    type: File[]
    outputSource: AnnotateGermlineVCF/annotated_variants



