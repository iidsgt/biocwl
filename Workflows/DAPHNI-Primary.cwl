#!/usr/bin/env cwl-runner
cwlVersion: v1.1

class: Workflow
label: DAPHNI RNA+DNA Primary Analyses
requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  ScatterFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  SubworkflowFeatureRequirement: {}
  ResourceRequirement:
    ramMin: 1000      # [MB]
    coresMin: 1        # [cores]
    tmpdirMin: 45000   # [MB]


inputs:
  AnnotationDB: Directory
  AssemblyFasta: File
  BWA-Index: File
  BlackList: File
  CytoBands: File
  DbSnp: File
  GeneConversionTable: File
  noChrBed: File
  TranscriptIDs: File
  GenomicRegion: string[]
  IntervalFile: File
  KnownSiteVCF: File
  LenFile: File
  MaxThreads: int
  Microsatellites: File
  NormalDNAForwardReads: File[]
  NormalDNAReverseReads: File[]
  ProteinDomains: File
  STARGtf: File
  STARIndex: Directory
  TumorDNAForwardReads: File[]
  TumorDNAReverseReads: File[]
  TumorRNAForwardReads: File[]
  TumorRNAReverseReads: File[]
  isTestSampleInput: boolean?
  Mills: Any
  axiomPoly: Any
  HapMap: Any
  Omni: Any
  1000G: Any
  DbSnp146: Any
  FuncoDatasourceGermline: Directory
  indices_folder: Directory

outputs:
  annotated_variants:
    type: File[]
    outputSource: dna_post/annotated_variants
  counted_unfiltered_vcf:
    type: File
    outputSource: dna_post/counted_unfiltered_vcf
  consensus_variant_calls:
    type: File
    outputSource: dna_post/consensus_vcf
  counted_vcf:
    type: File
    outputSource: dna_post/counted_vcf
  facets_cncf:
    type: File
    outputSource: dna_post/facets_cncf
  facets_summary:
    type: File
    outputSource: dna_post/facets_summary
  facets_diagnostics_plot:
    type: File
    outputSource: dna_post/facets_diagnostics_plot
  facets_cellularity: 
    type: File
    outputSource: dna_post/facets_cellularity
  facets_flags:
    type: File
    outputSource: dna_post/facets_flags
  facets_genome_segments:
    type: File
    outputSource: dna_post/facets_genome_segments
  facets_parsed_cnvs:
    type: File
    outputSource: dna_post/facets_parsed_cnvs
  facets_annotated:
    type: File
    outputSource: dna_post/facets_annotated
  normal_dna_Final_Bam:
    type: File
    outputSource: normal_dna_pre/Final_Bam
  normal_dna_Final_Bam_Index:
    type: File
    outputSource: normal_dna_pre/Final_Bam_Index
  normal_dna_MarkDuplicates_Metrics:
    type: File
    outputSource: normal_dna_pre/MarkDuplicates_Metrics
  normal_dna_coverage:
    type: File?
    outputSource: normal_dna_pre/Coverage
  tmb_details:
    type: File
    outputSource: dna_post/tmb_details
  scar_results:
    type: File
    outputSource: dna_post/scar_results
  msi_output:
    type: File
    outputSource: dna_post/msi_output
  msi_output_dis_tab:
    type: File
    outputSource: dna_post/msi_output_dis_tab
  msi_output_germline:
    type: File
    outputSource: dna_post/msi_output_germline
  msi_output_somatic:
    type: File
    outputSource: dna_post/msi_output_somatic
  snp_pileup:
    type: File
    outputSource: dna_post/snp_pileup
  annotateGermlineVCFTumor:
    type: File[]
    outputSource: dna_post/annotateGermlineVCFTumor
  annotateGermlineVCFNormal:
    type: File[]
    outputSource: dna_post/annotateGermlineVCFNormal
  tumor_dna_Final_Bam:
    type: File
    outputSource: tumor_dna_pre/Final_Bam
  tumor_dna_Final_Bam_Index:
    type: File
    outputSource: tumor_dna_pre/Final_Bam_Index
  tumor_dna_MarkDuplicates_Metrics:
    type: File
    outputSource: tumor_dna_pre/MarkDuplicates_Metrics
  tumor_dna_coverage:
    type: File?
    outputSource: tumor_dna_pre/Coverage
  tumor_rna_Final_Bam:
    type: File
    outputSource: tumor_rna_pre/Final_Bam
  tumor_rna_Final_Index:
    type: File
    outputSource: tumor_rna_pre/Final_Index
  tumor_rna_Fusions:
    type: File
    outputSource: tumor_rna_pre/Fusions
  tumor_rna_FusionsPDF:
    type: File
    outputSource: tumor_rna_pre/FusionsPDF
  tumor_rna_MarkDuplicates_Metrics:
    type: File
    outputSource: tumor_rna_pre/MarkDuplicates_Metrics
  tumor_rna_Unmapped_Reads1:
    type: File
    outputSource: tumor_rna_pre/Unmapped_Reads1
  tumor_rna_Unmapped_Reads2:
    type: File
    outputSource: tumor_rna_pre/Unmapped_Reads2
  tumor_rna_featurecounts:
    type: File
    outputSource: tumor_rna_post/featurecounts
#  rsem_gene_results_file:
#    type: File
#    outputSource: RSEM/gene_results_file
  multiqc_html:
    type: File
    outputSource: multiQCReport/multiqc_html
  multiqc_zip:
    type: File
    outputSource: multiQCReport/multiqc_zip

steps:
  QC:
    run: ../Workflows/DAPHNI-QC.cwl
    in:
      NormalDNAForwardReads: NormalDNAForwardReads
      NormalDNAReverseReads: NormalDNAReverseReads
      TumorDNAForwardReads: TumorDNAForwardReads
      TumorDNAReverseReads: TumorDNAReverseReads
      MaxThreads: MaxThreads
      isTestSampleInput: isTestSampleInput
    out:
    - tumor_dna_forward_reads_trimmed
    - tumor_dna_reverse_reads_trimmed
    - tumor_fastp_json
    - normal_dna_forward_reads_trimmed
    - normal_dna_reverse_reads_trimmed
    - normal_fastp_json

  dna_post:
    in:
      AnnotationDB: AnnotationDB
      BedFile: IntervalFile
      DbSnp: DbSnp
      GenomicRegion: GenomicRegion
      IntervalFile: IntervalFile
      KnownSiteVCF: KnownSiteVCF
      Microsatellites: Microsatellites
      NormalBam: normal_dna_pre/Final_Bam
      Reference: BWA-Index
      SampleId:
        valueFrom: tumor
      TumorBam: tumor_dna_pre/Final_Bam
      DbSnp146: DbSnp146
      Mills: Mills
      axiomPoly: axiomPoly
      HapMap: HapMap
      Omni: Omni
      1000G: 1000G
      FuncoDatasourceGermline: FuncoDatasourceGermline
      GeneConversionTable: GeneConversionTable
      noChrBed: noChrBed
      TranscriptIDs: TranscriptIDs
    run: ../Workflows/DAPHNI-DNA-Post-Single.cwl
    out:
    - snp_pileup
    - consensus_vcf
    - counted_unfiltered_vcf
    - read_counts
    - counted_vcf
    - annotated_variants
    - facets_annotated
    - facets_parsed_cnvs
    - facets_genome_segments
    - facets_diagnostics_plot
    - facets_cellularity
    - facets_cncf
    - facets_summary
    - facets_flags
    - msi_output
    - msi_output_dis_tab
    - msi_output_germline
    - msi_output_somatic
    - annotateGermlineVCFTumor
    - annotateGermlineVCFNormal
    - scar_results
    - tmb_details
  normal_dna_pre:
    in:
      BWA-Index: BWA-Index
      ForwardReads: QC/normal_dna_forward_reads_trimmed
      KnownSiteVCF: KnownSiteVCF
      MaxThreads: MaxThreads
      ReverseReads: QC/normal_dna_reverse_reads_trimmed
      SampleID:
        valueFrom: normal
    run: ../Workflows/DAPHNI-DNA-Pre.cwl
    out:
    - Final_Bam
    - Final_Bam_Index
    - MarkDuplicates_Metrics
    - Coverage
    - multiMetrics

  tumor_dna_pre:
    in:
      BWA-Index: BWA-Index
      ForwardReads: QC/tumor_dna_forward_reads_trimmed
      KnownSiteVCF: KnownSiteVCF
      MaxThreads: MaxThreads
      ReverseReads: QC/tumor_dna_reverse_reads_trimmed
      SampleID:
        valueFrom: tumor
    run: ../Workflows/DAPHNI-DNA-Pre.cwl
    out:
    - Final_Bam
    - Final_Bam_Index
    - MarkDuplicates_Metrics
    - Coverage
    - multiMetrics

  tumor_rna_pre:
    in:
      AssemblyFasta: AssemblyFasta
      BlackList: BlackList
      CytoBands: CytoBands
      ForwardReads: TumorRNAForwardReads
      LenFile: LenFile
      ProteinDomains: ProteinDomains
      ReverseReads: TumorRNAReverseReads
      STARGtf: STARGtf
      STARIndex: STARIndex
      Threads: MaxThreads
    run: ../Workflows/DAPHNI-RNA-Pre.cwl
    out:
    - Final_Bam
    - Final_Index
    - MarkDuplicates_Metrics
    - Unmapped_Reads1
    - Unmapped_Reads2
    - Fusions
    - FusionsDiscarded
    - FusionsPDF
    - rna_fastp_json
    - starLog
#  RSEM:
#    run: ../Tools/rsem-calculate-expression.cwl
#    in:
#      upstream_read_file:
#        valueFrom: $(self[0])
#        source: TumorRNAForwardReads
##      downstream_read_file: TumorRNAReverseReads
#      downstream_read_file:
#        valueFrom: $(self[0])
#        source: TumorRNAReverseReads
#      indices_folder: indices_folder
##      output_filename:
##        valueFrom: tumor
#    out:
#      [ gene_results_file ]
#


  tumor_rna_post:
    in:
      Annotation: STARGtf
      InputBam: tumor_rna_pre/Final_Bam
      Threads: MaxThreads
    run: ../Workflows/DAPHNI-RNA-Post.cwl
    out:
    - featurecounts

  multiQCReport:
    in:
      qc_files_array_of_array: [tumor_dna_pre/multiMetrics, normal_dna_pre/multiMetrics]
      qc_files_array: [QC/normal_fastp_json, QC/tumor_fastp_json, tumor_rna_pre/rna_fastp_json, tumor_rna_pre/starLog]
    run: ../Tools/multiQC.cwl
    out:
    - multiqc_zip
    - multiqc_html