#!/usr/bin/env cwl-runner
cwlVersion: v1.0

class: Workflow
label: DAPHNI DNA Secondary Post-Processing

requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  SubworkflowFeatureRequirement: {}

inputs:
  sampleIDList: string[]
  msiFiles: File[]
  tmbFiles: File[]
  scarFiles: File[]

steps:
  mergeScarResults:
    run: ../Tools/bindDNAMetrics.cwl
    in:
      sampleNameList: sampleIDList
      filesList: scarFiles
      metricType: 
        valueFrom: "scarScore"
    out:
      [mergedTableLatest]

  mergeTMBResults:
    run: ../Tools/bindDNAMetrics.cwl
    in:
      sampleNameList: sampleIDList
      filesList: tmbFiles
      metricType: 
        valueFrom: "tumorMutationBurden"
    out:
      [mergedTableLatest]

  mergeMSIResults:
    run: ../Tools/bindDNAMetrics.cwl
    in:
      sampleNameList: sampleIDList
      filesList: msiFiles
      metricType:
        valueFrom: "msiScore"
    out:
      [mergedTableLatest]

outputs:
  scarTableLatest:
    type: File
    outputSource: mergeScarResults/mergedTableLatest
  
  msiTableLatest:
    type: File
    outputSource: mergeMSIResults/mergedTableLatest
  
  tmbTableLatest:
    type: File
    outputSource: mergeTMBResults/mergedTableLatest