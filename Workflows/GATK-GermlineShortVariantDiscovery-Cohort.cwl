#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: Workflow

label: GATK Germline short variant discovery (SNPs + Indels) Best Practices Workflows
$namespaces:
  arv: "http://arvados.org/cwl#"
  cwltool: "http://commonwl.org/cwltool#"
hints:
  arv:RuntimeConstraints:
    keep_cache: 1024
    outputDirType: keep_output_dir
  cwltool:LoadListingRequirement:
    loadListing: no_listing
  arv:IntermediateOutput:
      outputTTL: 2592000

requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement : {}

inputs:
  InputBam: File
  Reference: File
  BedFile: File
  Mills: Any
  axiomPoly: Any
  DbSnp146: Any
  HapMap: Any
  Omni: Any
  1000G: Any
  FuncoDatasourceGermline: Directory
  SampleID: string

steps:
  HaplotypeCaller:
    run: ../Tools/GATK/GATK-HaplotypeCaller.cwl
    in:
      InputFile: InputBam
      Reference: Reference
      emitRefConfidence:
        default: "GVCF"
    out:
      [filteredVCF]

  VCFReheader:
    run: ../Tools/BCFTools/BCFTools-Re-header.cwl
    in:
      InputFile: HaplotypeCaller/filteredVCF
      SampleName: BedFile

    out:
      [reheaded_vcf]

  GenomicsDBImport:
    run: ../Tools/GATK/GATK-GenomicsDBImport.cwl
    hints:
      ResourceRequirement:
        coresMin: 6 # reader-threads + 1
        ramMin: 18000
        tmpdirMin: 1000
      arv:RuntimeConstraints:
        keep_cache: 1280 # 64 * (4 * reader-threads)
    in:
      MergeInputIntervals:
        default: true
      VariantFiles:
        source: HaplotypeCaller/filteredVCF
        valueFrom: ${return [self]}
      GenomicsdbWorkspacePath:
        default: "genomicsdb"
      Intervals: BedFile
    out:
      [genomics_db_workspace]

  GenotypeGVCFs:
    run: ../Tools/GATK/GATK-GenotypeGVCFs.cwl
    hints:
      ResourceRequirement:
        ramMin: 20000
        coresMin: 1
        tmpdirMin: 1000
    in:
      Reference: Reference
      Variant: GenomicsDBImport/genomics_db_workspace
    out:
      [genotyped_vcf]

  VariantFiltration:
    run: ../Tools/GATK/GATK-VariantFiltration.cwl
    in:
      Variant: GenotypeGVCFs/genotyped_vcf
      filterExpression:
        default: "ExcessHet > 54.69"
      filterName:
        default: "ExcessHet"
    out:
      [filteredVCF]

  MakeSitesOnlyVcf:
    run: ../Tools/GATK/GATK-MakeSitesOnlyVcf.cwl
    in:
      InputVCF: VariantFiltration/filteredVCF
    out:
      [SitesOnlyVcf]

  VariantRecalibrator:
    run: ../Tools/GATK/GATK-VariantRecalibrator.cwl
    in:
      Variant: MakeSitesOnlyVcf/SitesOnlyVcf
      trustAllPolymorphic: { default: true }
      tranche:
        default: ["100.0", "99.95", "99.9", "99.5", "99.0", "97.0", "96.0", "95.0", "94.0", "93.5", "93.0", "92.0", "91.0", "90.0"]
      useAnnotation:
        default: ["FS", "ReadPosRankSum", "MQRankSum", "QD", "SOR"]
      Mode:
        default: INDEL
      MaxGaussians:
        default: 4
      Resources: [Mills, axiomPoly, DbSnp146]
    out:
      [recalFile, output_tranches]

  VariantRecalibrator2:
    run: ../Tools/GATK/GATK-VariantRecalibrator.cwl
    in:
      Variant: MakeSitesOnlyVcf/SitesOnlyVcf
      trustAllPolymorphic:
        default: true
      tranche:
        default: ["100.0", "99.95", "99.9", "99.8", "99.6", "99.5", "99.4", "99.3", "99.0", "98.0", "97.0", "90.0"]
      useAnnotation:
        default: ["QD", "MQRankSum", "ReadPosRankSum", "FS", "MQ", "SOR"]
      Mode:
        default: SNP
      MaxGaussians:
        default: 6
      Resources: [HapMap, Omni, 1000G, DbSnp146]
    out:
      [recalFile, output_tranches]

  ApplyVQSR:
    run: ../Tools/GATK/GATK-ApplyVQSR.cwl
    in:
      recalFile: VariantRecalibrator/recalFile
      TranchesFile: VariantRecalibrator/output_tranches
      Variant: VariantFiltration/filteredVCF
      TruthSensitivity:
        default: 99.7
      Mode:
        default: INDEL
    out:
      [ApplyVQSRVCF]

  ApplyVQSR2:
    run: ../Tools/GATK/GATK-ApplyVQSR.cwl
    in:
      recalFile: VariantRecalibrator2/recalFile
      TranchesFile: VariantRecalibrator2/output_tranches
      Variant: ApplyVQSR/ApplyVQSRVCF
      TruthSensitivity:
        default: 99.7
      Mode:
        default: SNP
    out:
      [ApplyVQSRVCF]

  AnnotateGermlineVCF:
    run: ../Tools/GATK/GATK-Funcotator.cwl
    in:
      InputVCF: ApplyVQSR2/ApplyVQSRVCF
      DataSources: FuncoDatasourceGermline
      Output:
        default: "annotated_variants.germline.vcf"
      OutputFormat:
        default: MAF
      Reference: Reference
      RefVersion:
        default: hg38
    out:
      [annotated_variants]


outputs:
  filteredVCF:
    type: File
    outputSource: VariantFiltration/filteredVCF

  genomics_db_workspace:
    type: Directory
    outputSource: GenomicsDBImport/genomics_db_workspace

  genotyped_vcf:
    type: File
    outputSource: GenotypeGVCFs/genotyped_vcf

  annotated_germline_variants:
    type: File[]
    outputSource: AnnotateGermlineVCF/annotated_variants

  indel_recalibrated_vfc:
    type: File
    outputSource: ApplyVQSR/ApplyVQSRVCF

  snp_recalibrated_vfc:
    type: File
    outputSource: ApplyVQSR2/ApplyVQSRVCF

  indel_recal_file:
    type: File
    outputSource: VariantRecalibrator/recalFile

  snp_recal_file:
    type: File
    outputSource: VariantRecalibrator2/recalFile

  SNP_tranches:
    type: File
    outputSource: VariantRecalibrator2/output_tranches

  INDEL_tranches:
    type: File
    outputSource: VariantRecalibrator/output_tranches

  SitesOnlyVcf:
    type: File
    outputSource: MakeSitesOnlyVcf/SitesOnlyVcf
