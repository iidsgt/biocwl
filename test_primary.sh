#!/bin/bash
PRIMARY_yaml="/data1/users/restrp01/mm-yamls/primary/SEMA-MM-046RNA/MM_0109_BM_02.PRIMARY.yaml"
priority=800
PRIMARY_yaml=`realpath $PRIMARY_yaml`
new_yaml=${PRIMARY_yaml##*/} 
report_id=`echo ${new_yaml} | sed 's/.PRIMARY.yaml//g'`
echo "Submitting PRIMARY for report ID" $report_id "with yaml" $PRIMARY_yaml "!"

docker run \
-v "/data1/daphni2/biocwl:/biocwl" \
-v "${PRIMARY_yaml}:/yaml" \
-v /var/run/docker.sock:/var/run/docker.sock \
-e ARVADOS_API_TOKEN \
-e ARVADOS_API_HOST \
sinaiiidgst/arvclient \
arvados-cwl-runner \
--submit \
--no-wait \
--intermediate-output-ttl 2592000 \
--project-uuid myarv-j7d0g-4j0q8ta0l27blwm \
--priority $priority \
--name ${report_id}.PRIMARY \
--output-name ${report_id}.PRIMARY \
--output-tags PRIMARY,outputs \
/biocwl/Workflows/DAPHNI-Primary.cwl \
/yaml