#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "quay.io/biocontainers/bbmap:38.76--h516909a_0"
  InlineJavascriptRequirement: {}

inputs:
  ForwardReads:
    type: File
    inputBinding:
      prefix: "in1="
      separate: False
      position: 1
  
  ReverseReads:
    type: File
    inputBinding:
      prefix: "in2="
      separate: false
      position: 2
  
  Compression:
    type:
    - "null"
    - type: enum
      symbols:
      - gz
      - bgzf
    default: null

  Output:
    type: string
    default: "fixed_reads"

  Out1:
    type: string
    default: |
      $(inputs.Output + "_1.fq" + (inputs.Compression ? ("." + inputs.Compression) : ""))
    inputBinding:
      prefix: "out1="
      separate: false
      valueFrom: |
        $(inputs.Output + "_1.fq" + (inputs.Compression ? ("." + inputs.Compression) : ""))

  Out2:
    type: string
    default: |
      $(inputs.Output + "_2.fq" + (inputs.Compression ? ("." + inputs.Compression) : ""))
    inputBinding:
      prefix: "out2="
      separate: false
      valueFrom: |
        $(inputs.Output + "_2.fq" + (inputs.Compression ? ("." + inputs.Compression) : ""))


baseCommand: ["repair.sh"]

outputs:
  Forward_Reads:
    type: File
    format: http://edamontology.org/format_1930  # FASTA
    outputBinding:
      glob: |
        $(inputs.Output + "_1.fq" + (inputs.Compression ? ("." + inputs.Compression) : ""))

  Reverse_Reads:
    type: File
    format: http://edamontology.org/format_1930  # FASTA
    outputBinding:
      glob: |
        $(inputs.Output + "_2.fq" + (inputs.Compression ? ("." + inputs.Compression) : ""))