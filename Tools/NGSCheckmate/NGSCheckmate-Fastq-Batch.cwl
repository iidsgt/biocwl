#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/ngscheckmate:latest"
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - entryname: files.txt
        entry: |-
          ${
            var pairs = "";
            var fr = [];
            var rr = [];
            for (var i=0; i<inputs.InputFiles.length; i++) {
              if (inputs.InputFiles[i].basename.search("R1") == -1) {
                rr.push(inputs.InputFiles[i])
              }
              else if (inputs.InputFiles[i].basename.search("R2") == -1) {
                fr.push(inputs.InputFiles[i])
              }
            }
            if (fr.length === rr.length) {
              for (var i=0; i<rr.length; i++) {
                pairs += fr[i].path + "\t" + rr[i].path + "\t" + rr[i].basename.split("-")[0] + "\n"
              }
            }
            return pairs
          }

inputs:
  InputFiles:
    type: File[]

  PatternFile:
    type: string
    default: "/SNP/SNP.pt"
    inputBinding:
      prefix: "--pt"

  InputList:
    type: string
    default: "files.txt"
    inputBinding:
      prefix: "--list"

  Threads:
    type: int
    default: 4
    inputBinding:
      prefix: "--maxthread"

# OPTIONAL ARGUMENTS

  Output:
    type: string
    default: "output"
    inputBinding:
      prefix: "--outdir"

baseCommand: [ncm_fastq.py]

outputs:
  data_dir:
    type: Directory
    outputBinding:
      glob: $(inputs.Output)