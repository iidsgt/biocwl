#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/ngscheckmate:84b8e281b6186d6084c169e20d4f1fb0fba8ccbd"
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - entryname: files.txt
        entry: |-
          ${
            var lines = "";
            lines += inputs.NormalFastq[0].path + "\t" + inputs.NormalFastq[1].path + "\tNormal\n";
            lines += inputs.TumorFastq[0].path + "\t" + inputs.TumorFastq[1].path + "\tTumor";
            return lines;
          }

inputs:
  NormalFastq:
    type: File[]
  
  TumorFastq:
    type: File[]

  PatternFile:
    type: string?
    default: /SNP/SNP.pt
    inputBinding:
      prefix: "--pt"

  InputList:
    type: string
    default: files.txt
    inputBinding:
      prefix: "--list"

  Threads:
    type: int
    default: 4
    inputBinding:
      prefix: "--maxthread"

# OPTIONAL ARGUMENTS

  Output:
    type: string
    default: "output"
    inputBinding:
      prefix: "--outdir"
      valueFrom: "output"

baseCommand: [ncm_fastq.py]

outputs:
  data_dir:
    type: Directory
    outputBinding:
      glob: "output"