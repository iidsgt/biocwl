#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool
requirements:
  DockerRequirement:
    dockerPull: "quay.io/biocontainers/bam-readcount:0.8--py36pl5.22.0_3"


inputs:
  
  InputFile:
    type: File
    inputBinding:
      position: 100

#OPTIONAL INPUTS

#  GenomicRegion:
#    type:
#      - "null"
#        name: GenomicRegion
#      - type: record
#        fields:
#          Chromosome:
#            type: string
#          RegionStart:
#            type: int
#          RegionEnd:
#            type: int
#          GenomicRegion:
#            type: string
#            inputBinding:
#              prefix: "--reg"
#              valueFrom: $(inputs.GenomicRegion.Chromosome + ":" + inputs.GenomicRegion.RegionStart + "-" + inputs.GenomicRegion.RegionEnd)

  Reference:
    type: File?
    inputBinding:
      prefix: "--reference-fasta"

  MinMapQual:
    type: int?
    inputBinding:
      prefix: "--min-mapping-quality"

  MinBaseQual:
    type: int?
    inputBinding:
      prefix: "--min-base-quality"

  MaxDepth:
    type: int?
    inputBinding:
      prefix: "--max-count"

  SiteList:
    type: File?
    inputBinding:
      prefix: "--site-list"

  PrintMapQ:
    type: boolean?
    inputBinding:
      prefix: "--print-individual-mapq"

  isPerLibrary:
    type: boolean?
    inputBinding:
      prefix: "--per-library"

  MaxWarn:
    type: int?
    inputBinding:
      prefix: "--max-warnings"
  
  isInsertCentric:
    type: boolean?
    inputBinding:
      prefix: "--insertion-centric"
  
stdout: readcount.tsv

baseCommand: [bam-readcount]

outputs:
  read_counts:
    type: stdout
