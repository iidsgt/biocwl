#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/translocation_classifier:52bda5d"

inputs:

  SampleExpression:
    type: File
    inputBinding:
      position: 1

  OutputDir:
    type: string
    default: "predicted_translocations.csv"
    inputBinding:
      valueFrom: "predicted_translocations.csv"
      position: 2

baseCommand: [python, /bin/predict_translocations.py]

outputs:
  transloc_out:
    type: File
    outputBinding:
      glob: "predicted_translocations.csv"

