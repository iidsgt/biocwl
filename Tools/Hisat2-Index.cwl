#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "quay.io/biocontainers/hisat2:2.0.4--py35_1"

#May need to specify dependent parameters and mutual exclusion to manage the -c flag
inputs:

  InputFiles:
    format: 
      - http://edamontology.org/format_1929
      - http://edamontology.org/format_1930/format_3752
    type:
      - type: record
        name: fasta
        fields:
          fasta:
            type: File[]
            inputBinding:
              itemSeparator: ","
              position: 100

      - type: record
        name: csv
        fields:
          csv:
            type: string[]
            #format: http://edamontology.org/format_1930/format_3752
            inputBinding:
              itemSeparator: ","
              position: 101
              prefix: "-c"

  IndexName:
    type: string
    inputBinding:
      position: 101

  isLargeIndex:
    type: boolean
    default: false
    inputBinding:
      position: 1
      prefix: "--large-index"

#Optional Inputs
  
  Snp:
    type: File?
    inputBinding:
      prefix: "--snp"
  
  Haplotype:
    type: File?
    inputBinding:
      prefix: "--haplotype"

  Exon:
    type:
      - "null"
      - type: record
        name: Exon
        fields:
          exon:
            type: File
            inputBinding:
              prefix: "--exon"
          ss:
            type: File
            inputBinding:
              prefix: "--ss"

  Cutoff:
    type: int?
    inputBinding:
      prefix: "--cutoff"

  Threads:
    type: int?
    default: 4
    inputBinding:
      prefix: "-p"

  Offrate:
    type: int?
    inputBinding:
      prefix: "--offrate"

  Auto:
    type:
      - "null"
      - type: record
        name: auto
        fields:
          no_auto:
            type: boolean
            inputBinding:
              prefix: "--noauto"

          bmax:
            type: int
            inputBinding:
              prefix: "--bmax"

          dcv:
            type: int
            inputBinding:
              prefix: 
  

baseCommand: [hisat2-build]

outputs:
  indexes:
    type: File[]
    outputBinding:
      glob: '*.ht2*'
