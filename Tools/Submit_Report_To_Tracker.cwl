#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "python:3.7"
  InitialWorkDirRequirement:
    listing:
      - entryname: myscript.py
        entry: |-
          from os import listdir
          report_json = "$(inputs.tumor_rna_Final_Bam.basename)\n$(inputs.tumor_dna_Final_Bam.basename)\n$(inputs.normal_dna_Final_Bam.basename)\n"
          print(report_json)
          with open('report.json', 'w') as f:
            f.write(report_json)

inputs:
  tumor_rna_Final_Bam:
    type: File
  tumor_dna_Final_Bam:
    type: File
  normal_dna_Final_Bam:
    type: File
    

stdout: out.json

outputs:
  std_out:
    type: stdout
  file_out:
    type: File
    outputBinding:
      glob: "report.json"

baseCommand: [python, myscript.py]
