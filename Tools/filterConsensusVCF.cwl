#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool


requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/filterconsensusvcf:9ab238333"
  InlineJavascriptRequirement: {}
inputs:

  ConsensusVariants:
    type: File
    inputBinding:
      position: 1

baseCommand: [Rscript, /bin/filterConsensusVCF.R]

outputs:
  filteredConsensusVCF:
    type: File
    outputBinding:
      glob: "vcf_counted.filtered.vcf.gz"
