#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/bedops:latest"

inputs:
  Chromosome:
    type: string
    inputBinding:
      position: 1

  Bed:
    type: File
    format: http://edamontology.org/format_3003  # BED
    inputBinding:
      position: 2

baseCommand: [bedextract]

stdout: subset.bed

outputs:
  File:
    type: stdout
    format: http://edamontology.org/format_3003  # BED
