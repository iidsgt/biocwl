#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/bedops:latest"

inputs:
  Bed:
    format: http://edamontology.org/format_3003  # BED
    type: File
    inputBinding:
      position: 1

baseCommand: [sort-bed]

stdout: sorted.bed

outputs:
  File:
    type: stdout
    format: http://edamontology.org/format_3003  # BED
