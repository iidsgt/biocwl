#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "biocontainers/vcftools:0.1.15"
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing: $([inputs.Bed].concat(inputs.Bed.secondaryFiles))

inputs:
  Bed:
    type: File
    format: http://edamontology.org/format_3003  # BED
    inputBinding:
      position: 1
      valueFrom: $(self.basename).gz
    secondaryFiles:
    - .gz

baseCommand: [tabix, "-p", bed]

outputs:
  File:
    type: File
    outputBinding:
      glob: $(inputs.Bed.basename)
    secondaryFiles:
    - .gz
    - .gz.tbi