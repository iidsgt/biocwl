#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "biocontainers/vcftools:0.1.15"
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - $(inputs.In)

inputs:
  In:
    type: File
    inputBinding:
      position: 1
      valueFrom: $(self.basename)

baseCommand: ["bgzip"]

arguments: [ "-c" ]

stdout: $(inputs.In.basename + ".gz")

outputs:
  Out:
    type: File
    format: $(inputs.In.format)
    outputBinding:
      glob: $(inputs.In.basename)
    secondaryFiles:
    - .gz