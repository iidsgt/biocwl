#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool
requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/strelka:2.9.10"
  InlineJavascriptRequirement: {}

inputs:
  Reference:
    type: File
    secondaryFiles:
      - .dict 
      - .fai
    inputBinding:
      position: 3

  NormalInput:
    type: File
    secondaryFiles:
      - .bai
    inputBinding:
      position: 1

  TumorInput:
    type: File
    secondaryFiles:
      - .bai
    inputBinding:
      position: 2

#Optional Inputs

  IndelCandidates:
    type: File?
    inputBinding:
      position: 6
    secondaryFiles:
      - .tbi

  Threads:
    type: int?
    default: 4
    inputBinding:
      position: 5
    
  Engine:
    type: string?
    default: "local"
    inputBinding:
      position: 4
      valueFrom: "local"
  
  CallRegions:
    type: File?
    secondaryFiles:
      - .gz
      - .gz.tbi
    inputBinding:
      valueFrom: $(self.path + ".gz")
      position: 7

baseCommand: ["/bin/strelka2"]

outputs:
  snv_vcf:
    type: File
    outputBinding:
      glob: 'results/variants/*.snvs.vcf.gz'
    secondaryFiles: ["null", .tbi]
  indel_vcf:
    type: File
    outputBinding:
      glob: 'results/variants/*.indels.vcf.gz'
    secondaryFiles: ["null", .tbi]

