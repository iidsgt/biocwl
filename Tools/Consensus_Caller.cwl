#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/consensus_caller:7d309e4"
  InlineJavascriptRequirement: {}


inputs:
  Mutect2:
    type: File
    inputBinding:
      prefix: "--mutect2"

  Strelka:
    type: File
    inputBinding:
      prefix: "--strelka"

  StrelkaIndels:
    type: File
    inputBinding:
      prefix: "--strelka_indels"

  Lancet:
    type: File
    inputBinding:
      prefix: "--lancet"

  Output:
    type: string
    default: "consensus.vcf"
    inputBinding:
      prefix: "--output"
      valueFrom: "consensus.vcf"

  regionFile:
    type: string
    default: "regions.tsv"
    inputBinding:
      prefix: "--region_file"
      valueFrom: "regions.tsv"


baseCommand: ["/bin/consensus_caller.py"]

outputs:
  consensus_vcf:
    type: File
    outputBinding:
      glob: "consensus.vcf"

  regions:
    type: File
    outputBinding:
      glob: "regions.tsv"


