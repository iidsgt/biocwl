#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/selescore:c95d5b7"
  InlineJavascriptRequirement: {}

inputs:
  rDataFile:
    type: File
    inputBinding:
      position: 1
  
  bostonDist:
    type: File
    inputBinding:
      position: 2

  outputDir:
    type: string
    default: $(runtime.outdir)
    inputBinding:
      valueFrom: $(runtime.outdir)
      position: 3

baseCommand: [Rscript, /R/run_selinescore.R]

outputs:
  selineScoreResults:
    type: File
    outputBinding:
      glob: "*_latest.tsv"