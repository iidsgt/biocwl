#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "biocontainers/samtools:v1.7.0_cv4"
  InlineJavascriptRequirement: {}

inputs:

  InputFile:
    type: File
    streamable: true
    inputBinding:
      position: 100

  Output:
    type: string
    default: "samtoolsviewoutput.dat"
    inputBinding:
      position: 99
      prefix: "-o"
      valueFrom: $("samtoolsviewoutput.dat")

  Format:
    type:
      - type: record
        name: CRAM
        fields:
          CRAM:
            type: boolean
            inputBinding:
              prefix: "-C"

          Reference:
            type: File
            inputBinding:
              prefix: "-T"

      - type: record
        name: BAM
        fields:
          BAM:
            type: boolean
            inputBinding:
              prefix: "-1"

  Region:
    type: string?
    inputBinding:
      position: 101

  IncludeSamHeader:
    type: boolean?
    inputBinding:
      prefix: "-u"

  PrintMatchingRecordCount:
    type: boolean?
    inputBinding:
      prefix: "-c"

  WriteUnFilteredReads:
    type: File?
    inputBinding:
      prefix: "-U"

  ReferenceNames:
    type: File?
    inputBinding:
      prefix: "-t"

  OverlappingReadsBed:
    type: File?
    inputBinding:
      prefix: "-L"

  OnlyIncludeReads:
    type: string?
    inputBinding:
      prefix: "-r"

  OnlyIncludeReadsFile:
    type: File?
    inputBinding:
      prefix: "-R"

  OnlyIncludeReadQual:
    type: int?
    inputBinding:
      prefix: "-q"

  OnlyIncludeLibraryReads:
    type: string?
    inputBinding:
      prefix: "-1"

  OnlyIncludeCigarVal:
    type: int?
    inputBinding:
      prefix: "-m"

  Threads:
    type: int?
    inputBinding:
      prefix: "-@"

  SubSampleReads:
    type: float?
    inputBinding:
      prefix: "-s"

  isMultiRegionIter:
    type: boolean?
    inputBinding:
      prefix: "-M"

  StripReadTag:
    type: 
      - "null"
      - type: array
        items: string
        inputBinding:
          prefix: "-x"

  isCollapseCigar:
    type: boolean?
    inputBinding:
      prefix: "-B"

  Reference:
    type: File?
    inputBinding:
      prefix: "-T"

baseCommand: ["samtools"]

arguments: 
  - valueFrom: "view"
    position: -1


outputs:
  alignment:
    type: File
    outputBinding:
      glob: samtoolsviewoutput.dat



  
