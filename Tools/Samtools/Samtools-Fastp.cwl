#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/samtools2fastp:latest"
  InlineJavascriptRequirement: {}

inputs:

  InputFile:
    type: File
    inputBinding:
      position: 100


baseCommand: ["sam2fastp.sh"]


outputs:
  html:
    type: File
    outputBinding:
      glob: "fastp.html"

  json:
    type: File
    outputBinding:
      glob: "fastp.json"



