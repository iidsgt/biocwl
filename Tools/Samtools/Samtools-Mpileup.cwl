#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "biocontainers/samtools:v1.7.0_cv4"
  InlineJavascriptRequirement: {}

inputs:

#REQUIRED ARGS

  InputFile:
    type: 
      - File[]
      - File
    inputBinding:
      position: 100

  Output:
    type: string
    default: "samtoolempileupoutput.pileup"
    inputBinding:
      prefix: "--output"
      valueFrom: "samtoolempileupoutput.pileup"

#OPTIONAL ARGS

  isIlluminaQualityEncoding:
    type: boolean?
    inputBinding:
      prefix: "--illumina1.3+"

  isCountOrphans:
    type: boolean?
    inputBinding:
      prefix: "--count-orphans"

#This may need to be mut exc
  BamList:
    type: File?
    inputBinding:
      prefix: "--bam-list"

  isNoBAQ:
    type: boolean?
    inputBinding:
      prefix: "--no-BAQ"

  AdjustMQ:
    type: int?
    inputBinding:
      prefix: "--adjust-MQ"

  MaxDepth:
    type: int?
    inputBinding:
      prefix: "--max-depth"

  isRedoBAQ:
    type: boolean?
    inputBinding:
      prefix: "--redo-BAQ"

  Reference:
    type: File?
    inputBinding:
      prefix: "--reference"

  ExcludeRG:
    type: File?
    inputBinding:
      prefix: "--exclude-RG"

  Positions:
    type: File?
    inputBinding:
      prefix: "--positions"

  MinMQ:
    type: int?
    inputBinding:
      prefix: "--min-MQ"

  MinBQ:
    type: int?
    inputBinding:
      prefix: "--min-BQ"

  Regions:
    type: string[]?
    inputBinding:
      prefix: "--region"
      itemSeparator: ","

  isIgnoreRG:
    type: boolean?
    inputBinding:
      prefix: "--ignore-RG"

  IncludeFlags:
    type:
      - "null"
      - type: array
        items:
          type: enum
          symbols:
            - UNMAP
            - SECONDARY
            - QCFAIL
            - DUP
            - PAIRED
            - PROPER_PAIR
            - MUNMAP
            - REVERSE
            - MREVERSE
            - READ1
            - READ2
            - SUPPLEMENTARY
    inputBinding:
      prefix: "--incl-flags"

  ExcludeFlags:
    type:
      - "null"
      - type: array
        items:
          type: enum
          symbols:
            - UNMAP
            - DUP
            - PAIRED
            - PROPER_PAIR
            - MUNMAP
            - REVERSE
            - MREVERSE
            - READ1
            - READ2
            - SUPPLEMENTARY
    inputBinding:
      prefix: "--excl-flags"

  isIgnoreOverlaps:
    type: boolean?
    inputBinding:
      prefix: "--ignore-overlaps"

  isOutputBP:
    type: boolean?
    inputBinding:
      prefix: "--output-BP"
  
  isOutputMQ:
    type: boolean?
    inputBinding:
      prefix: "--output-MQ"

  isOutputQname:
    type: boolean?
    inputBinding:
      prefix: "--output-QNAME"

  isOutputAllPos:
    type: 
      - type: record
        name: AllPos
        fields:
          AllPos:
            type: boolean
            inputBinding:
              prefix: "-a"

          AbsAllPos:
            type: boolean
            inputBinding:
              prefix: "-aa"

  ReferenceIndex:
    type: File?
    inputBinding:
      prefix: "--fasta-ref"

  InputFmtOption:
    type: string?
    inputBinding:
      prefix: "--input-fmt-option"


baseCommand: ["samtools"]

arguments: 
  - valueFrom: "mpileup"
    position: -1


outputs:
  index:
    type: File
    outputBinding:
      glob: "samtoolempileupoutput.pileup"
    secondaryFiles:
      - ^.*

