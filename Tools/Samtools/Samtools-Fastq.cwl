#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "biocontainers/samtools:v1.7.0_cv4"
  InlineJavascriptRequirement: {}


inputs:
  InputFile:
    type: File
    inputBinding:
      position: 100

  Output:
    type: string
    default: reads

  FileSuffix:
    type: string
    default: fq

  Compression:
    type:
    - "null"
    - type: enum
      symbols:
      - gz
      - bgzf
    default: null
    
  ForwardReads:
    type: string
    default: |
      $(inputs.Output + "_1.fq" + (inputs.Compression ? ("." + inputs.Compression) : ""))
    inputBinding:
      prefix: "-1"
      valueFrom: |
        $(inputs.Output + "_1.fq" + (inputs.Compression ? ("." + inputs.Compression) : ""))

  ReverseReads:
    type: string
    default: |
      $(inputs.Output + "_2.fq" + (inputs.Compression ? ("." + inputs.Compression) : ""))
    inputBinding:
      prefix: "-2"
      valueFrom: |
        $(inputs.Output + "_2.fq" + (inputs.Compression ? ("." + inputs.Compression) : ""))

  Threads:
    type: int?
    inputBinding:
      prefix: "-@"

baseCommand: [samtools]

arguments:
  - valueFrom: "fastq"
    position: -1

outputs:
  Forward_Reads:
    type: File
    format: http://edamontology.org/format_1930 # FASTA
    outputBinding:
      glob: |
        $(inputs.Output + "_1.fq" + (inputs.Compression ? ("." + inputs.Compression) : ""))

  Reverse_Reads:
    type: File
    format: http://edamontology.org/format_1930 # FASTA
    outputBinding:
      glob: |
        $(inputs.Output + "_2.fq" + (inputs.Compression ? ("." + inputs.Compression) : ""))