#!/usr/bin/env cwl-runner
cwlVersion: v1.1
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/vicc-api:06c53d55"
  InlineJavascriptRequirement: {}
  NetworkAccess:
    networkAccess: true


inputs:
  Input:
    type: File
    inputBinding:
      position: 100

  Url:
    type: string
    default: "https://search.cancervariants.org"
    inputBinding:
      prefix: "--es_url"

  Type:
    type: string?
    inputBinding:
      prefix: "--type"

  SampleID:
    type: string
    inputBinding:
      prefix: "--sample_id"

  Output:
    type: string
    default: "data.csv"
    inputBinding:
      prefix: "--output"
      valueFrom: "data.csv"

baseCommand: ["/bin/sonofvicctor.py"]

outputs:
  vicc_out:
    type: File
    outputBinding:
      glob: "vicc*"

  civic_out:
    type: File
    outputBinding:
      glob: "civic*"