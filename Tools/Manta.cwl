#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool
requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/manta:latest"
  InlineJavascriptRequirement: {}

inputs:
  Reference:
    type: File
    secondaryFiles:
      - .dict 
      - .fai
    inputBinding:
      position: 3

  NormalInput:
    type: File
    secondaryFiles:
      - .bai
    inputBinding:
      position: 1

  TumorInput:
    type: File
    secondaryFiles:
      - .bai
    inputBinding:
      position: 2

#Optional Inputs
  Threads:
    type: int
    default: 4
    inputBinding:
      position: 5
    
  Engine:
    type: string?
    default: "local"
    inputBinding:
      position: 4
      valueFrom: "local"

baseCommand: ["/bin/manta"]

outputs:
  candidate_indels:
    type: File
    outputBinding:
      glob: 'results/variants/candidateSmallIndels.vcf.gz'
    secondaryFiles:
      - .tbi

