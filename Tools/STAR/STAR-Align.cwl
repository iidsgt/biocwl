#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

$namespaces:
  arv: "http://arvados.org/cwl#"
  cwltool: "http://commonwl.org/cwltool#"

requirements:
  DockerRequirement:
    dockerPull: "quay.io/biocontainers/star:2.7.9a--h9ee0642_0"

inputs:
  # Required Inputs
  RunThreadN:
    type: int
    inputBinding:
      prefix: "--runThreadN"

  Index:
    type: Directory
    inputBinding:
      prefix: "--genomeDir"

  InputFiles:
    type: File[]
    inputBinding:
      prefix: "--readFilesIn"
      position: 1

  # Optional Inputs
  Gtf:
    type: File?
    inputBinding:
      prefix: "--sjdbGTFfile"

  Overhang:
    type: int?
    inputBinding:
      prefix: "--sjdbOverhang"

  OutFilterType:
    type:
     - "null"
     - type: enum
       symbols:
        - Normal
        - BySJout
    inputBinding:
      prefix: "--outFilterType"

  OutFilterIntronMotifs:
    type:
     - "null"
     - type: enum
       symbols:
        - None
        - RemoveNoncanonical
        - RemoveNoncanonicalUnannotated
    inputBinding:
      prefix: "--outFilterIntronMotifs"
  
  OutSAMtype:
    type: string[]?
    inputBinding:
      prefix: "--outSAMtype"

  twopassMode:
    type: string?
    inputBinding:
      prefix: "--twopassMode"

  outSAMattrRGline:
    type: string?
    inputBinding:
      prefix: "--outSAMattrRGline"

  ReadFilesCommand:
    type: string?
    inputBinding:
      prefix: "--readFilesCommand"

  chimMultimapScoreRange:
    type: int?
    inputBinding:
      prefix: "--chimMultimapScoreRange"

  peOverlapNbasesMin:
    type: int?
    inputBinding:
      prefix: "--peOverlapNbasesMin"

  peOverlapMMp:
    type: float?
    inputBinding:
      prefix: "--peOverlapMMp"

  chimNonchimScoreDropMin:
    type: int?
    inputBinding:
      prefix: "--chimNonchimScoreDropMin"


  chimMultimapNmax:
    type: int?
    inputBinding:
      prefix: "--chimMultimapNmax"

  chimScoreJunctionNonGTAG:
    type: int?
    inputBinding:
      prefix: "--chimScoreJunctionNonGTAG"

  AlignIntronMin:
    type: int?
    inputBinding:
      prefix: "--alignIntronMin"
  
  AlignIntronMax:
    type: int?
    inputBinding:
      prefix: "--alignIntronMax"

  chimSegmentReadGapMax:
    type: int?
    inputBinding:
      prefix: "--chimSegmentReadGapMax"

  alignSJstitchMismatchNmax:
    type: int[]?
    inputBinding:
      prefix: "--alignSJstitchMismatchNmax"

  AlignMatesGapMax:
    type: int?
    inputBinding:
      prefix: "--alignMatesGapMax"

  AlignSJoverhangMin:
    type: int?
    inputBinding:
      prefix: "--alignSJoverhangMin"
  
  AlignSJDBoverhangMin:
    type: int?
    inputBinding:
      prefix: "--alignSJDBoverhangMin"

  chimScoreSeparation:
    type: int?
    inputBinding:
      prefix: "--chimScoreSeparation"

  SeedSearchStartLmax:
    type: int?
    inputBinding:
      prefix: "--seedSearchStartLmax"

  chimScoreDropMax:
    type: int?
    inputBinding:
      prefix: "--chimScoreDropMax"

  chimScoreMin:
    type: int?
    inputBinding:
      prefix: "--chimScoreMin"

  ChimOutType:
    type: string[]?
    inputBinding:
      prefix: "--chimOutType"

  ChimSegmentMin:
    type: int?
    inputBinding:
      prefix: "--chimSegmentMin"
    
  ChimJunctionOverhangMin:
    type: int?
    inputBinding:
      prefix: "--chimJunctionOverhangMin"

  chimOutJunctionFormat:
    type: int?
    inputBinding:
      prefix: "--chimOutJunctionFormat"

  OutFilterMultimapNmax:
    type: int?
    inputBinding:
      prefix: "--outFilterMultimapNmax"

  outBAMcompression:
    type: int?
    inputBinding:
      prefix: "--outBAMcompression  "

  OutFilterMismatchNmax:
    type: int?
    inputBinding:
      prefix: "--outFilterMismatchNmax"

  OutFilterMismatchNoverLmax:
    type: double?
    inputBinding:
      prefix: "--outFilterMismatchNoverLmax"

  OutReadsUnmapped:
    type:
     - "null"
     - type: enum
       symbols:
        - None
        - Fastx
    inputBinding:
      prefix: "--outReadsUnmapped"
  
  OutSAMstrandField:
    type:
     - "null"
     - type: enum
       symbols:
        - None
        - intronMotif
    inputBinding:
      prefix: "--outSAMstrandField"
  
  OutSAMunmapped:
    type:
     - "null"
     - type: enum
       symbols:
        - None
        - Within
        - "Within KeepPairs"
    inputBinding:
      prefix: "--outSAMunmapped"
  
  OutSAMmapqUnique:
    type: int?
    inputBinding:
      prefix: "--outSAMmapqUnique"
  
  OutSamMode:
    type: 
     - "null"
     - type: enum
       symbols:
        - None
        - Full
        - NoQS
    inputBinding:
      prefix: "--outSAMmode"
  
  LimitOutSAMoneReadBytes:
    type: int?
    inputBinding:
      prefix: "--limitOutSAMoneReadBytes"

  LimitBAMsortRAM:
    type: long?
    inputBinding:
      prefix: "--limitBAMsortRAM"

  OutFileNamePrefix:
    type: string?
    inputBinding:
      prefix: "--outFileNamePrefix"

  outStd:
    type: string?
    inputBinding:
      prefix: "--outStd"

  GenomeLoad:
    type:
     - "null"
     - type: enum
       symbols:
        - LoadAndKeep
        - LoadAndRemove
        - LoadAndExit
        - Remove
        - NoSharedMemory
    inputBinding:
      prefix: "--genomeLoad"

baseCommand: [STAR]     

arguments:
  - valueFrom: "--runMode alignReads"
    position: -1

outputs:
  alignment_unsorted:
    type:  ["null", File]
    outputBinding:
      glob: "Aligned.out.bam"
  alignment:
    type:  File
    outputBinding:
      glob: "Aligned.sortedByCoord.out.bam"

  unmapped_reads_1:
    type: ["null", File]
    outputBinding:
      glob: "Unmapped.out.mate1"

  unmapped_reads_2:
    type: ["null", File]
    outputBinding:
      glob: "Unmapped.out.mate2"

  chimeric_junctions:
    type: ["null", File]
    outputBinding:
      glob: "Chimeric.out.junction"
      
  chimericreads:
    type: ["null", File]
    outputBinding:
      glob: "Chimeric.out.sam"
  starLog:
    type: ["null", File]
    outputBinding:
      glob: "Log.final.out"
