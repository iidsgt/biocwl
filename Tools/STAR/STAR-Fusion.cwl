#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "quay.io/biocontainers/star-fusion:0.7.0--1"


inputs:
  # Required Inputs
  CTATGenomeLib:
    type: string
    inputBinding:
      prefix: "--genome_lib_dir"

  ChimericOutJunction:
    type: File
    inputBinding:
      prefix: "-J"

  Output:
    type: string
    default: "out"
    inputBinding:
      prefix: "--output_dir"
      valueFrom: "out"


baseCommand: [STAR-Fusion]

arguments:
  - valueFrom: "--runMode alignReads"
    position: -1

outputs:
  fusion_predictions:
    type: File
    outputBinding:
      glob: "star-fusion.fusion_predictions.tsv"

  fusion_predictions_abridged:
    type: File
    outputBinding:
      glob: "star-fusion.fusion_predictions.abridged.tsv"
