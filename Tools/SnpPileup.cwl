#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "waleedosman/facets_test:firsttry"
  InlineJavascriptRequirement: {}

inputs:
  PairName:
    type: string?
    default: "snppileupout.pileup"
    inputBinding:
      position: 101
      valueFrom: |
        ${
          if (inputs.isGzip == true)
              return "snppileupout.pileup.gz";
          else
              return "snppileupout.pileup";
        }

  TumorBam:
    type: File
    inputBinding:
      position: 103

  NormalBam:
    type: File
    inputBinding:
      position: 102

  DbSnp:
    type: File
    inputBinding:
      position: 100

#Optional

  MinMapQuality:
    type: int?
    default: 15
    inputBinding:
      prefix: "--min-map-quality"

  MinBaseQuality:
    type: int?
    default: 20
    inputBinding:
      prefix: "--min-base-quality"

  MinReadCounts:
    type: int[]?
    default: [25,0]
    inputBinding:
      itemSeparator: ","
      prefix: "--min-read-counts"

  PseudoSnps:
    type: int?
    default: 100
    inputBinding:
      prefix: "--pseudo-snps" 

  isGzip:
    type: boolean?
    default: false
    inputBinding:
      prefix: "--gzip"

  isVerbose:
    type: boolean?
    inputBinding:
      prefix: "--verbose"

baseCommand: [snp-pileup]

outputs:
  pileup:
    type: File
    outputBinding:
      glob: |
        ${
          if (inputs.isGzip == true)
              return "snppileupout.pileup.gz";
          else
              return "snppileupout.pileup";
        }
