#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "quay.io/biocontainers/arriba:1.0.1--h10824c4_0"

inputs:
  # Required Inputs
  ChimericOutBam:
    type: File
    inputBinding:
      prefix: "-x"

  AnnotationGtf:
    type: File
    inputBinding:
      prefix: "-g"

  AssemblyFasta:
    type: File
    inputBinding:
      prefix: "-a"

  BlackList:
    type: File
    inputBinding:
      prefix: "-b"

  FusionsOutput:
    type: string
    default: "fusions.tsv"
    inputBinding:
      prefix: "-o"

  FusionsDiscardedOutput:
    type: string
    default: "fusions.discarded.tsv"
    inputBinding:
      prefix: "-O"

  isPeptidePrint:
    type: string?
    default: "-P"
    inputBinding:
      prefix: "-P"

baseCommand: [arriba]

outputs:
  fusions:
    type: File
    outputBinding:
      glob: "fusions.tsv"

  fusion_discarded:
    type: File
    outputBinding:
      glob: "fusions.discarded.tsv"

