#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "quay.io/biocontainers/arriba:1.0.1--h10824c4_0"
inputs:
  # Required Inputs
  ChimericOutBam:
    type: File
    inputBinding:
      prefix: "--alignments="
      separate: false
    secondaryFiles:
      - .bai

  AnnotationGtf:
    type: File
    inputBinding:
      prefix: "--annotation="
      separate: false

  Fusions:
    type: File
    inputBinding:
      prefix: "--fusions="
      separate: false

  PdfOutput:
    type: string?
    default: "fusions.pdf"
    inputBinding:
      prefix: "--output="
      separate: false

  CytoBands:
    type: File
    inputBinding:
      prefix: "--cytobands="
      separate: false

  ProteinDomains:
    type: File
    inputBinding:
      prefix: "--proteinDomains="
      separate: false


baseCommand: [draw_fusions.R]

outputs:
  fusionsPDF:
    type: File
    outputBinding:
      glob: "fusions.pdf"