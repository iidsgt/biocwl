#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/mixenrich:ccc7a6b"
  InlineJavascriptRequirement: {}


inputs:

  SampleName:
    type: string
    inputBinding:
      position: 1

  RData:
    type: File
    inputBinding:
      position: 2

  OutputDir:
    type: string
    default: "MixEnrich.csv"
    inputBinding:
      position: 3
      valueFrom: "MixEnrich.csv"

  GeneConvTable:
    type: File
    inputBinding:
      position: 4

  Pathways:
    type: File
    inputBinding:
      position: 5

baseCommand: [Rscript, /bin/MixEnrich_daphniv2.R]

outputs:
  MixEnrichTable:
    type: File
    outputBinding:
      glob: "MixEnrich.csv"

