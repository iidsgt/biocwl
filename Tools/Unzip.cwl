#!/usr/bin/env cwl-runner

cwlVersion: v1.0

class: CommandLineTool

requirements:
  InlineJavascriptRequirement: {}
  DockerRequirement:
    dockerPull: samepagelabs/zip

inputs:
  - id: InputFile
    type: File
    inputBinding:
      position: 0

outputs:
  - id: unzipped_directory
    type: Directory
    outputBinding:
      glob: $("./")

arguments:
  - valueFrom: "-d"
    position: 1
  - valueFrom: "./"
    position: 2

baseCommand: [unzip]
# Source adapted from https://github.com/jeremiahsavage/cwl_zip_unzip/blob/b10408c9d2189209127b68072a8095ebf5acbf1f/unzip.cwl
