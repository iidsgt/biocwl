#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/dnapathways:f4e2cb7"
  InlineJavascriptRequirement: {}

inputs:

  pathwayFile:
    type: File
    inputBinding:
      position: 1

  OncoFile:
    type: File
    inputBinding:
      position: 2

  CNVFile:
    type: File
    inputBinding:
      position: 3

  MixEnrichOut:
    type: File
    inputBinding:
      position: 4

  RNARData:
    type: File
    inputBinding:
      position: 5

  Sample_Name:
    type: string
    inputBinding:
      position: 6

  OutputDir:
    type: string
    default: "pathways_out_table"
    inputBinding:
      position: 7
      valueFrom: "pathways_out"

baseCommand: [Rscript, /bin/Daphni2_DNA_Pathways.R]

outputs:
  DNAPathwaysOut:
    type:
      - "null"
      - File[]
    outputBinding:
      glob: "*.png"

  DNAPathwaysTableOut:
    type: ["null", File]
    outputBinding:
      glob: "pathways_out"
