#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
   DockerRequirement:
      dockerPull: "sinaiiidgst/myelomahallmarks:3ae9283" # with fusions
    # dockerPull: "sinaiiidgst/myelomahallmarks:beaf2e6"
   InlineJavascriptRequirement: {}

# required run-time and sample metadata arguments
inputs:
  
  ProgMarkTable:
    type: File
    inputBinding:
      position: 1

  dnaSampleID:
    type: string
    inputBinding:
     position: 2
     
  TranslocTable:
    type: File
    inputBinding:
      position: 3
  
  FacetncytTable:
    type: File
    inputBinding:
      position: 4
  
  ScarScorePath:
    type: File
    inputBinding:
      position: 5
  
  AnnotatedVariantsPath:
    type: File
    inputBinding:
      position: 6

  outdir:
    type: string
    default: $(runtime.outdir)
    inputBinding:
      valueFrom: $(runtime.outdir)
      position: 7

  FusionsTable:
    type: File
    inputBinding:
      position: 8


 
baseCommand: [Rscript, /bin/trialcheck.r]

outputs:
 
  FinalOutput:
    type: File
    outputBinding:
      glob: "*results.csv"

