#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "conda/c3i-linux-64"
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - entryname: HTMLReport.html
        entry: $(inputs.HTMLReport)
        writable: true
      - entryname: curl.py
        writable: true
        entry: |-
          import requests
          import datetime
          import time
          time_now = datetime.datetime.now()
          f = open("HTMLReport.html").read()
          def send_simple_message():
            response = requests.post(
                              "https://daphni-test.nextgenhealthcare.org/api/analysis",
                              headers ={
                              'Content-Type': 'application/json',
                              'Authorization': 'Bearer $(inputs.AuthToke)'
                              },
                              json={
                                     "patient": "25856923",
                                     "time_point": 5,
                                     "tumor_rna": "MM_0068_RNA_T_05_01",
                                     "tumor_dna": "MM_0068_DNA_T_05_01",
                                     "normal_dna": "MM_0068_DNA_N_01_01",
                                     "report_html": f
                                   }
                                    )

            print(response.text)
            print(response.status_code)


          send_simple_message()

inputs:
  HTMLReport:
    type: File
    inputBinding:
      position: 100

  AuthToke:
    type: string
    inputBinding:
      position: 100

baseCommand: ["python", "curl.py"]

stdout: out

outputs:
  out:
    type: stdout
