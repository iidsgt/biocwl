#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

doc: |
  Modified from https://github.com/common-workflow-library/bio-cwl-tools/blob/8b925555a78a42b9e7c0bee7a966f84a90791123/fastp/fastp.cwl
  Modified from https://github.com/nigyta/bact_genome/blob/master/cwl/tool/fastp/fastp.cwl

requirements:
    DockerRequirement:
        dockerPull: "quay.io/biocontainers/fastp:0.20.0--hdbcaa40_0"
    InlineJavascriptRequirement: {}

baseCommand: [fastp]

arguments:
    - prefix: -o
      valueFrom: $(inputs.fastq1.nameroot)_R1.fastp.fastq.gz
    - prefix: -O
      valueFrom: $(inputs.fastq2.nameroot)_R2.fastp.fastq.gz

inputs:
    # REQUIRED
    fastq1:
      type: File
      # format: http://edamontology.org/format_1930 # FASTA
      inputBinding:
        prefix: -i
    fastq2:
      # format: http://edamontology.org/format_1930 # FASTA
      type: File
      inputBinding:
        prefix: -I

    # OPTIONAL
    threads:
      type: int?
      default: 12
      inputBinding:
        prefix: --thread
    qualified_phred_quality:
      type: int?
      default: 20
      inputBinding:
        prefix: --qualified_quality_phred
    unqualified_phred_quality:
      type: int?
      default: 20
      inputBinding:
        prefix: --unqualified_percent_limit
    min_length_required:
      type: int?
      default: 50
      inputBinding:
        prefix: --length_required
    force_polyg_tail_trimming:
      type: boolean?
      inputBinding:
        prefix: --trim_poly_g
    trim_poly_x:
      type: boolean?
      default: true
      inputBinding:
        prefix: --trim_poly_x
    disable_trim_poly_g:
      type: boolean?
      default: true
      inputBinding:
        prefix: --disable_trim_poly_g
    base_correction:
      type: boolean?
      default: true
      inputBinding:
        prefix: --correction
    cut_tail:
      type: boolean?
      default: true
      inputBinding:
        prefix: --cut_tail
    detect_adapter_for_pe:
      type: boolean
      default: false
      inputBinding:
        prefix: --detect_adapter_for_pe

outputs:
    out_fastq1:
      type: File
      outputBinding:
        glob: $(inputs.fastq1.nameroot)_R1.fastp.fastq.gz
    out_fastq2:
      type: File
      outputBinding:
        glob: $(inputs.fastq2.nameroot)_R2.fastp.fastq.gz
    html_report:
      type: File
      outputBinding:
        glob: fastp.html
    json_report:
      type: File
      outputBinding:
        glob: fastp.json
