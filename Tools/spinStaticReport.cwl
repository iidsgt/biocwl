#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
    InlineJavascriptRequirement: {}
    DockerRequirement:
        dockerPull: "sinaiiidgst/spinreport:c141581" 
inputs:
    # required run-time and sample metadata arguments
    outdir:
        type: string
        default: $(runtime.outdir)
        inputBinding:
            valueFrom: $(runtime.outdir)
            prefix: "--outdir"
    tiers:
        type: string
        inputBinding:
            prefix: "--tiers"
    sampleName:
        type: string
        inputBinding:
            prefix: "--sampleName"
    sexChrs:
        type: string
        inputBinding:
            prefix: "--sexChrs"

    # required reference files
    geneCoordinates:
        type: File
        inputBinding:
            prefix: "--geneCoordinates"
    cytobandCoordinates:
        type: File
        inputBinding:
            prefix: "--cytobandCoordinates"
    psnReferenceTable:
        type: File
        inputBinding:
            prefix: "--psnReferenceTable"

    # required pred engine outputs
    drugDetailsRanked:
        type: File
        inputBinding:
            prefix: "--drugDetailsRanked"

    variantSupportSummary:
        type: File
        inputBinding:
            prefix: "--variantSupportSummary"

    variantDetailsTable:
        type: File
        inputBinding:
            prefix: "--variantDetailsTable"
    # These parameters should be included if data is available
    
    # optional (but strongly recommended where avaialble) clinical info
    dateSampleCollected:
        type: string?
        inputBinding:
            prefix: "--dateSampleCollected"

    patientName:
        type: string?
        inputBinding:
            prefix: "--patientName"
    DOB:
        type: string?
        inputBinding:
            prefix: "--DOB"
    MRN:
        type: string?
        inputBinding:
            prefix: "--MRN"

    # cohort level files
    zScoreTable:
        type: File?
        inputBinding:
            prefix: "--zScoreTable"
    seliFile:
        type: File?
        inputBinding:
            prefix: "--seliFile"
    gep70File:
        type: File?
        inputBinding:
            prefix: "--gep70File"
    msiFile:
        type: File?
        inputBinding:
            prefix: "--msiFile"
    tmbFile:
        type: File?
        inputBinding:
            prefix: "--tmbFile"
    scarFile:
        type: File?
        inputBinding:
            prefix: "--scarFile"

    # variant-type results from prediciton engine
    prognosticMarkerSummary:
        type: File?
        inputBinding:
            prefix: "--prognosticMarkerSummary"
    somFile:
        type: File?
        inputBinding:
            prefix: "--somFile"
    snvFile:
        type: File?
        inputBinding:
            prefix: "--snvFile"
    cnaFile:
        type: File?
        inputBinding:
            prefix: "--cnaFile"

    exprFile:
        type: File?
        inputBinding:
            prefix: "--exprFile"
    
    # other inputs from secondary or primary
    facetsCNCFResultsFile:
        type: File?
        inputBinding:
            prefix: "--facetsCNCFResultsFile"
    tumorPurityPloidy:
        type: File?
        inputBinding:
            prefix: "--tumorPurityPloidy"
    treeFile:
        type: File?
        inputBinding:
            prefix: "--treeFile"
    mmPSNFile:
        type: File?
        inputBinding:
            prefix: "--mmPSNFile"
    # multiqc zip folder
    multiQCFile:
        type: File?
        inputBinding:
            prefix: "--multiqc"

baseCommand: [Rscript, /bin/spin_report.r]

outputs:
    report:
        type: File
        outputBinding:
            glob: "*main.daphni_report.html"
    figures:
        type: File[]
        outputBinding:
            glob: "*.svg"
    kables:
        type: File[]
        outputBinding:
            glob: "*kable.daphni_report.html"
