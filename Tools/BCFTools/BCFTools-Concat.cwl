#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

hints:
  DockerRequirement:
    dockerPull: "quay.io/biocontainers/bcftools:1.7--0"

inputs:

  InputFiles:
    type: File[]
    inputBinding:
      position: 100

  Output:
    type: string
    default: "merged.vcf"
    inputBinding:
      prefix: "--output"
      valueFrom: "merged.vcf"

  Threads:
    type: int?
    inputBinding:
      prefix: "--threads"

baseCommand: [bcftools]

arguments:
  - valueFrom: "concat"
    position: -1

outputs:
  merged_vcf:
    type: File
    outputBinding:
      glob: "merged.vcf"