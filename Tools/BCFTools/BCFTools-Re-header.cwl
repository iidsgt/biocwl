#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

hints:
  DockerRequirement:
    dockerPull: "quay.io/biocontainers/bcftools:1.7--0"
  InitialWorkDirRequirement:
    listing:
      - entryname: samples.txt
        entry: |-
          tumor $(inputs.SampleName)

inputs:
  InputFile:
    type: File
    inputBinding:
      position: 100

  SampleName:
    type: File
    default: "samples.txt"
    inputBinding:
      prefix: "--samples"
      valueFrom: "samples.txt"

  Output:
    type: string
    default: "reheaded.vcf"
    inputBinding:
      prefix: "--output"
      valueFrom: "reheaded.vcf"

  Threads:
    type: int?
    inputBinding:
      prefix: "--threads"

baseCommand: [bcftools]

arguments:
  - valueFrom: "reheader"
    position: -1

outputs:
  reheaded_vcf:
    type: File
    outputBinding:
      glob: "reheaded.vcf"

