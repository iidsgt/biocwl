#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "conda/c3i-linux-64"
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - entryname: check.py
        writable: true
        entry: |-
          import csv
          import sys
          import os
          if (len(sys.argv) == 3):
            if sys.argv[2] == "test":
              sys.stdout.write("test")
              sys.exit(0)
          p = os.path.join(sys.argv[1], "output_matched.txt")
          with open(p) as fp:
              s = fp.readlines()
              if len(s) == 0:
                sys.stdout.write("Samples Are Not Matched.")
                sys.exit(1)
              s = s[0].split("\t")
          if "matched" in s:
              sys.stdout.write("Samples Matched")
              sys.exit(0)
          else:
              sys.stdout.write(f"Samples are mismatched: {s[0]}:{s[2]}, Percent: {s[3]}")
              sys.exit(1)

inputs:
  InputFile:
    type: Directory
    inputBinding:
      position: 2

  isTestSampleInput:
    type: boolean?
    inputBinding:
      position: 3
      valueFrom: "test"

baseCommand: ["python", "check.py"]

stdout: value

outputs:
  isMatched:
    type: stdout


