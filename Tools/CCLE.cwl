#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/ccle:cf26339"

inputs:
  RSEMGeneResults:
    type: File
    inputBinding:
      position: 1

  OutputCNV:
    type: File
    inputBinding:
      position: 2

  AnnotatedVariants:
    type: File
    inputBinding:
      position: 3

  CCLEPredictedOut:
    type: string
    default: "Predicted_out_ccle"
    inputBinding:
      position: 4

baseCommand: [python, /bin/final_ccle_classifier.py]

outputs:
  predicted_out_ccle:
    type: File
    outputBinding:
      glob: "Predicted_out_ccle"

