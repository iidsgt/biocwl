#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

hints:
  DockerRequirement:
    dockerPull: "sinaiiidgst/cloneannotate:4477bd2"
  InlineJavascriptRequirement: {}


inputs:
  MutationVicc:
    type: File
    inputBinding:
      position: 1


  MutationCivic:
    type: File
    inputBinding:
      position: 2

  CNVVicc:
    type: File
    inputBinding:
      position: 3


  CNVCivic:
    type: File
    inputBinding:
      position: 4

  ClusterGeneList:
    type: File
    inputBinding:
      position: 5

  CNVCloneTable:
    type: File
    inputBinding:
      position: 6

baseCommand: [Rscript, /bin/Vicc_Civic_Clone_Annotate.R]

outputs:
  ViccMutationCloneData:
    type: File
    outputBinding:
      glob: "vicc_mutation_clone_data.txt"

  CivicMutationCloneData:
    type: File
    outputBinding:
      glob: "civic_mutation_clone_data.txt"

  ViccCNVCloneData:
    type: File
    outputBinding:
      glob: "vicc_cnv_clone_data.txt"

  CivicCNVCloneData:
    type: File
    outputBinding:
      glob: "civic_cnv_clone_data.txt"