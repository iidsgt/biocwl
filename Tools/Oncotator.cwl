#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "gcr.io/broad-getzlab-workflows/cga_production_pipeline:v1.1"
  InlineJavascriptRequirement: {}

inputs:
  # REQUIRED ARGS

  InputFile:
    type: File
    inputBinding:
  
  Output:
    type: string
    default: $("Oncotator" + inputs.InputFile.nameext)
    inputBinding:
      valueFrom: $("Oncotator" + inputs.InputFile.nameext)

  GenomeBuild:
    type: string
    inputBinding:


  # OPTIONAL ARGS

  Help:
    type: boolean?
    inputBinding:
      prefix: "--help"

  Verbose:
    type: boolean?
    inputBinding:
      prefix: "--verbose"

  Version:
    type: boolean?
    inputBinding:
      prefix: "--version"

  InputFormat:
    type: 
     - "null"
     - type: enum
       symbols:
        - TCGAMAF
        - SEG_FILE
        - VCF
        - MAFLITE
    inputBinding:
      prefix: "--input_format"

  DbDir:
    type: Directory[]
    inputBinding:
      prefix: "--db-dir"

  OutputFormat:
    type:
     - "null"
     - type: enum
       symbols:
        - TCGAMAF
        - VCF
        - SIMPLE_TSV
        - TCGAVCF
        - SIMPLE_BED
        - GENE_LIST
    inputBinding:
      prefix: "--output_format"

  OverrideConfig:
    type: File?
    inputBinding:
      prefix: "--override_config"

  DefaultConfig:
    type: File?
    inputBinding:
      prefix: "--default_config"

  NoMulticore:
    type: boolean?
    inputBinding:
      prefix: "--no-multicore"

  AnnotateManual:
    type: string[]?
    inputBinding:
      prefix: "--annotate-manual"
  
  AnnotateDefault:
    type: string[]?
    inputBinding:
      prefix: "--annotate-default"
  
  CacheUrl:
    type: string?
    inputBinding:
      prefix: "--cache-url"

  ReadOnlyCache:
    type: boolean?
    inputBinding:
      prefix: "--read_only_cache"

  TxMode:
    type:
      - "null"
      - type: enum
        symbols:
         - CANONICAL
         - EFFECT
    inputBinding:
      prefix: "--tx-mode"

  InferGenotypes:
    type:
     - "null"
     - type: enum
       symbols:
        - yes
        - "true"
        - t
        - "1"
        - y
        - no
        - "false"
        - f
        - "0"
        - n
    inputBinding:
      prefix: "--infer_genotypes"

  SkipNoAlt:
    type: boolean?
    inputBinding:
      prefix: "--skip-no-alt"

  LogName:
    type: string?
    inputBinding:
      prefix: "--log_name"

  Prepend:
    type: boolean?
    inputBinding:
      prefix: "--prepend"

  InferOnps:
    type: boolean?
    inputBinding:
      prefix: "--infer-onps"

  CanonicalTxFile:
    type: File?
    inputBinding:
      prefix: "--canonical-tx-file"

  CollapseFilterCols:
    type: boolean?
    inputBinding:
      prefix: "--collapse-filter-cols"

  ReannotateTcgaMafCols:
    type: boolean?
    inputBinding:
      prefix: "--reannotate-tcga-maf-cols"

  AllowOverwriting:
    type: boolean?
    inputBinding:
      prefix: "--allow-overwriting"

  CollapseNumberAnnotations:
    type: boolean?
    inputBinding:
      prefix: "--collapse-number-annotations"

  LongerOtherTx:
    type: boolean?
    inputBinding:
      prefix: "--longer-other-tx"

  PruneFilterCols:
    type: boolean?
    inputBinding:
      prefix: "--prune-filter-cols"


baseCommand: ["/root/oncotator_venv/bin/oncotator"]

outputs:
  index:
    type: File
    outputBinding:
      glob: $("Oncotator" + inputs.InputFile.nameext)
