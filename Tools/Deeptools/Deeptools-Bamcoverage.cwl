#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "quay.io/biocontainers/deeptools:3.0.1--py35_1"
  InlineJavascriptRequirement: {}

inputs:
  InputFile:
    type: File
    inputBinding:
      prefix: "--bam"
    secondaryFiles:
      - .bai

  Output:
    type: string
    default: $("coverage." + inputs.OutFileFormat)
    inputBinding:
      prefix: "--outFileName"
      valueFrom: $("coverage." + inputs.OutFileFormat)

  OutFileFormat:
    type:
      - type: enum
        symbols:
          - "bigwig"
          - "bedgraph"
    inputBinding:
      prefix: "--outFileFormat"

baseCommand: ["bamCoverage"]

outputs:
  coverage:
    type: File?
    outputBinding:
      glob: ["*.bigwig", "*.bedgraph"]
