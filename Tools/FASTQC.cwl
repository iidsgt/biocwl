#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "biocontainers/fastqc:v0.11.5_cv3"

inputs:
  InputRead1:
    type: File
    inputBinding:
      position: 100

  #Optional Inputs
  isCasava:
    type: boolean?
    inputBinding:
      position: 1
      prefix: "--casava"
  
  Format:
    type:
      - "null"
      - type: enum
        symbols:
        - bam
        - sam
        - bam_mapped
        - sam_mapped
        - fastq
    inputBinding:
      position: 2
      prefix: "--format"

  Kmers:
    type: int?
    inputBinding:
      position: 3
      prefix: "-k"

  Adapters:
    type: File?
    inputBinding:
      position: 4
      prefix: "-a"

  Contaminants:
    type: File?
    inputBinding:
      position: 5
      prefix: "-c"

baseCommand: [fastqc, --outdir, ./, --extract]

outputs:
  zippedFile:
    type: ["null", File]
    outputBinding:
      glob: '*.zip'

  report:
    type: Directory
    outputBinding:
      glob: ./out
