#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/dnamerge:7a89032"
  InlineJavascriptRequirement: {}

inputs:
  sampleNameList:
    type: string[]
    inputBinding:
      itemSeparator: ","
      position: 1

  filesList:
    type: File[]
    inputBinding:
      itemSeparator: ","
      position: 2
      
  metricType:
    type: string
    inputBinding:
      position: 3
      
  outDir:
    type: string
    default: $(runtime.outdir)
    inputBinding:
      valueFrom: $(runtime.outdir)
      position: 4

baseCommand: [Rscript, /bin/dnaCohortMerge.R]

outputs:
  mergedTableLatest:
    type: File
    outputBinding:
      glob: "*_latest.tsv"