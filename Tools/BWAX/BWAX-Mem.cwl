#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

$namespaces:
  arv: "http://arvados.org/cwl#"
  cwltool: "http://commonwl.org/cwltool#"

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/bwa:33fc63c"
hints:
  arv:RuntimeConstraints:
    keep_cache: 2048   # [MB]

inputs:
  InputFile:
    type: File[]
#    format: http://edamontology.org/format_1930 # FASTA
    inputBinding:
      position: 201
    
  Index:
    type: File
    inputBinding:
      position: 200
    secondaryFiles:
      - .fai
      - .amb
      - .ann
      - .bwt
      - .pac
      - .sa

  Format:
    type:
      - type: enum
        symbols:
          - "bam"
          - "sam"
  
    inputBinding:
      prefix: "--format"
    
  Output:
    type: string
    default: $("readsout." + inputs.Format)
    inputBinding:
      prefix: "--output"
      valueFrom: $("readsout." + inputs.Format)

#Optional arguments

  Threads:
    type: int?
    inputBinding:
      prefix: "-t"

  MinSeedLen:
    type: int?
    inputBinding:
      prefix: "-k"
  
  BandWidth:
    type: int?
    inputBinding:
      prefix: "-w"

  ZDropoff:
    type: int?
    inputBinding:
      prefix: "-d"

  SeedSplitRatio:
    type: float?
    inputBinding:
      prefix: "-r"
    
  MaxOcc:
    type: int?
    inputBinding:
      prefix: "-c"

  MatchScore:
    type: int?
    inputBinding:
      prefix: "-A"

  MmPenalty:
    type: int?
    inputBinding:
      prefix: "-B"

  GapOpenPen:
    type: int?
    inputBinding:
      prefix: "-O"

  GapExtPen:
    type: int?
    inputBinding:
      prefix: "-E"

  ClipPen:
    type: int?
    inputBinding:
      prefix: "-L"

  UnpairPen:
    type: int?
    inputBinding:
      prefix: "-U"

  RgLine:
    type: string?
    inputBinding:
      prefix: "-R"

  VerboseLevel:
    type: int?
    inputBinding:
      prefix: "-v"

  isOutSecAlign:
    type: boolean?
    inputBinding:
      prefix: "-a"

  isMarkShortSplit:
    type: boolean?
    inputBinding:
      prefix: "-M"

  isUseHardClip:
    type: boolean?
    inputBinding:
      prefix: "-H"

  isMultiplexedPair:
    type: boolean?
    inputBinding:
      prefix: "-p"
      

baseCommand: [bwax, mem]

outputs:
  reads_out:
    type: File
    streamable: true
    outputBinding:
      glob: $("readsout." + inputs.Format)
    
  