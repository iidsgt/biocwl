#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/gdsc:final"

inputs:
  GeneResults:
    type: File
    inputBinding:
      position: 1

  OutputCNV:
    type: File
    inputBinding:
      position: 2

  AnnotatedVariants:
    type: File
    inputBinding:
      position: 3

  GDSCoutfile:
    type: string
    default: "gdsc_out"
    inputBinding:
      position: 4

baseCommand: [python, /bin/classifier_gdsc.py]

outputs:
  IC50s:
    type: File
    outputBinding:
      glob: "*"

