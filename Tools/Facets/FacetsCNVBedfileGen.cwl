#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/facets_cnv:8a5daa6c1ff"
  InlineJavascriptRequirement: {}


inputs:
  ParsedCNVs:
    type: File
    inputBinding:
      position: 1

baseCommand: [Rscript, "/bin/FacetsCNVBedfileGen.R"]

outputs:
  CNV_bed_file:
    type: File
    outputBinding:
      glob: "out.bed"
