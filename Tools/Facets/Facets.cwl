#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "waleedosman/facets_test:firsttry"
  InlineJavascriptRequirement: {}
inputs:
  PairName:
    type: string
    inputBinding:
      position: 1

  PileUp:
    type: File
    inputBinding:
      position: 2
  
  CritValue:
    type: int?
    default: 300
    doc: "cval for procSample https://rdrr.io/github/mskcc/facets/man/procSample.html"
    inputBinding:
      position: 3

  MaxIter:
    type: int?
    doc: "Maximum number of EM iterations in emcncf https://rdrr.io/github/mskcc/facets/man/emcncf.html"
    default: 10
    inputBinding:
      position: 4   
       

baseCommand: [Rscript, "/facets.R"]

outputs:
  genome_segments:
    type: File
    outputBinding:
      glob: $(inputs.PairName + ".genome_segments.pdf")

  diagnostics_plot:
    type: File
    outputBinding:
      glob: $(inputs.PairName + ".diagnostic_plot.pdf")

  cncf:
    type: File
    outputBinding:
      glob: $(inputs.PairName + ".facets_cncf.txt")

  summary:
      type: File
      outputBinding:
        glob: $(inputs.PairName + ".facets_output.txt")

  flags:
      type: File
      outputBinding:
        glob: $(inputs.PairName + ".facets_flags.txt")

  cellularity:
      type: File
      outputBinding:
        glob: "purity.txt"
