#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "quay.io/biocontainers/bedtools:2.27.1--1"
  InlineJavascriptRequirement: {}

inputs:
  wa:
    type: boolean
    default: true
    inputBinding:
      position: 1
      prefix: '-wa'

  wb:
    type: boolean
    default: true
    inputBinding:
      position: 2
      prefix: '-wb'

  CNVBedFile:
    type: File
    inputBinding:
      position: 3
      prefix: '-a'

  noChrBed:
    type: File
    inputBinding:
      position: 4
      prefix: '-b'



baseCommand: [bedtools, intersect]

stdout: BedTools.intersected.bed

outputs:
  output_bed:
    type: stdout
    streamable: true
