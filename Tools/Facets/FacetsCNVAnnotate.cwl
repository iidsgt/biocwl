#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/facets_cnv:8a5daa6c1ff"
  InlineJavascriptRequirement: {}


inputs:
  IntersectedBed:
    type: File
    inputBinding:
      position: 1

  TranscriptIDs:
    type: File
    inputBinding:
      position: 2

  GeneConversionTable:
    type: File
    inputBinding:
      position: 3



baseCommand: [Rscript, "/bin/FacetsCNVAnnotate.R"]

outputs:
  facets_annotated:
    type: File
    outputBinding:
      glob: "output_CNV_file.txt"
