#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "quay.io/biocontainers/msisensor:0.5--hd0b41b6_0"

inputs:
  Microsatellites:
    type: File
    inputBinding:
      prefix: "-d"

  NormalBam:
    type: File
    secondaryFiles: [ ".bai" ]
    inputBinding:
      prefix: "-n"

  TumorBam:
    type: File
    secondaryFiles: [ ".bai" ]
    inputBinding:
      prefix: "-t"

  Output:
    type: string
    default: "msi_output"
    inputBinding:
      prefix: "-o"
      valueFrom: "msi_output"

  #Optional Inputs

  BedFile:
    type: File?
    inputBinding:
      prefix: "-e"

  CovNormalization:
    type: int?
    inputBinding:
      prefix: "-z"


baseCommand: ["msisensor"]

arguments:
  - valueFrom: "msi"
    position: -1

outputs:

  msi_output:
    type: File
    outputBinding:
      glob: msi_output

  msi_output_dis_tab:
    type: File
    doc: "The actual output of this tool is <prefix>_dis, which is different than the documentation which says to look for <prefix>_dis_tab: https://github.com/ding-lab/msisensor/blob/92b603e5167ba8ae3078394aa2429232a5d8dd75/README.md#output"
    outputBinding:
      glob: msi_output_dis

  msi_output_germline:
    type: File
    outputBinding:
      glob: msi_output_germline

  msi_output_somatic:
    type: File
    outputBinding:
      glob: msi_output_somatic
