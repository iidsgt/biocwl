#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/mutationburden:44d74f2"
  InlineJavascriptRequirement: {}

inputs:

  ConsensusVariants:
    type: File
    inputBinding:
      position: 1
  IntervalsBed:
    type: File
    inputBinding:
      position: 2


baseCommand: [Rscript, /bin/Daphni2_Mutation_Burden.R]

outputs:
  mutational_burden:
    type: File
    outputBinding:
      glob: "*burden.txt"

  mutation_type_table:
    type: File
    outputBinding:
      glob: "*type_table.txt"

  tmb_details:
    type: File
    outputBinding:
      glob: "*_details.txt"
