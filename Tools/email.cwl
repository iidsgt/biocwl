#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "conda/c3i-linux-64"
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - entryname: fastpReport.html
        entry: $(inputs.FastpReport)
        writable: true
      - entryname: curl.py
        writable: true
        entry: |-
          import requests
          import datetime
          import time
          time_now = datetime.datetime.now()
          newline = '\n'
          f = open("fastpReport.html").read()
          email_text = "Dear IIDSGT Members,{newline}{newline} Attached you will find the output of the latest run of our daily, automated pipeline. It was triggered at midnight. It completed at {timenow}.{newline}{newline}Best Regards,{newline}{newline}IIDSGT Reporter".format(timenow=time_now, newline=newline)
          def send_simple_message():
            response = requests.post(
                              "https://api.mailgun.net/v3/sandbox2b32107a17bf4fc99dd8f2d49b137671.mailgun.org/messages",
                              auth=("api", "f505e6d15dd32a39d536c08e03a748b8-5645b1f9-660c7918"),
                              data={"from": "IIDGST GROUP at MT SINAI <mailgun@sandbox2b32107a17bf4fc99dd8f2d49b137671.mailgun.org>",
                                    "to": ["ryan.williams@mssm.edu", "evan.clark@mssm.edu", "waleed.osman@mssm.edu", "alessandro.lagana@mssm.edu", "jacob.roberts@mssm.edu"],
                                    "subject": "Pipeline Reporter (Beta)",
                                    "text": email_text
                                    },
                              files=[("attachment", ("fastpReport.html", f))
                              ]
                                    )

            print(response.text)


          send_simple_message()

inputs:

  FastpReport:
    type: File
    inputBinding:
      position: 100

baseCommand: ["python", "curl.py"]

stdout: out

outputs:
  out:
    type: stdout