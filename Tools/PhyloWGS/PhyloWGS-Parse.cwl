#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/facets2phylo:92302ad"

inputs:

  InputFile:
    type: File
    inputBinding:
      position: 100

  CNVFormat:
    type: string
    default: "facets"
    inputBinding:
      prefix: "--cnv-format"

  Cellularity:
    type: File
    inputBinding:
      prefix: "--cellularity"

  Output:
    type: string
    default: "parsed_cnvs.txt"
    inputBinding:
      prefix: "--cnv-output"
      valueFrom: "parsed_cnvs.txt"

baseCommand: [python, /facets2phylo/parser/parse_cnvs.py]

outputs:
  parsed_cnvs:
    type: File
    outputBinding:
      glob: "parsed_cnvs.txt"

