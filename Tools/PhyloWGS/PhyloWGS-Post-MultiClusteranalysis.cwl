#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/phylowgs-post-r:latest3"
  InitialWorkDirRequirement:
    listing:
      - $(inputs.SummJson)
      - $(inputs.MultiTimepointsZip)
      - $(inputs.MultiTimepointsJson)
      - $(inputs.PhyloSSM)
      - $(inputs.PhyloCNV)
      - entryname: run.sh
        entry: |-
          FILE_EXISTS="$((inputs.SummJson) ? 'True':'False')"
          echo "$FILE_EXISTS"
          if [ "$FILE_EXISTS" = "False" ]; then
            echo "Short Circuit"
            return 0;
          fi;
          gunzip *.gz
          unzip -qq *.zip -d ./sample.mutass
          echo "RUNNINGCMD:$@"
          $@


#$@ will expands into RScript Phylo.R summ.json.gz multitimepoints.zip ...
inputs:

  SummJson:
    type: File?
    inputBinding:
      position: 1

  MultiTimepointsZip:
    type: File?
    inputBinding:
      valueFrom: $(self.basename.replace(/\.zip$/, ''))
      position: 2

  MultiTimepointsJson:
    type: File?
    inputBinding:
      position: 3

  PhyloSSM:
    type: File?
    inputBinding:
      position: 4

  PhyloCNV:
    type: File?
    inputBinding:
      position: 5

  CytoBand:
    type: File
    inputBinding:
      position: 6

  SSMOutput:
    type: string
    default: "SSM_clone_table.csv"
    inputBinding:
      position: 7
      valueFrom: "SSM_clone_table.csv"

  CNVOutput:
    type: string
    default: "CNV_clone_table.csv"
    inputBinding:
      position: 8
      valueFrom: "CNV_clone_table.csv"

  ClusterGeneListOutput:
    type: string
    default: "Cluster_Gene_List.csv"
    inputBinding:
      position: 9
      valueFrom: "Cluster_Gene_List.csv"

  OncoFile:
    type: File
    inputBinding:
      position: 10

baseCommand: [sh, run.sh, Rscript, /bin/PhyloWGSMultiClusteranalysis.R]

outputs:
  ssm_output:
    type: ["null", File]
    outputBinding:
      glob: $("SSM_clone_table.csv")

  cnv_output:
    type: ["null", File]
    outputBinding:
      glob: $("CNV_clone_table.csv")

  cluster_gene_list_output:
    type: ["null", File]
    outputBinding:
      glob: $("Cluster_Gene_List.csv")