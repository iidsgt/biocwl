#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/facets2phylo:cc67b8b"
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - entryname: run.sh
        entry: |-
          FILE_EXISTS="$((inputs.ZipFile) ? 'True':'False')"
          echo "$FILE_EXISTS"
          if [ "$FILE_EXISTS" = "False" ]; then
            echo "Short Circuit"
            return 0;
          fi;
          $@

inputs:
  Output:
    type: string
    default: "sample"


  Sample:
    type: string
    inputBinding:
      position: 1

  ZipFile:
    type: File?
    inputBinding:
      position: 2

  TreeSummaryOutput:
    type: string
    default: $(inputs.Output + ".summ.json.gz")
    inputBinding:
      position: 3
      valueFrom: $(inputs.Output + ".summ.json.gz")

  MutlistOutput:
    type: string
    default: $(inputs.Output + ".muts.json.gz")
    inputBinding:
      position: 4
      valueFrom: $(inputs.Output + ".muts.json.gz")

  MutassOutput:
    type: string
    default: $(inputs.Output + ".mutass.zip")
    inputBinding:
      position: 5
      valueFrom: $(inputs.Output + ".mutass.zip")


baseCommand: [sh, run.sh, python, /facets2phylo/write_results.py]

outputs:
  tree_summary_output:
    type: ["null", File]
    outputBinding:
      glob: $(inputs.Output + ".summ.json.gz")

  muts_output:
    type: ["null", File]
    outputBinding:
      glob: $(inputs.Output + ".muts.json.gz")

  mutass_output:
    type: ["null", File]
    outputBinding:
      glob: $(inputs.Output + ".mutass.zip")