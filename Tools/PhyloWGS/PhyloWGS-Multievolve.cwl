#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/facets2phylo:92302ad"
  InitialWorkDirRequirement:
    listing:
      - entryname: run.sh
        entry: |-
          ALL_FILES_EXISTS="$((inputs.SSMs && inputs.CNVs) ? 'True':'False')"
          echo "$ALL_FILES_EXISTS"
          if [ "$ALL_FILES_EXISTS" = "False" ]; then
            echo "Short Circuit"
            return 0;
          fi;
          $@

inputs:

  NumChains:
    type: int
    default: 6
    inputBinding:
      prefix: "-n"

  SSMs:
    type: File?
    inputBinding:
      prefix: "--ssms"

  CNVs:
    type: File?
    inputBinding:
      prefix: "--cnvs"

  Output:
    type: string
    default: "out"
    inputBinding:
      prefix: "--output-dir"
      valueFrom: "out"

baseCommand: [sh, run.sh, python, /facets2phylo/multievolve.py]

outputs:
  trees:
    type: ["null", File]
    outputBinding:
      glob: "out/trees.zip"