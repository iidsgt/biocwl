#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/facets2phylo:92302ad"
  InlineJavascriptRequirement: {}

inputs:

  ParsedCNVs:
    type: File
    default: $("sample1=" + self.path)
    inputBinding:
      prefix: "--cnvs"
      valueFrom: $("sample1=" + self.path)

  MergedVCF:
    type: File
    default: $("sample1=" + self.path)
    inputBinding:
      position: 100
      valueFrom: $("sample1=" + self.path)

  VCFType:
    type: string
    default: "sample1=mutect_smchet"
    inputBinding:
      prefix: "--vcf-type"

  Output:
    type: string
    default: "prepared_cnvs"

  OutputVariants:
    type: string
    default: $(inputs.Output + ".ssm.txt")
    inputBinding:
      prefix: "--output-variants"
      valueFrom: $(inputs.Output + ".ssm.txt")

  OutputCNVs:
    type: string
    default: $(inputs.Output + ".cnv.txt")
    inputBinding:
      prefix: "--output-cnvs"
      valueFrom: $(inputs.Output + ".cnv.txt")



baseCommand: [python, /facets2phylo/parser/create_phylowgs_inputs.py]

outputs:
  prepared_cnvs:
    type: ["null", File]
    outputBinding:
      glob: $(inputs.Output + ".cnv.txt")

  prepared_variants:
    type: ["null", File]
    outputBinding:
      glob: $(inputs.Output + ".ssm.txt")
