#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/psninputgenerator:7cdfd80"

inputs:
  sampleName:
    type: string
    inputBinding:
      position: 1

  rnaRDataFile:
    type: File
    inputBinding:
      position: 2

  parsedCNVs:
    type: File
    inputBinding:
      position: 3

  mmrfReferenceTable:
    type: File
    inputBinding:
      position: 4

  expressionFeatures:
    type: File
    inputBinding:
      position: 5

  cnvFeatures:
    type: File
    inputBinding:
      position: 6

  cytoBand:
    type: File
    inputBinding:
      position: 7
    
  outDir:
    type: string
    default: $(runtime.outdir)
    inputBinding:
      position: 8
      valueFrom: $(runtime.outdir)

baseCommand: [Rscript, /bin/psnInputGenerator.R]

outputs:
  fsqn_normalized_counts:
    type: File
    outputBinding:
      glob: "*.quantile_normalized_counts.csv"
  
  per_band_cnvs:
    type: File
    outputBinding:
      glob: "*.cnv_per_band.csv"

  psn_cnv_inputs:
    type: File
    outputBinding:
      glob: "*cnv.psn_inputs.csv"

  psn_exprs_inputs:
    type: File
    outputBinding:
      glob: "*expression.psn_inputs.csv"

