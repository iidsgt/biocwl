#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool


hints:
    DockerRequirement:
      dockerPull: "quay.io/biocontainers/bowtie2:2.3.4.1--py35h2d50403_1"

inputs:
  ForwardReads:
    type: File
    format: http://edamontology.org/format_1930 # FASTA
    inputBinding:
      prefix: "-1"
  ReverseReads:
    type: File
    format: http://edamontology.org/format_1930 # FASTA
    inputBinding:
      prefix: "-2"
    
  Index:
    type: Directory
    inputBinding:
      prefix: "-x"

  Output:
    type: string
    default: "bowtie2_out.sam"
    inputBinding:
      prefix: "-S"
      valueFrom: "bowtie2_out.sam"


#Optional arguments

  Threads:
    type: int?
    inputBinding:
      prefix: "-t"

outputs:
  Alignment:
    type: File
    outputBinding:
      glob: "*bam"