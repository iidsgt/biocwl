#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/rnacohort:07a6ba1"

inputs:
  
  RData:
    type: File
    inputBinding:
      position: 1

  OutputDir:
    type: string
    default: "output"
    inputBinding:
      position: 2
      valueFrom: "output"

  GeneConvTable:
    type: File
    inputBinding:
      position: 3

baseCommand: [computePathwayActivation.R]

outputs:
  pathway_activation:
    type: File
    outputBinding:
      glob: "output/pathwayActivation_XXX.csv"

  latest_pathway_activation:
    type: File
    outputBinding:
      glob: "output/pathwayActivation_latest.csv"
