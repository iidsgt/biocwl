#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/rnacohort:07a6ba1"

inputs:
  SampleName:
    type: string
    inputBinding:
      position: 1

  ZScoreTable:
    type: File
    inputBinding:
      position: 2

  GeneConvTable:
    type: File
    inputBinding:
      position: 3

  OutputDir:
    type: string
    default: "output"
    inputBinding:
      position: 4
      valueFrom: "output"

  ZscoreThreshold:
    type: float
    default: 1.5
    inputBinding:
      position: 5

baseCommand: [createFilesForLincs.R]

outputs:
  genes_up:
    type: File
    outputBinding:
      glob: "output/UP_top.txt"

  genes_down:
    type: File
    outputBinding:
      glob: "output/DOWN_top.txt"
