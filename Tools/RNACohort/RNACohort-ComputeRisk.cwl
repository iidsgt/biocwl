#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/rnacohort:07a6ba1"

inputs:
  
  RData:
    type: File
    inputBinding:
      position: 1

  OutputDir:
    type: string
    default: "output"
    inputBinding:
      position: 2
      valueFrom: "output"

  GeneConvTable:
    type: File
    inputBinding:
      position: 3

baseCommand: [computeRiskScores.R]

outputs:
  gep_scores:
    type: File
    outputBinding:
      glob: "output/gep70scores_XXX.RData"

  latest_gep_scores:
    type: File
    outputBinding:
      glob: "output/gep70scores_latest.RData"

  latest_gep_scores_tsv:
    type: File
    outputBinding:
      glob: "output/gep70scores_latest.csv"
