#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  ResourceRequirement:
    ramMin: 12000      # [MB]
    coresMin: 4       # [cores]
    tmpdirMin: 10000
  DockerRequirement:
    dockerPull: "sinaiiidgst/rnacohort:7589b97"
  InlineJavascriptRequirement: {}

inputs:
  ReportIds:
    type: string[]
    inputBinding:
      itemSeparator: ","
      position: 1

  Batches:
    type: string[]
    inputBinding:
      itemSeparator: ","
      position: 2

  FeaturecountsFiles:
    type: File[]
    inputBinding:
      itemSeparator: ","
      position: 3

  OutputDir:
    type: string
    default: $(runtime.outdir)
    inputBinding:
      valueFrom: $(runtime.outdir)
      position: 4

  GeneConvTable:
    type: File
    inputBinding:
      position: 5

baseCommand: [Rscript, /bin/mergeRNA_v2.R]

outputs:
  raw_counts:
    type: [File, "null"]
    outputBinding:
      glob: "rawCounts_latest.tsv"

  zscores:
    type: [File, "null"]
    outputBinding:
      glob: "zscores_latest.tsv"

  normCorrCounts:
    type: [File, "null"]
    outputBinding:
      glob: "normCorrCounts_vst_latest.tsv"

  merged_rna_rdata:
    type: [File, "null"]
    outputBinding:
      glob: "RNAdata_latest.RData"
