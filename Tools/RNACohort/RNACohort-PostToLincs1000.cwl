#!/usr/bin/env cwl-runner
cwlVersion: v1.1
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "blairy/requests:latest"
  NetworkAccess:
    networkAccess: true
  InitialWorkDirRequirement:
    listing:
      - entryname: lincs1000.py
        entry: |-
          import requests
          import json
          import argparse
          import csv
          import sys
          from collections import defaultdict
          url = 'http://amp.pharm.mssm.edu/L1000CDS2/query'


          with open('$(inputs.UpGenes.path)','r') as i:
              upGenes = []
              for l in i.readlines():
                g = l.rstrip()
                if g == 'NA' or g == '':
                  continue
                upGenes.append(g)

          with open('$(inputs.DownGenes.path)','r') as j:
              dnGenes = []
              for l in j.readlines():
                g = l.rstrip()
                if g == 'NA' or g == '':
                  continue
                dnGenes.append(g)


          def upperGenes(genes):
              # The app uses uppercase gene symbols. So it is crucial to perform upperGenes() step.
              return [gene.upper() for gene in genes]


          # gene-set search example
          data = {"upGenes":[],"dnGenes":[]}
          data['upGenes'] = upperGenes(upGenes)
          data['dnGenes'] = upperGenes(dnGenes)
          config = {"aggravate":False,"searchMethod":"geneSet","share":True,"combination":False,"db-version":"latest"}
          metadata = []
          payload = {"data":data,"config":config,"meta":metadata}
          headers = {'content-type':'application/json'}
          try:
            r = requests.post(url,data=json.dumps(payload),headers=headers)
            resGeneSet = r.json()
            if 'err' in resGeneSet.keys():
              print("Not enough genes")
              resGeneSet = {}
          except Exception as e:
            print(e)
            print("Not enough genes")
            resGeneSet = {}

          with open('output.json', 'w') as outfile:
              json.dump(resGeneSet, outfile)

inputs:
  UpGenes:
    type: File
  DownGenes:
    type: File
    

outputs:
  drug_targets:
    type: File
    outputBinding:
      glob: "output.json"


baseCommand: [python3.7, lincs1000.py]
