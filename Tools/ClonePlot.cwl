#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/phylowgs-post-r:clonetreeplot713c1e1"
  InlineJavascriptRequirement: {}

inputs:

  SummJson:
    type: File
    inputBinding:
      position: 1

  CloneTreeOut:
    type: string
    default: "clone_tree_plot.png"
    inputBinding:
      position: 2
      valueFrom: "clone_tree_plot.png"

  DNA_VICC:
    type: File
    inputBinding:
      position: 3

  DNA_Civic:
    type: File
    inputBinding:
      position: 4

  CNV_VICC:
    type: File
    inputBinding:
      position: 5

  CNV_civic:
    type: File
    inputBinding:
      position: 6

baseCommand: [Rscript, /bin/Daphni2_Clone_Tree_Plot.R]

outputs:
  clone_tree_plot:
    type:
      - "null"
      - File[]
    outputBinding:
      glob: "*.png"
