#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/l1000cds2postprocessing:5cd8ce4"

inputs:

  L1000json:
    type: File
    inputBinding:
      position: 1

  lincsAnnotRData:
    type: File
    inputBinding:
      position: 2

baseCommand: [Rscript, /bin/Daphni2_L1000CDS2PostProcessing.R]

outputs:
  l1000_drug_list:
    type: File
    outputBinding:
      glob: "L1000_Drug_list.txt"

