#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool


requirements:
  DockerRequirement:
    dockerPull: "quay.io/biocontainers/subread:2.0.1--h5bf99c6_1"
  InlineJavascriptRequirement: {}
inputs:
# REQUIRED ARGS

  InputFile:
    type: File
    inputBinding:
      position: 200
    secondaryFiles:
    - ^.bai

  Output:
    type: string
    default: "Subreadout.fc.txt"
    inputBinding:
      prefix: "-o"
      valueFrom: "Subreadout.fc.txt"

  AnnotationFile:
    type: File
    inputBinding:
      prefix: "-a"

  isPairedEnd:
    type: boolean
    inputBinding:
      prefix: "-p"

  FragmentLength:
    type:
      - "null"
      - type: record
        name: check_fragment_length
        fields:

          isFragLength:
            type: boolean
            inputBinding:
              prefix: "-P"

          minFragLength:
            type: int
            inputBinding:
              prefix: "-d"
          
          maxFragLength:
            type: int
            inputBinding:
              prefix: "-D"

  NThreads:
    type: int
    inputBinding:
      prefix: "-T"

#OPTIONAL ARGS

  chrAliases:
    type: File?
    inputBinding:
      prefix: '-A'

  isBothEndsMapped:
    type: boolean?
    inputBinding:
      prefix: "-B"

  isCountChimFrags:
    type: boolean?
    inputBinding:
      prefix: "-C"

  isMetaFeature:
    type: boolean?
    inputBinding:
      prefix: "-f"

  AnnotationFormat:
    type: 
      - "null"
      - type: enum
        symbols:
          - "GTF"
          - "SAF"
    inputBinding:
      prefix: "-F"

  AttrType:
    type: string?
    inputBinding:
      prefix: "-g"

  Reference:
    type: File?
    inputBinding:
      prefix: "-G"

  JuncCounts:
    type: boolean?
    inputBinding:
      prefix: "-J"

  isLongRead:
    type: boolean?
    inputBinding:
      prefix: "-J"

  CountMultiMap:
    type:
      - "null"
      - type: record
        name: multi_map
        fields:
          isCountMultiMap:
            type: boolean?
            inputBinding:
              prefix: "-M"

          Fraction:
            type: string?
            inputBinding:
              prefix: "--fraction"
    inputBinding:
      prefix: "-M"

  MultiOverlap:
    type:
      - "null"
      - type: record
        name: multi_overlap
        fields:
          isMultiOverlap:
            type: boolean?
            inputBinding:
              prefix: "-O"

          Fraction:
            type: string?
            inputBinding:
              prefix: "--fraction"

  minQualScore:
    type: int?
    inputBinding:
      prefix: "-Q"

  isReportReads:
    type: 
      - "null"
      - type: enum
        symbols:
          - "SAM"
          - "BAM"
          - "CORE"
    inputBinding:
      prefix: "-R"

  Strand:
    type: int?
    inputBinding:
      prefix: "-s"

  FeatureType:
    type: string?
    inputBinding:
      prefix: "-t"

  isCountByreadGrp:
    type: boolean?
    inputBinding:
      prefix: "--byReadGroup"

  isNotSort:
    type: boolean?
    inputBinding:
      prefix: "--donotsort"

  extraAttrib:
    type: string?
    inputBinding:
      prefix: "--extraAttributes"

baseCommand: ["featureCounts"]

outputs:
  featurecounts:
    type: File
    outputBinding:
      glob: "Subreadout.fc.txt"

  reported_reads:
    type:
      - "null"
      - type: array
        items: File
    outputBinding:
      glob:
        - "*.sam"
        - "*.bam"
        - "*.featureCounts"
    

