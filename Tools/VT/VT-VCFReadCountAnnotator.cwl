#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "mgibio/vcf_annotation_tools-cwl:3.0.0"

inputs:
  InputVCF:
    type: File
    inputBinding:
      position: 1
  
  ReadCounts:
    type: File
    inputBinding:
      position: 2

  NucleotideType:
    type:
      - "null"
      - type: enum
        symbols:
          - DNA
          - RNA
    inputBinding:
      position: 3

  Output:
    type: string
    default: "vcf_counted.vcf"
    inputBinding:
      prefix: "-o"
      valueFrom: "vcf_counted.vcf"

# OPTIONAL ARGUMENTS

  SampleName:
    type: string?
    inputBinding:
      prefix: "-s"

  VariantType:
    type:
      - "null"
      - type: enum
        symbols:
          - snv
          - indel
          - all
    inputBinding:
      prefix: "-t"

baseCommand: [vcf-readcount-annotator]

outputs:
  vcf:
    type: File
    outputBinding:
      glob: "vcf_counted.vcf"