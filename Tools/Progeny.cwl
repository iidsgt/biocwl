#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/progeny:bc5ee6b"

inputs:

  RData:
    type: File
    inputBinding:
      position: 1

  GeneConvTable:
    type: File
    inputBinding:
      position: 2

baseCommand: [Daphni2_ProgenyPathways.R]

outputs:
  progenyPathwaylatest:
    type: File[]
    outputBinding:
      glob: "*.csv"
