#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "fedora:latest"
  InlineJavascriptRequirement: {}

inputs:
  Files:
    format: http://edamontology.org/format_1930 # FASTA
    type: File[]
    inputBinding:
      position: 1


baseCommand: [cat]

stdout: joinedfiles.dat.gz

outputs:
  File:
    type: stdout
    format: http://edamontology.org/format_1930 # FASTA