#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/readcountmerge:latest"
  InlineJavascriptRequirement: {}


inputs:
  ReadCounts:
    type: File
    inputBinding:
      position: 1

  ConsensusVCF:
    type: File
    inputBinding:
      position: 2

baseCommand: [Rscript, /bin/MergeVCF-Readcounts.R]

outputs:
  counted_vcf:
    type: File
    outputBinding:
      glob: "counted_merged.vcf"
