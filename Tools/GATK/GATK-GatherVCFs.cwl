#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "broadinstitute/gatk:4.1.1.0"
  InlineJavascriptRequirement: {}

inputs:
  # REQUIRED ARGS

  VariantFiles:
    type:
      type: array
      items: File
      inputBinding:
        prefix: "--INPUT"
    inputBinding:
      position: 2

  Output: 
    type: string
    inputBinding:
      prefix: "--OUTPUT"

  # OPTIONAL ARGS


arguments: 
  - valueFrom: "GatherVcfs"
    position: -1

baseCommand: ["/gatk/gatk"]

outputs:
  merged_vcf:
    type: File
    outputBinding:
      glob: $(inputs.Output)

