#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "broadinstitute/gatk:4.1.1.0"
  InlineJavascriptRequirement: {}

inputs:
  # REQUIRED ARGS

  Output: 
    type: string
    default: $("MergeBamAlignmentOut" + inputs.UnmappedBam.nameext)
    inputBinding:
      prefix: "--OUTPUT"
      valueFrom: $("MergeBamAlignmentOut" + inputs.UnmappedBam.nameext)

  ReferenceSequence: 
    type: File
    format: http://edamontology.org/format_1929  # FASTA 
    inputBinding:
      prefix: "--REFERENCE_SEQUENCE" 

  ReferenceSequenceIndex: 
    type: File
    format: http://edamontology.org/format_1929  # FASTA 

  UnmappedBam: 
    type: File
    format:  # SAM or BAM
     - "http://edamontology.org/format_2573"
     - "http://edamontology.org/format_2572" 
    inputBinding:
      prefix: "--UNMAPPED_BAM" 

  # OPTIONAL ARGS

  AddMateCigar: 
    type: boolean?
    inputBinding:
      prefix: "--ADD_MATE_CIGAR" 

  AddPgTagToReads: 
    type: boolean?
    inputBinding:
      prefix: "--ADD_PG_TAG_TO_READS" 

  AlignedReadsOnly: 
    type: boolean?
    inputBinding:
      prefix: "--ALIGNED_READS_ONLY" 

  AlignerProperPairFlags: 
    type: boolean?
    inputBinding:
      prefix: "--ALIGNER_PROPER_PAIR_FLAGS" 

  ArgumentsFile: 
    type: File[]?
    inputBinding:
      prefix: "--arguments_file" 
      separate: false
      itemSeparator: "," 

  AttributesToRemove: 
    type: string[]?
    inputBinding:
      prefix: "--ATTRIBUTES_TO_REMOVE" 
      separate: false
      itemSeparator: "," 

  AttributesToRetain: 
    type: string[]?
    inputBinding:
      prefix: "--ATTRIBUTES_TO_RETAIN" 
      separate: false
      itemSeparator: "," 

  AttributesToReverse: 
    type: string[]?
    inputBinding:
      prefix: "--ATTRIBUTES_TO_REVERSE" 
      separate: false
      itemSeparator: "," 

  AttributesToReverseComplement: 
    type: string[]?
    inputBinding:
      prefix: "--ATTRIBUTES_TO_REVERSE_COMPLEMENT" 
      separate: false
      itemSeparator: "," 

  ClipAdapters: 
    type: boolean?
    inputBinding:
      prefix: "--CLIP_ADAPTERS" 

  ClipOverlappingReads: 
    type: boolean?
    inputBinding:
      prefix: "--CLIP_OVERLAPPING_READS" 

  CompressionLevel: 
    type: Directory?
    inputBinding:
      prefix: "--COMPRESSION_LEVEL" 

  CreateIndex: 
    type: boolean?
    inputBinding:
      prefix: "--CREATE_INDEX" 

  CreateMd5File: 
    type: boolean?
    inputBinding:
      prefix: "--CREATE_MD5_FILE" 

  ExpectedOrientations: 
    type: string[]?
    inputBinding:
      prefix: "--EXPECTED_ORIENTATIONS" 
      separate: false
      itemSeparator: "," 

  Ga4ghClientSecrets: 
    type: File?
    inputBinding:
      prefix: "--GA4GH_CLIENT_SECRETS" 

  Help: 
    type: boolean?
    inputBinding:
      prefix: "--help" 

  IncludeSecondaryAlignments: 
    type: boolean?
    inputBinding:
      prefix: "--INCLUDE_SECONDARY_ALIGNMENTS" 

  IsBisulfiteSequence: 
    type: boolean?
    inputBinding:
      prefix: "--IS_BISULFITE_SEQUENCE" 

  JavaOptions: 
    type: string?
    inputBinding:
      prefix: "--java-options" 

  MatchingDictionaryTags: 
    type: string[]?
    inputBinding:
      prefix: "--MATCHING_DICTIONARY_TAGS" 
      separate: false
      itemSeparator: "," 

  MaxInsertionsOrDeletions: 
    type: Directory?
    inputBinding:
      prefix: "--MAX_INSERTIONS_OR_DELETIONS" 

  MaxRecordsInRam: 
    type: Directory?
    inputBinding:
      prefix: "--MAX_RECORDS_IN_RAM" 

  MinUnclippedBases: 
    type: Directory?
    inputBinding:
      prefix: "--MIN_UNCLIPPED_BASES" 

  PrimaryAlignmentStrategy: 
    type: 
     - "null"
     - type: enum
       symbols:
        - BestMapq
        - EarliestFragment
        - BestEndMapq
        - MostDistant
    inputBinding:
      prefix: "--PRIMARY_ALIGNMENT_STRATEGY" 

  ProgramGroupCommandLine: 
    type: string?
    inputBinding:
      prefix: "--PROGRAM_GROUP_COMMAND_LINE" 

  ProgramGroupName: 
    type: string?
    inputBinding:
      prefix: "--PROGRAM_GROUP_NAME" 

  ProgramGroupVersion: 
    type: string?
    inputBinding:
      prefix: "--PROGRAM_GROUP_VERSION" 

  ProgramRecordId: 
    type: string?
    inputBinding:
      prefix: "--PROGRAM_RECORD_ID" 

  Quiet: 
    type: boolean?
    inputBinding:
      prefix: "--QUIET" 

  Read1Trim: 
    type: Directory?
    inputBinding:
      prefix: "--READ1_TRIM" 

  Read2Trim: 
    type: Directory?
    inputBinding:
      prefix: "--READ2_TRIM" 

  Reads:
    type:
     - "null"
     - type: record
       name: AlignedBam
       fields:
         AlignedBam: 
           type: File[]?
           inputBinding:
             prefix: "--ALIGNED_BAM" 
             separate: false
             itemSeparator: "," 
     - type: record
       name: Read1AlignedBam
       fields:
         Read1AlignedBam: 
           type: File[]?
           inputBinding:
             prefix: "--READ1_ALIGNED_BAM" 
             separate: false
             itemSeparator: "," 
         Read2AlignedBam: 
           type: File[]?
           inputBinding:
             prefix: "--READ2_ALIGNED_BAM" 
             separate: false
             itemSeparator: "," 

  Showhidden: 
    type: boolean?
    inputBinding:
      prefix: "--showHidden" 

  SortOrder: 
    type: 
     - "null"
     - type: enum
       symbols:
        - unsorted
        - queryname
        - coordinate
        - duplicate
        - unknown
    inputBinding:
      prefix: "--SORT_ORDER" 

  TmpDir: 
    type: Directory[]?
    inputBinding:
      prefix: "--TMP_DIR" 
      separate: false
      itemSeparator: "," 

  UnmapContaminantReads: 
    type: boolean?
    inputBinding:
      prefix: "--UNMAP_CONTAMINANT_READS" 

  UnmappedReadStrategy: 
    type: 
     - "null"
     - type: enum
       symbols:
        - COPY_TO_TAG
        - DO_NOT_CHANGE
        - MOVE_TO_TAG
    inputBinding:
      prefix: "--UNMAPPED_READ_STRATEGY" 

  UseJdkDeflater: 
    type: boolean?
    inputBinding:
      prefix: "--USE_JDK_DEFLATER" 

  UseJdkInflater: 
    type: boolean?
    inputBinding:
      prefix: "--USE_JDK_INFLATER" 

  ValidationStringency: 
    type: 
     - "null"
     - type: enum
       symbols:
        - STRICT
        - LENIENT
        - SILENT
    inputBinding:
      prefix: "--VALIDATION_STRINGENCY" 

  Verbosity: 
    type: 
     - "null"
     - type: enum
       symbols:
        - ERROR
        - WARNING
        - INFO
        - DEBUG
    inputBinding:
      prefix: "--VERBOSITY" 

  Version: 
    type: boolean?
    inputBinding:
      prefix: "--version" 

arguments: 
  - valueFrom: "MergeBamAlignment"
    position: -1

baseCommand: ["/gatk/gatk"]

outputs:
  alignment:
    type: File
    outputBinding:
      glob: $(inputs.Output)
    secondaryFiles:
      - ^.bai

