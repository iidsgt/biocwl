#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "broadinstitute/gatk:4.1.1.0"
  InlineJavascriptRequirement: {}

inputs:
  # REQUIRED ARGS

  InputFile:
    type: File
    inputBinding:
      prefix: "--INPUT"

  Output: 
    type: string
    default: $("SetNmMDOut" + inputs.InputFile.nameext)
    inputBinding:
      prefix: "--OUTPUT" 
      valueFrom: $("SetNmMDOut" + inputs.InputFile.nameext)

  Reference: 
    type: File
    format: "http://edamontology.org/format_1929"
    inputBinding:
      prefix: "--REFERENCE_SEQUENCE" 


  # OPTIONAL ARGS

  ArgumentsFile:
    type: File[]?
    inputBinding:
      prefix: "--arguments_file"

  isBisulfiteSeq:
    type: boolean?
    inputBinding:
      prefix: "--IS_BISULFITE_SEQUENCE"

  isSetUQ:
    type: boolean?
    inputBinding:
      prefix: "--SET_ONLY_UQ"

  CompresionLevel:
    type: int?
    inputBinding:
      prefix: "--COMPRESSION_LEVEL"

  isCreateIndex:
    type: boolean?
    inputBinding:
      prefix: "--CREATE_INDEX"

  isCreateMD5:
    type: boolean?
    inputBinding:
      prefix: "--CREATE_MD5_FILE"

  GH4Secrets:
    type: File?
    inputBinding:
      prefix: "--GA4GH_CLIENT_SECRETS"

  MaxRecordsRam:
    type: int?
    inputBinding:
      prefix: "--MAX_RECORDS_IN_RAM"

  isQuiet:
    type: boolean?
    inputBinding:
      prefix: "--QUIET"
      
  isJDKDeflator:
    type: boolean?
    inputBinding:
      prefix: "--USE_JDK_DEFLATER"

  isJDKInflator:
    type: boolean?
    inputBinding:
      prefix: "--USE_JDK_INFLATER"

  ValidationStringency: 
    type: 
     - "null"
     - type: enum
       symbols:
        - STRICT
        - LENIENT
        - SILENT
    inputBinding:
      prefix: "--VALIDATION_STRINGENCY"

  Verbosity: 
    type: 
     - "null"
     - type: enum
       symbols:
        - ERROR
        - WARNING
        - INFO
        - DEBUG
    inputBinding:
      prefix: "--VERBOSITY"
 
  Version: 
    type: boolean?
    inputBinding:
      prefix: "--version" 

baseCommand: ["/gatk/gatk"]

arguments: 
  - valueFrom: "SetNmMdAndUqTags"
    position: -1


outputs:
  alignment:
    type: File
    outputBinding:
      glob: $("SetNmMDOut" + inputs.InputFile.nameext)
 
  index:
    type: ["null", File]
    outputBinding:
      glob: $("SetNmMDOut" + inputs.InputFile.nameext + ".bai")

