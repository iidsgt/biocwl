#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "broadinstitute/gatk:4.1.1.0"
  InlineJavascriptRequirement: {}

inputs:

  TumorInput:
    type: File
    inputBinding:
      prefix: "-I"

  NormalInput:
    type: File?
    inputBinding:
      prefix: "-I"

  Reference:
    type: File
    inputBinding:
      prefix: "-R"

  NormalName:
    type: string
    default: "normal"
    inputBinding:
      prefix: "-normal"
      valueFrom: "normal"

  Output:
    type: string
    default: "mutect2out.vcf"
    inputBinding:
      prefix: "--output"
      valueFrom: "mutect2out.vcf"

#OPTIONAL ARGS

  dbSNP:
    type: File?
    inputBinding:
      prefix: "--dbsnp"

  Cosmic:
    type: File?
    inputBinding:
      prefix: "--cosmic"

  isDetectArtifacts:
    type: boolean?
    inputBinding:
      prefix: "--artifact_detection_mode"

  IntervalList:
    type: File?
    inputBinding:
      prefix: "-L"
  

arguments:

  - valueFrom: "Mutect2"
    position: -1

baseCommand: ["/gatk/gatk"]

outputs:
  vcf:
    type: File
    outputBinding:
      glob: "mutect2out.vcf"
    secondaryFiles:
      - .stats