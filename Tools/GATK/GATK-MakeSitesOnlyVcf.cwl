#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "broadinstitute/gatk:4.1.3.0"
  InlineJavascriptRequirement: {}

inputs:
  # REQUIRED ARGS

  InputVCF:
    type: File
    inputBinding:
      prefix: "--INPUT"

  OutputVCF:
    type: string
    default: "SitesOnlyVcf.vcf.gz"
    inputBinding:
      prefix: "--OUTPUT"
      valueFrom: "SitesOnlyVcf.vcf.gz"

baseCommand: ["/gatk/gatk"]

arguments:
  - valueFrom: "MakeSitesOnlyVcf"
    position: -1

outputs:
  SitesOnlyVcf:
    type: File
    outputBinding:
      glob: "SitesOnlyVcf.vcf.gz"