#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "broadinstitute/gatk:4.1.1.0"
  InlineJavascriptRequirement: {}

inputs:
  # REQUIRED ARGS

  VariantFile:
    type: File
    inputBinding:
      prefix: "--variant"

  Output: 
    type: string
    default: $("variants_dict" + inputs.VariantFile.nameext)
    inputBinding:
      prefix: "--output" 
      valueFrom: $("variants_dict" + inputs.VariantFile.nameext)

  SourceDict:
    type: File?
    inputBinding:
      prefix: "--source-dictionary"

  Reference:
    type: File?
    inputBinding:
      prefix: "--reference"


  # OPTIONAL ARGS


arguments: 
  - valueFrom: "UpdateVCFSequenceDictionary"
    position: -1

baseCommand: ["/gatk/gatk"]

outputs:
  vcf:
    type: File
    outputBinding:
      glob: $("variants_dict" + inputs.VariantFile.nameext)

