#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "broadinstitute/gatk:4.1.3.0"
  InlineJavascriptRequirement: {}

inputs:
  # REQUIRED ARGS

  InputFile:
    type: File
    inputBinding:
      prefix: "-I"
    secondaryFiles:
      - .bai

  Reference:
    type: File
    inputBinding:
      prefix: "-R"
    secondaryFiles:
      - ^.dict
      - .fai

  Output:
    type: string
    default: "raw.snps.indels.vcf"
    inputBinding:
      prefix: "--O"
      valueFrom: "raw.snps.indels.vcf"

  # OPTIONAL ARGS
  Alleles:
    type: File?
    inputBinding:
      prefix: "--alleles"

  Intervals:
    type: File?
    inputBinding:
      prefix: "--intervals"

  Dbsnp:
    type: File?
    inputBinding:
      prefix: "-D"

  emitRefConfidence:
    type: string?
#    default: "GVCF"
    inputBinding:
      prefix: "--emit-ref-confidence"

baseCommand: ["/gatk/gatk"]

arguments:
  - valueFrom: "HaplotypeCaller"
    position: -1

outputs:
  filteredVCF:
    type: File
    outputBinding:
      glob: "raw.snps.indels.vcf"
