#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "broadinstitute/gatk:4.1.3.0"
  InlineJavascriptRequirement: {}
  EnvVarRequirement:
    envDef:
      TILEDB_DISABLE_FILE_LOCKING: '1'

inputs:
  # REQUIRED ARGS

  Reference:
    type: File
    inputBinding:
      prefix: "--reference"
    secondaryFiles:
      - ^.dict
      - .fai

  Variant:
    type:
      - File
      - Directory
    inputBinding:
      prefix: "--variant"
      valueFrom: |
        ${
          if (self.class=="File")
                return self.path;
          else
            return "gendb://"+self.path;
        }

  Output:
    type: string
    default: "output.vcf.gz"
    inputBinding:
      prefix: "--output"

  # OPTIONAL ARGS

  Intervals:
    type: string?
    inputBinding:
      prefix: "--intervals"

  JavaOptions:
    type: string?
    inputBinding:
      prefix: "--java-options"



baseCommand: ["/gatk/gatk"]

arguments:
  - valueFrom: "GenotypeGVCFs"
    position: -1

outputs:
  genotyped_vcf:
    type: File
    outputBinding:
      glob: "output.vcf.gz"
