#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

# Modified from: https://github.com/NCI-GDC/gdc-dnaseq-cwl/blob/master/tools/gatk4_collectmultiplemetrics.cwl
requirements:
  DockerRequirement:
    dockerPull: "broadinstitute/gatk:4.1.3.0"
  InlineJavascriptRequirement: {}

baseCommand: ["/gatk/gatk"]

arguments:
  - valueFrom: "CollectMultipleMetrics"
    position: -1
  - valueFrom: $(inputs.INPUT.nameroot + ".multiple_metrics")
    prefix: --OUTPUT

inputs:

  INPUT:
    type: File
    inputBinding:
      prefix: --INPUT

  REFERENCE_SEQUENCE:
    type: File
    inputBinding:
      prefix: --REFERENCE_SEQUENCE

outputs:
  multiMetrics:
    type: File[]
    outputBinding:
      glob: "*"