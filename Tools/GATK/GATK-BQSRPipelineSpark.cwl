#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "broadinstitute/gatk:4.1.1.0"
  InlineJavascriptRequirement: {}

inputs:
  # REQUIRED ARGS

  InputFile:
    type: 
      - File[]
      - File
    inputBinding:
      prefix: "--input"

  Output: 
    type: string
    default: "BQSROut.bam"
    inputBinding:
      prefix: "--output" 
      valueFrom: "BQSROut.bam"

  KnownSites: 
    type: File[]
    inputBinding:
      prefix: "--known-sites" 
      itemSeparator: ","

  Reference:
    type: File?
    inputBinding:
      prefix: "--reference"

  # OPTIONAL ARGS

  ArgumentsFile:
    type: File[]?
    inputBinding:
      prefix: "--arguments_file"

  BamPartitionSize:
    type: int?
    inputBinding:
      prefix: "--bam-partition-size"

  BinaryTagName:
    type: string?
    inputBinding:
      prefix: "--binary-tag-name"

  BQSRBaqGapOpen:
    type: double?
    inputBinding:
      prefix: "--bqsr-baq-gap-open-penalty"

  SparkConf:
    type: string[]?
    inputBinding:
      prefix: "--conf"
      itemSeparator: ","

  DefaultBaseQualities:
    type: int?
    inputBinding:
      prefix: "--default-base-qualities"

  DefaultDeletionsQualities:
    type: int?
    inputBinding:
      prefix: "--deletions-default-quality"

  isDisableSeqDictValidation:
    type: boolean?
    inputBinding:
      prefix: "--disable-sequence-dictionary-validation"

  isEmitOriginalQuals:
    type: boolean?
    inputBinding:
      prefix: "--emit-original-quals"

  GcsMaxRetries:
    type: int?
    inputBinding:
      prefix: "--gcs-max-retries"

  GcsProjectRequestorPays:
    type: string?
    inputBinding:
      prefix: "--gcs-project-for-requester-pays"
  
  GlobalQscorePrior:
    type: double?
    inputBinding:
      prefix: "--global-qscore-prior"

  IndelsContextSize:
    type: int?
    inputBinding:
      prefix: "--indels-context-size"

  InsertionsDefaultQuality:
    type: int?
    inputBinding:
      prefix: "--insertions-default-quality"

  IntervalMergingRule:
    type: 
      - "null"
      - type: enum
        symbols:
          - ALL
          - OVERLAPPING_ONLY
    inputBinding:
      prefix: "--interval-merging-rule"

  Intervals:
    type: string[]?
    inputBinding:
      prefix: "--intervals"
      itemSeparator: ","

  JavaOptions: 
    type: string?
    inputBinding:
      prefix: "--java-options" 

  LowQualityTail:
    type: int?
    inputBinding:
      prefix: "--low-quality-tail"

  MaximumCycleValue:
    type: int?
    inputBinding:
      prefix: "--maximum-cycle-value"

  MismatchesContextSize:
    type: int?
    inputBinding:
      prefix: '--mismatches-context-size'

  MismatchesDefaultQuality:
    type: int?
    inputBinding:
      prefix: "--mismatches-default-quality"

  NumReducers:
    type: int?
    inputBinding:
      prefix: "--num-reducers"

  OutputShardTmpDir:
    type: Directory?
    inputBinding:
      prefix: "--output-shard-tmp-dir"

  PreserveQscoresLessThan:
    type: int?
    inputBinding:
      prefix: "--preserve-qscores-less-than"

  ProgramName:
    type: string?
    inputBinding:
      prefix: "--program-name"

  QuantizeQuals:
    type:
      - "null"
      - type: record
        name: QuantizeQuals
        fields:
          QuantizeQuals:
              type: int
              inputBinding:
                prefix: "--quantize-quals"
      - type: record
        name: RoundDownQuantize
        fields:
          RoundDownQuantize:
            type: boolean?
            inputBinding:
              prefix: "--round-down-quantized"

          StaticQuantizedQuals:
            type: boolean?
            inputBinding:
              prefix: "--static-quantized-quals"

  isShardedOutput:
    type: boolean?
    inputBinding:
      prefix: "--sharded-output"

  SparkMaster:
    type: string?
    inputBinding:
      prefix: "--spark-master"

  SparkVerbosity:
    type: 
      - "null"
      - type: enum
        symbols:
          - ALL
          - DEBUG
          - INFO
          - WARN
          - ERROR
          - FATAL
          - OFF
          - TRACE
    inputBinding:
      prefix: "--spark-verbosity"

  isUseOriginalQuals:
    type: boolean?
    inputBinding:
      prefix: "--use-original-qualities"

  ReadFilter:
    type: string[]?
    inputBinding:
      prefix: "--read-filter"
      itemSeparator: ","

  ReadIndex:
    type: string[]?
    inputBinding:
      prefix: "--read-index"
      itemSeparator: ","

  ExcludeIntervals:
    type: 
      - "null"
      - type: array
        items: string
        inputBinding:
          prefix: "--exclude-intervals"
      - File
    inputBinding:
      prefix: "--exclude-intervals"

  isOutputVCFCommandLine:
    type: boolean?
    inputBinding:
      prefix: "--add-output-vcf-command-line"

  isCreateBamIndex:
    type: boolean?
    inputBinding:
      prefix: "--create-output-bam-index"

  isCreateBamSplittingIndex:
    type: boolean?
    inputBinding:
      prefix: "--create-output-bam-splitting-index"

  isCreateOutputVariantIndex:
    type: boolean?
    inputBinding:
      prefix: "--create-output-variant-index"

  DisableReadFilter:
    type: string[]?
    inputBinding:
      prefix: "--disable-read-filter"

  isDisableDefaultReadFilters:
    type: boolean?
    inputBinding:
      prefix: "--disable-tool-default-read-filters"

  GatkConfigFile:
    type: File?
    inputBinding:
      prefix: "--gatk-config-file"

  IntervalExclPadding:
    type: int?
    inputBinding:
      prefix: "--interval-exclusion-padding"

  IntervalPadding:
    type: int?
    inputBinding:
      prefix: "--interval-padding"

  IntervalSetRule:
    type:
      - "null"
      - type: enum
        symbols:
          - UNION
          - INTERSECTION
    inputBinding:
      prefix: "--interval-set-rule"

  isQuiet:
    type: boolean?
    inputBinding:
      prefix: "--QUIET"

  ReadValidationStringency:
    type: 
      - "null"
      - type: enum
        symbols:
          - STRICT
          - LENIENT
          - SILENT
    inputBinding:
      prefix: "--read-validation-stringency" 

  isJDKDeflator:
    type: boolean?
    inputBinding:
      prefix: "--USE_JDK_DEFLATER"  
  
  isJDKInflator:
    type: boolean?
    inputBinding:
      prefix: "--USE_JDK_INFLATER"

  Verbosity: 
    type: 
     - "null"
     - type: enum
       symbols:
        - ERROR
        - WARNING
        - INFO
        - DEBUG
    inputBinding:
      prefix: "--VERBOSITY"

baseCommand: ["/gatk/gatk"]

arguments: 
  - valueFrom: "BQSRPipelineSpark"
    position: -1

outputs:
  index:
    type: File
    outputBinding:
      glob: $("BQSROut.bam")
    secondaryFiles:
      - ^.bai
      - ^.vcf
      - ^.tbi