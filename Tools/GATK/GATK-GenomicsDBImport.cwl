#!/usr/bin/env cwl-runner
cwlVersion: v1.1
class: CommandLineTool

$namespaces:
  arv: "http://arvados.org/cwl#"
  cwltool: "http://commonwl.org/cwltool#"

hints:
  arv:APIRequirement: {}

requirements:
  DockerRequirement:
    dockerPull: "broadinstitute/gatk:4.1.7.0"
  InlineJavascriptRequirement: {}
  NetworkAccess:
    networkAccess: true
inputs:
  # REQUIRED ARGS

  GenomicsdbWorkspacePath:
    type: string
    inputBinding:
      prefix: "--genomicsdb-workspace-path"

  Intervals:
    type:
      - File
      - string
    inputBinding:
      prefix: "--intervals"

  VariantFiles:
    type:
      type: array
      items: File
      inputBinding:
        prefix: "-V"
    secondaryFiles:
      - .tsv

  SampleNameMap:
    type: string?
    inputBinding:
      prefix: "--sample-name-map"

  MergeInputIntervals:
    type: boolean?
    inputBinding:
      prefix: "--merge-input-intervals"

  ReaderThreads:
    type: int?
    inputBinding:
      prefix: "--reader-threads"

baseCommand: ["/gatk/gatk"]

arguments:
  - valueFrom: "GenomicsDBImport"
    position: -1

outputs:
  genomics_db_workspace:
    type: Directory
    outputBinding:
      glob: $(inputs.GenomicsdbWorkspacePath)