#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
- class: DockerRequirement
  dockerPull: broadinstitute/gatk:4.0.4.0
- class: InlineJavascriptRequirement
hints:
- class: SoftwareRequirement
  packages:
      gatk:
          version: [ "4.0.4.0" ]
          https://schema.org/citation: https://doi.org/10.1101/gr.107524.110
baseCommand: gatk

inputs:
  Variant:
    type: File
    inputBinding:
      prefix: -V
      position: 1
    secondaryFiles:
      - .idx
    doc: One or more VCF files containing variants  Required.
  output_recalibration_filename:
    type: string
    default: "output.recal"
    inputBinding:
      prefix: -O
      position: 2
    doc: The output recal file used by ApplyRecalibration  Required.
  TranchesFile:
    type: string
    default: "tranches_out.tranches"
    inputBinding:
      prefix: --tranches-file
      position: 3
    doc:  The output tranches file used by ApplyRecalibration  Required.
  trust_all_polymorphic:
    type: boolean?
    inputBinding:
      prefix: --trust-all-polymorphic
      position: 4
    doc: "Trust that all the input training sets' unfiltered records contain only polymorphic sites to drastically speed up the computation.  Default value: false."
  tranche:
    type:
      type: array
      items: string
      inputBinding:
        prefix: -tranche
    inputBinding:
      position: 5
  useAnnotation:
    type:
      type: array
      items: string
      inputBinding:
        prefix: -an
    inputBinding:
      position: 6
    doc: The names of the annotations which should used for calculations
  Mode:
    type:
      type: enum
      symbols: [SNP,INDEL,BOTH]
    inputBinding:
      prefix: --mode
      position: 7
    doc: "Recalibration mode to employ  Default value: SNP. Possible values: {SNP, INDEL, BOTH}"
  MaxGaussians:
    type: int?
    inputBinding:
      prefix: --max-gaussians
      position: 8
    doc: "Max number of Gaussians for the positive model  Default value: 8."
  Resources: # This is a complex record type that cannot be represented into command-line args, so we mark input but produce binding as an expression
    inputBinding:
      position: 9
    type:
      type: array
      items:
        type: record
        name: resource
        fields:
          name: { type: string }
          known: { type: boolean }
          training: { type: boolean }
          truth: { type: boolean }
          prior: { type: int }
          file: { type: File }
      inputBinding:
        prefix: '-resource'
        valueFrom: >
          ${
            function makeResourceLine(resource) {
              return resource.name +
              ',known=' + (resource.known ? 'true' : 'false') +
              ',training=' + (resource.training ? 'true' : 'false') +
              ',truth=' + (resource.truth ? 'true' : 'false') +
              ',prior=' + resource.prior +
              ':' + resource.file.path;
            }
            return makeResourceLine(self);
          }
#  Intervals:
#    type: File
#    inputBinding:
#      prefix: --intervals
#      position: 10
#    doc: One or more genomic intervals over which to operate
#  AlleleSpecific:
#    type: boolean
#    default: true
#    inputBinding:
#      prefix: "--use-allele-specific-annotations"
outputs:
  recalFile:
    type: File
    outputBinding:
      glob: "output.recal"
  output_tranches:
    type: File
    outputBinding:
      glob: "tranches_out.tranches"

arguments:
- valueFrom: VariantRecalibrator
  position: 0