#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "broadinstitute/gatk:4.1.1.0"
  InlineJavascriptRequirement: {}

inputs:
  # REQUIRED ARGS

  InputFile: 
    type: File
    format:  # SAM or BAM
     - "http://edamontology.org/format_2573"
     - "http://edamontology.org/format_2572" 
    inputBinding:
      prefix: "--INPUT" 

  Output:
    type: string
    default: $("SortSamOut" + inputs.InputFile.nameext)
    inputBinding:
      prefix: "--OUTPUT"
      valueFrom: $("SortSamOut" + inputs.InputFile.nameext)

  SortOrder: 
    type: 
     - "null"
     - type: enum
       symbols:
        - queryname
        - coordinate
        - duplicate
    inputBinding:
      prefix: "--SORT_ORDER" 

  # OPTIONAL ARGS

  ArgumentsFile: 
    type: File[]?
    inputBinding:
      prefix: "--arguments_file" 
      separate: false
      itemSeparator: "," 

  CompressionLevel: 
    type: int?
    inputBinding:
      prefix: "--COMPRESSION_LEVEL" 

  CreateIndex: 
    type: boolean?
    inputBinding:
      prefix: "--CREATE_INDEX" 

  CreateMd5File: 
    type: boolean?
    inputBinding:
      prefix: "--CREATE_MD5_FILE" 

  Ga4ghClientSecrets: 
    type: File?
    inputBinding:
      prefix: "--GA4GH_CLIENT_SECRETS" 

  Help: 
    type: boolean?
    inputBinding:
      prefix: "--help" 

  JavaOptions: 
    type: string?
    inputBinding:
      prefix: "--java-options" 

  MaxRecordsInRam: 
    type: int?
    inputBinding:
      prefix: "--MAX_RECORDS_IN_RAM" 

  Quiet: 
    type: boolean?
    inputBinding:
      prefix: "--QUIET" 

  ReferenceSequence: 
    type: File?
    format: http://edamontology.org/format_1929  # FASTA 
    inputBinding:
      prefix: "--REFERENCE_SEQUENCE" 

  Showhidden: 
    type: boolean?
    inputBinding:
      prefix: "--showHidden" 

  TmpDir: 
    type: Directory[]?
    inputBinding:
      prefix: "--TMP_DIR" 
      separate: false
      itemSeparator: "," 

  UseJdkDeflater: 
    type: boolean?
    inputBinding:
      prefix: "--USE_JDK_DEFLATER" 

  UseJdkInflater: 
    type: boolean?
    inputBinding:
      prefix: "--USE_JDK_INFLATER" 

  ValidationStringency: 
    type: 
     - "null"
     - type: enum
       symbols:
        - STRICT
        - LENIENT
        - SILENT
    inputBinding:
      prefix: "--VALIDATION_STRINGENCY" 

  Verbosity: 
    type: 
     - "null"
     - type: enum
       symbols:
        - ERROR
        - WARNING
        - INFO
        - DEBUG
    inputBinding:
      prefix: "--VERBOSITY" 

  Version: 
    type: boolean?
    inputBinding:
      prefix: "--version" 

baseCommand: ["/gatk/gatk", ""]

arguments: 
  - valueFrom: "SortSam"
    position: -1


outputs:
  index:
    type: File
    outputBinding:
      glob: $("SortSamOut" + inputs.InputFile.nameext)

