#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "broadinstitute/gatk:4.1.1.0"
  InlineJavascriptRequirement: {}
  ShellCommandRequirement: {}

inputs:
  # REQUIRED ARGS

  Variant:
    type: File
    inputBinding:
      prefix: "-V"

  Output:
    type: string
    default: "ApplyVQSR.vcf.gz"
    inputBinding:
      prefix: "-O"
      valueFrom: "ApplyVQSR.vcf.gz"

  recalFile:
    type: File
    inputBinding:
      prefix: "--recal-file"

  TranchesFile:
    type: File?
    inputBinding:
      prefix: "--tranches-file"

  TruthSensitivity:
    type: float
    inputBinding:
      prefix: "--truth-sensitivity-filter-level"

  CreateIndex:
    type: boolean
    default: true
    inputBinding:
      prefix: "--create-output-variant-index"

#  AlleleSpecific:
#    type: boolean
#    default: true
#    inputBinding:
#      prefix: "--use-allele-specific-annotations"

  Mode:
    type:
      - type: enum
        symbols:
          - SNP
          - INDEL
          - BOTH
    inputBinding:
      prefix: "--mode"

baseCommand: ["/gatk/gatk"]

arguments: 
  - valueFrom: "ApplyVQSR"
    position: -1

outputs:
  ApplyVQSRVCF:
    type: File
    outputBinding:
      glob: "ApplyVQSR.vcf.gz"

