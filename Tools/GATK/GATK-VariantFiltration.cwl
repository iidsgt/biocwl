#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "broadinstitute/gatk:4.1.3.0"
  InlineJavascriptRequirement: {}

inputs:
  # REQUIRED ARGS

  Variant:
    type: File
    inputBinding:
      prefix: "-V"

  Output:
    type: string
    default: "filtered.vcf.gz"
    inputBinding:
      prefix: "-O"
      valueFrom: "filtered.vcf.gz"

  # OPTIONAL ARGS

  Reference:
    type: File?
    inputBinding:
      prefix: "-R"
    secondaryFiles:
      - ^.dict
      - .fai

  filterExpression:
    type: string?
    inputBinding:
      prefix: "--filter-expression"

  filterName:
    type: string?
    inputBinding:
      prefix: "--filter-name"


baseCommand: ["/gatk/gatk"]

arguments:
  - valueFrom: "VariantFiltration"
    position: -1

outputs:
  filteredVCF:
    type: File
    outputBinding:
      glob: "filtered.vcf.gz"
