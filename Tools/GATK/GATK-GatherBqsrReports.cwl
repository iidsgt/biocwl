#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "broadinstitute/gatk:4.1.1.0"
  InlineJavascriptRequirement: {}

inputs:
  # REQUIRED ARGS

  InputFile:
    type: File
    inputBinding:
      prefix: "-I"

  Output:
    type: string
    default: "output.grp"
    inputBinding:
      prefix: "--out"
      valueFrom: "output.grp"

  # OPTIONAL ARGS

  JavaOptions: 
    type: string?
    inputBinding:
      prefix: "--java-options" 

arguments: 
  - valueFrom: "GatherBQSRReports"
    position: -1

baseCommand: ["/gatk/gatk", ""]

outputs:
  table:
    type: File
    outputBinding:
      glob: $(inputs.Output)
    