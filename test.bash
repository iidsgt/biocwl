#!/bin/bash
set -e
for f in $(find ./ -name '*.cwl')
do
    cwltool --validate $f
done

