#!/usr/bin/env python
#
# Given an input CWL, traverse any subworkflows it calls, replacing every relative path to another CWL with a
# git-SHA-qualified permalink to the current version of that CWL.
#
# When there is a tree of CWLs (with more than one level of nesting / sub-workflow'ing), this requires:
# - committing the "leaves" of the tree
# - updating any CWLs that call them to use the git SHA of that commit
# - committing the "next level of leaves" (nodes that would be leaves if all leaves of the original tree were removed)
# - etc. until the root is reached, given permalinks to its children, committed with those links, and permalinked at
#   that state
#
# When all this is done, the whole tree (with potentially multiple commits that build it) is pushed to an upstream git
# repository (that the permalinks presumably point at). It is pushed to a given branch that is intended to be
# "clobbered" repeatedly with snapshots of CWLs.
#
# Pushing to that branch has some subtleties:
# - we don't care about the previous state of the branch
#   - the branch ("cwl-snapshots") is intended to have these "snapshots" layered onto it
#   - they're serialized into a git lineagethat lineage doesn't strictly model their logical/dataflow lineage:
#     - one side of their git lineage points at the upstream commits they were based on
#     - the other side just indicates a chronological creation order of "snapshots"
# - so what we want is *almost* like a force-push:
#   - we don't want to lose history or orphan previous "CWL snapshot" commits
#   - we want a "merge" but where we don't pay attention to the tree from one side of the merge (the previous CWL s
#     napshot, which is in general unrelated to the snapshot we're currently snapshotting)
#   - so, we create a fake "merge" commit:
#     - it takes its tree from our "CWL snapshot" commit
#     - its parents are:
#       - the same "CWL snapshot" commit that it gets its tree from
#       - the top commit of the CWL-snapshot commit-tree
#
# The CI/CD pushes to a `cwl-releases` branch, while manual runs from the CLI will push to a branch called
# "$USER-cwl-snapshots".


from contextlib import nullcontext
from os import devnull, environ as env
from pathlib import Path
from re import match
from ruamel.yaml import YAML
from shutil import copy
from sys import stdout
from tempfile import TemporaryDirectory

from utils.cd import cd
from utils.process import lines, output as _output, run, success

from tree import Tree


def git_modified_files(ignore_submodules=True):
    ignore_submodules = [ '--ignore-submodules' ] if ignore_submodules else []
    modified_files = [
        match('(?P<mode>..)\s(?P<path>.*)', line).groupdict()
        for line in lines(
            [ 'git', 'status', '--porcelain', ] + ignore_submodules
        ) \
        if line
    ]
    return [
        dict(
            mode=m['mode'],
            path=Path(m['path'])
        )
        for m in modified_files
    ]


def main(
    input,
    output,
    remote=None,
    upstream=None,
    push_options=None,
    branch=None,
    cwl_permalink_prefix=None,
    current_dir=False,
    keep_tmpdir=False,
    open_browser=False,
    copy_to_clipboard=False,
):
    '''Snapshot / Permalink a CWL file (and sub-workflows it points to).

    :param input: path to input CWL to snapshot
    :param output: path to write snapshotted CWL to ("-" for stdout, None for /dev/null)
    :param remote: name of git "remote" to push to (default: upstream; mostly useful when `current_dir` is set and a
            specific existing git remote should be used)
    :param upstream: git remote URL to push to (if a remote is being added, e.g. when work is performed in a temporary
            git clone; see `current_dir`)
    :param push_options: additional options to pass as "-o" flags to `git push`
    :param branch: git branch to push to. By default, a branch of the form `$USER-cwl-snapshots` is constructed. CI/CD
            pushes to `cwl-releases`
    :param cwl_permalink_prefix: base of the CWL permalink URL to be output (default: https://gitlab.com/iidsgt/biocwl/raw )
    :param current_dir: when set, perform snapshotting commits in the current working directory / repo (as opposed to
            cloning the current repository into a temporary directory, and making commits and pushing from there)
    :param keep_tmpdir: when set, and a temporary git clone is created, don't delete it at the end of the script's
            execution (e.g. for debugging purposes).
    :param open_browser: when set, attempt to open the snapshotted CWL in a web browser (using the "open" command)
    :param copy_to_clipboard: when set, copy the snapshotted CWL permalink to the clipboard (using "pbcopy")
    '''
    if remote:
        upstream = _output([ 'git', 'remote', 'get-url', remote, ])
    elif not upstream:
        upstream = 'https://gitlab.com/iidsgt/biocwl.git'

    remote = remote or 'upstream'

    add_remote = not (current_dir and success('git', 'remote', 'get-url', remote))

    if not push_options: push_options = []

    if not branch:
        if 'USER' in env:
            user = env['USER']
        else:
            user = _output([ 'git', 'config', 'user.name', ]).strip().split(' ')[0]
        branch = f'{user}-cwl-snapshots'
        print(f'Pushing to user-snapshot branch: {branch}')

    if not cwl_permalink_prefix: cwl_permalink_prefix = 'https://gitlab.com/iidsgt/biocwl/raw'

    www_url_prefix = 'https://gitlab.com/iidsgt/biocwl'

    write_output_file = False
    if output == '-':
        out = stdout
        out_ctx = nullcontext()
    elif not output:
        out = open(devnull, 'w')
        out_ctx = out
    else:
        out = Path(output).open('w')
        out_ctx = out
        write_output_file = True

    yaml = YAML()
    yaml.preserve_quotes = True

    root = Path(__file__).parent.absolute().resolve()

    input = Path(input).absolute().resolve().relative_to(root)

    print(f'Snapshotting {input}')

    if current_dir:
        dir = root
        dir_ctx = nullcontext()
        cd_ctx = nullcontext()
    else:
        dir = TemporaryDirectory()
        if keep_tmpdir:
            dir_ctx = nullcontext()
        else:
            dir_ctx = dir
        dir = Path(dir.name).absolute().resolve()
        cd_ctx = cd(dir)

    with dir_ctx:
        if not current_dir:
            print(f'Cloning: {root} → {dir}')
            run([ 'git', 'clone', root, dir ])

        def read(path):
            with path.open('r') as f:
                return yaml.load(f)

        def children(data):
            steps = data['cwl'].get('steps', {})
            parent = data['path'].parent
            def child_path(step):
                with cd(parent):
                    raw_path = step['run']
                    from urllib.parse import urlparse
                    url = urlparse(raw_path)
                    if url.scheme:
                        if url.scheme != 'https' or url.hostname != 'gitlab.com' or not match(r'^/iidsgt/biocwl/raw/[a-f0-9]{40}/.*', url.path):
                            raise ValueError(f'Unrecognized URL to child step: {raw_path}')

                        # No-op on URLs that already point to GitLab permalinked/snapshotted CWLs
                        return None
                    else:
                        child_path = Path(raw_path).absolute().resolve().relative_to(root)
                        print(f'Resolving child path {raw_path} ({Path(raw_path).absolute()}) from {input.parent}: {child_path}')
                    return child_path

            raw_children = {
                step_name: child_path(step)
                for step_name, step in steps.items()
            }

            return { k:v for k,v in raw_children.items() if v }

        tree = \
            Tree(
                input,
                data_fn=lambda path: dict(path=path, cwl=read(path)),
                children_fn=children,
            )

        def commit():
            run([ 'git', 'add', '.', ])
            modified_files = git_modified_files()
            if modified_files:
                paths = [ str(m['path']) for m in modified_files ]
                msg = 'Snapshotting CWLs:\n\n\t%s' % '\n\t'.join(paths)
                run([ 'git', 'commit', '-m', msg, ])
            else:
                print(f'Nothing to commit; using existing HEAD')

            prev_commit = _output([ 'git', 'log', '-n', '1', '--format=%H', 'HEAD' ]).strip()
            return prev_commit

        original_commit = _output([ 'git', 'log', '-n', '1', '--format=%H', 'HEAD', ]).strip()

        with cd_ctx:
            if add_remote:
                run([ 'git', 'remote', 'add', remote, upstream, ])

            if not current_dir:
                print(f'Resetting to upstream commit: {original_commit}')
                run([ 'git', 'reset', '--hard', original_commit, ])

            def copy_cwl(d):
                dst = d['path']
                src = root / dst
                print(f'Copying leaf CWL: {src} -> {dst}')
                dst.parent.mkdir(parents=True, exist_ok=True)
                copy(src, dst)

            if not current_dir:
                tree.foreach(copy_cwl)

            root_path = None
            num_processed = 0
            while True:
                prev_commit = commit()
                commit_permalink_prefix = f'{cwl_permalink_prefix}/{prev_commit}'
                print(f'HEAD commit: {prev_commit}, prefix {commit_permalink_prefix}')

                def mark_leaves(n):
                    children = n.child_nodes
                    if not children or all([ child.data.get('processed') for child in children ]):
                        return { **n.data, 'leaf': True }
                    else:
                        return n.data

                tree = tree.map(mark_leaves, nodes=True)

                leaves = tree.reduce(lambda d: [ d ] if d.get('leaf') and not d.get('processed') else [])
                num_leaves = len(leaves)
                if not leaves: break

                num_unprocessed = len(tree) - num_processed - num_leaves
                print(f'{num_leaves} leaves, {num_processed} processed, {num_unprocessed} unprocessed, prev commit {prev_commit}, prev root path {root_path}')

                # Update leaves' parents to point to explicitly-SHA'd URLs to the leaves
                leaf_paths = set([ str(leaf['path']) for leaf in leaves ])
                print(f'Looking for {len(leaf_paths)} leaf paths:\n\t%s' % '\n\t'.join(leaf_paths))
                def resolve_leaf_paths(node):
                    d = node.data.copy()
                    path = d['path']
                    cwl = d['cwl']
                    steps = cwl.get('steps', {})
                    children = node.children
                    child_paths = [ str(child.data['path']) for child in children.values() ]
                    print(f'{path}: checking for {child_paths}')
                    updated = False
                    for step_name, child in children.items():
                        child_path = str(child.data['path'])
                        print(f'{path}: checking child {child_path}')
                        if child_path in leaf_paths:
                            step = steps[step_name]
                            run = step['run']
                            cwl_permalink = f'{commit_permalink_prefix}/{child_path}'
                            print(f'{path}:{step_name}: updating {run} to {cwl_permalink}')
                            step['run'] = cwl_permalink
                            updated = True

                    if updated:
                        print(f'Writing new CWL to {path}')
                        with path.open('w') as f:
                            yaml.dump(cwl, f)
                    return d

                tree = tree.map(resolve_leaf_paths, nodes=True)
                root_path = tree.data['path']
                root_url = f'{commit_permalink_prefix}/{root_path}'

                def mark_processed(d):
                    if d.get('leaf') and not d.get('processed'):
                        new = { **d, 'processed': True }
                        del new['leaf']
                        return new
                    return d

                tree = tree.map(mark_processed)
                num_processed = tree.reduce(lambda d: 1 if d.get('processed') else 0)

            if keep_tmpdir and not current_dir: print(f'tmp clone: {dir}')
            print(f'Commit {prev_commit}, root URL {root_url}')

            run([ 'git', 'fetch', remote, ])
            run([ 'git', 'config', 'advice.detachedHead', 'false', ])

            upstream_branch = f'{remote}/{branch}'
            try:
                run([ 'git', 'checkout', '-b', branch, prev_commit ])
            except:
                # `branch` already exists
                run([ 'git', 'checkout', branch ])
                run([ 'git', 'reset', '--hard', prev_commit ])

            if success('git', 'show', upstream_branch):
                run([ 'git', 'branch', f'--set-upstream-to={branch}', branch ])
                branch_commit = _output([ 'git', 'log', '-n', '1', '--format=%H', upstream_branch, ]).strip()
            else:
                branch_commit = prev_commit

            parents = { prev_commit, branch_commit }
            git_tree = _output(['git', 'log', '-n', '1', '--format=%T', prev_commit, ]).strip()
            merged_commit = _output(
                [ 'git', 'commit-tree', git_tree, ] + \
                [
                    arg
                    for parent in parents
                    for arg in [ '-p', parent ]
                ] + \
                [ '-m', 'Merge in new CWL snapshot', ]
            ) \
            .strip()
            print(f'Created merged commit {merged_commit} with parents {parents}')
            run([ 'git', 'reset', '--hard', merged_commit ])
            run(
                [ 'git', 'push', ] + \
                [
                    arg
                    for option in push_options
                    for arg in [ '-o', option ]
                ] + \
                [ remote, branch, ])

            with out_ctx:
                with root_path.open('r') as f:
                    cwl = yaml.load(f)

                yaml.dump(cwl, out)

            print('')
            print('*' * 80)
            print(f'Snapshotted {input}:')
            print(f'         Branch: {branch} ({www_url_prefix}/tree/{branch})')
            print(f'         Commit: {prev_commit} ({www_url_prefix}/commit/{prev_commit})')
            print(f'  CWL Permalink: {root_url}')
            if write_output_file:
                print(f' CWL local path: {output}')
            print('*' * 80)

            if copy_to_clipboard and success('which', 'pbcopy'):
                print('Copying to clipboard…')
                from subprocess import PIPE, Popen
                echo = Popen([ 'echo', root_url ], stdout=PIPE)
                pbcopy = Popen([ 'pbcopy' ], stdin=echo.stdout)
                pbcopy.wait()

            if open_browser and success('which', 'open'):
                run([ 'open', root_url, ])

if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Canonicalize URLs to CWLs in an input directory')
    parser.add_argument('input', help='Input CWL(s) to snapshot')
    parser.add_argument('output', nargs='?', help='Path to write output "snapshotted"/permalinked CWL to (default: /dev/null; pass "-" for stdout)')
    parser.add_argument('--branch', '-b', help='Branch to push CWLs to')
    parser.add_argument('--current_dir', '-c', action='store_true', help="When set, don't git clone into a temporary dir to work in; add commits and push directly from the clone containing this file")
    parser.add_argument('--keep_tmpdir', '-k', action='store_true', help="When set, don't delete the temporary git clone including snapshotted CWLs (useful for debugging)")
    parser.add_argument('--copy_to_clipboard', '-l', action='store_true', help="When set, copy a permalink to the CWL to the clipboard (if pbcopy is available)")
    parser.add_argument('--open_browser', '-o', action='store_true', help='When set, open the permalinked CWL in a web browser')
    parser.add_argument('--push_option', action='append', help='Arguments to send to the server when git pushing (see "git push" docs about "-o" options)')
    parser.add_argument('--repo_url_prefix', '-p', help='Base URL for output CWL permalink, e.g. https://gitlab.com/<group>/<project>/raw')
    parser.add_argument('--remote', '-r', help='Name of the git remote to fetch from / push to (e.g. if --current_dir is set, and an existing git remote should be used)')
    parser.add_argument('--upstream', '-u', help='Git remote URL to push snapshotted CWLs to (ignored if --current_dir is set and --remote is pointed at an existing git remote in the current clone)')
    args = parser.parse_args()
    main(
        input=args.input,
        output=args.output,
        branch=args.branch,
        remote=args.remote,
        upstream=args.upstream,
        push_options=args.push_option,
        current_dir=args.current_dir,
        keep_tmpdir=args.keep_tmpdir,
        open_browser=args.open_browser,
        copy_to_clipboard=args.copy_to_clipboard,
    )
