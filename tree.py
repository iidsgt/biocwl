class Tree:
    def __init__(self, data, children_fn, parent=None, data_fn=None, name=None):
        if data_fn:
            data = data_fn(data)
        self.data = data
        self.parent = parent
        self.name = name

        children = children_fn(data)
        if isinstance(children, list):
            self.children = [
                Tree(child, children_fn, parent=self, data_fn=data_fn)
                for child in children
            ]
        elif isinstance(children, dict):
            self.children = {
                name: Tree(child, children_fn, parent=self, data_fn=data_fn, name=name)
                for name, child in children.items()
            }
        else:
            raise Exception(f'Invalid children: {children}')

    def foreach(self, fn):
        self.foreach_node(lambda n: fn(n.data))

    def foreach_node(self, fn):
        fn(self)
        [ child.foreach_node(fn) for child in self.child_nodes ]

    @property
    def child_nodes(self):
        if isinstance(self.children, list):
            return self.children
        else:
            return self.children.values()

    def fold(self, init, fn, nodes=False):
        init = fn(init, self if nodes else self.data)
        for child in self.child_nodes:
            init = child.fold(init, fn, nodes=nodes)
        return init

    def reduce(self, fn, nodes=False, default=None):
        def apply(accum, v):
            val = fn(v)
            if accum is None:
                return val
            else:
                return accum + val
        return self.fold(default, apply, nodes=nodes)

    def arr(self, fn, nodes=False):
        return self.reduce(lambda v: [ fn(v) ], nodes=nodes, default=[])

    def _map(self, fn, nodes=False):
        self.data = fn(self if nodes else self.data)
        [ child._map(fn, nodes=nodes) for child in self.child_nodes ]

    def copy(self):
        # Make a new tree where each node's data is the full corresponding node from the original tree
        new = Tree(self, children_fn=lambda d: d.children)
        # "Flatten" that new tree so that each node's "data" is the data from the wrapped original-tree node
        new._map(lambda d: d.data)
        return new

    def map(self, fn, nodes=False):
        new = self.copy()
        new._map(fn, nodes=nodes)
        return new

    def filter(self, fn):
        if not fn(self):
            return None
        new = self.copy()
        if isinstance(self.children, list):
            new.children = [ child.filter(fn) for child in new.children ]
            new.children = [ child for child in new.children if child ]
        else:
            filtered = { name: child.filter(fn) for name, child in new.children.items() }
            dropped = { name: child for name, child in filtered.items() if child }
            print(f'{self.name}: new children: {len(new.children)} -> {len(filtered)} -> {len(dropped)}')
            new.children = dropped
        return new

    @property
    def leaves(self, nodes=False):
        return self.reduce(lambda n: [] if n.children else [ n if nodes else n.data ])

    def pop_leaves(self):
        trunks = self.filter(lambda n: n.children)
        return trunks, self.leaves

    def __len__(self):
        return 1 + sum([ len(child) for child in self.child_nodes ])
