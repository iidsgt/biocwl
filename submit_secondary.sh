secondary_yaml="B905/MM_0071_BM_02.SECONDARY.yml"
priority=500
report_id=`echo ${secondary_yaml##*/} | sed 's/.SECONDARY.yml//g'`

echo "submitting SECONDARY for report ID" $report_id "with yaml" $secondary_yaml

docker run \
-v "/data1/users/meghana/biocwl:/data" \
-v /var/run/docker.sock:/var/run/docker.sock \
-e ARVADOS_API_TOKEN \
-e ARVADOS_API_HOST \
sinaiiidgst/arvclient \
arvados-cwl-runner \
--submit \
--no-wait \
--intermediate-output-ttl 2592000 \
--project-uuid myarv-j7d0g-4j0q8ta0l27blwm \
--priority $priority \
--name ${report_id}.SECONDARY \
--output-name ${report_id}.SECONDARY \
--output-tags SECONDARY,outputs \
/data/Workflows/DAPHNI-Secondary.cwl \
/data/$secondary_yaml